/* 
 //  
 //                                  _oo8oo_
 //                                 o8888888o
 //                                 88" . "88
 //                                 (| -_- |)
 //                                 0\  =  /0
 //                               ___/'==='\___
 //                             .' \\|     |// '.
 //                            / \\|||  :  |||// \
 //                           / _||||| -:- |||||_ \
 //                          |   | \\\  -  /// |   |
 //                          | \_|  ''\---/''  |_/ |
 //                          \  .-\__  '-'  __/-.  /
 //                        ___'. .'  /--.--\  '. .'___
 //                     ."" '<  '.___\_<|>_/___.'  >' "".
 //                    | | :  `- \`.:`\ _ /`:.`/ -`  : | |
 //                    \  \ `-.   \_ __\ /__ _/   .-` /  /
 //                =====`-.____`.___ \_____/ ___.`____.-`=====
 //                                  `=---=`
 //  
 //  
 //               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 // 
 //                          佛祖保佑         永不宕机/永无bug
 //
 */

/**
 * date:2020/4/13
 * author: 杨不易
 * description: 前后端分离 toen存储 ,页面验证登陆,部门权限
 */

/**
 * 设置cookie
 * @param {Object} name
 */
function setCookie(name, value) {
	var Days = 30;
	var exp = new Date();
	exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}



/**
 * 获取cookie
 */
function getCookie(name) {
	var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
	if (arr = document.cookie.match(reg)) {
		return unescape(arr[2]);
	} else {
		return null;
	}
}


/**
 * 删除cookie 
 * 根据name删除cookie
 * @param {Object} name
 */
function delCookie(name) {
	var exp = new Date();
	exp.setTime(exp.getTime() - 1);
	var cval = getCookie(name);
	if (cval != null) {
		document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
	}
}

/**
 * 清除所有cookie
 */
function clearAllCookie() {
	var date = new Date();
	date.setTime(date.getTime() - 10000);
	var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
	console.log("需要删除的cookie名字：" + keys);
	if (keys) {
		for (var i = keys.length; i--;)
			document.cookie = keys[i] + "=0; expire=" + date.toGMTString() + "; path=/";
	}
}

/**
 * [获取URL中的参数名及参数值的集合]
 * 示例URL:http://htmlJsTest/getrequest.html?uid=admin&rid=1&fid=2&name=小明
 * @param {[string]} urlStr [当该参数不为空的时候，则解析该url中的参数集合]
 * @return {[string]}       [参数集合]
 */
function GetRequest(urlStr) {
	if (typeof urlStr == "undefined") {
		var url = decodeURI(location.search); //获取url中"?"符后的字符串
	} else {
		var url = "?" + urlStr.split("?")[0];
	}
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

//获取当前提交地址参数
function getParams(key) {
	var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) {
		return unescape(r[2]);
	}
	return null;
};


/**
 * 键值对转换对象
 * name=yby ----> {name:yby}
 * 传入"#dataFrm" 表单id
 * @param {Object} formName
 */
function getJson(formName) {
	console.log($(formName).serializeJSON());
	console.log(JSON.stringify($(formName).serializeJSON()));
	return $(formName).serializeJSON();
}


// 进行了负载均衡 8081 8082 8080 三个端口
// 根据nginx  映射这三个端口  
// var api = 'https://yangbuyi.top:8080/'
// 访问后台地址 获取数据   因为 nginx设置了端口映射 则不需要进行 添加端口号  
// 九个
// 业务层和系统层
var api = 'http://localhost:8080/api/';
// 杨帅
var api1 = 'http://localhost:8080/api/yangshuai/';
// 刘璠
var api2 = 'http://localhost:8080/api/liufan/';
var api3 = 'http://localhost:8080/api/zhouzihao/';
var api4 = 'http://localhost:8080/api/yingshengjie/';
var api5 = 'http://localhost:8080/api/liuchengyang/';
var api6 = 'http://localhost:8080/api/liuxinyu/';
var api7 = 'http://localhost:8080/api/tanyuhao/';
var api8 = 'http://localhost:8080/api/zhangxinge/';




// var api2 = 'http://localhost:8080/api/';
// 文件上传地址 端口81 
// var baseUrl = 'http://localhost:8080/api:81/';

// 用于其它的 
var baseUrl = 'http://yangbuyi.top:81/';
//var baseUrl = 'http://39.101.199.86:81/';
// http://127.0.0.1:8848/erpweb/login.html
// var loginUrl = 'http://localhost:8080/api/login.html';
var loginUrl = 'http://127.0.0.1:8848/erpweb/login.html';

// var indexUrl = 'http://127.0.0.1:8848/ERP-WEB/index.html';

// 下次再发ajax请求把token带到后台 
// 获取前端存入进去的token
var token = $.cookie('TOKEN');
//  第三方登陆的问题 顺序
if (getParams("token")) {
	//登陆成功这后把token放到cookie里面
	$.cookie('TOKEN', getParams("token"), {
		expires: 7
	});
	sessionStorage.setItem("yby", getParams("token"))
	// 存储到全局ajax
	token = getParams("token");
	console.log(token);
	let gitee = getParams("gitee");
	console.log(gitee);
	let github = getParams("github");
	console.log(gitee);

}


//设置全局ajax前置拦截  每一次ajax都把token带过去确保登陆成功凭证
$.ajaxSetup({
	headers: {
		'TOKEN': token //每次ajax请求时把token带过去
	}
});


//如果访问登陆页面这外的页面并且还没有登陆成功之后写入cookie的token就转到登陆页面
if (token == undefined) {

//	alert("token不见啦" + $.cookie('TOKEN'));
	if (window.location != loginUrl) {
		window.top.location = loginUrl;
	}
} else {
	// 如果当前不是登陆页面进行访问后台   判断是否已经登陆成功   
	// 如诺没有登陆成功  则跳转到 登陆页面
	if (window.location != loginUrl) {
		$.ajax({
			url: api + "login/checkLogin",
			async: true,
			type: 'post',
			dataType: 'json',
			success: function(res) {
				if (res.code == -1) {
					window.top.location = loginUrl;
				}
			},
			error: function(res) {
				// alert("登陆失效.请重新,认证!");
				window.top.location = loginUrl;
			}
		});
	}
}

//删除cookie
function delCookie(name) {
	var exp = new Date();
	exp.setTime(exp.getTime() - 1);
	var cval = $.cookie('TOKEN');
	if (cval != null)
		document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString() + ";path=";
}

// 获取权限
var pers = sessionStorage.getItem("permissions");
// 获取用户类型
var usertype = sessionStorage.getItem("usertype");
// 判断是否为超级管理 是的话则显示所有的增删改按钮权限
// 否则 根据用户id获取对应的按钮权限
if (usertype == 1) {
	console.log(pers)
	if (pers != null) {
		// var permissions=pers.split(",");
		// //部门权限开始
		// if(permissions.indexOf("dept:add")<0){
		// 	$(".dept_btn_add").hide();
		// }
		// if(permissions.indexOf("dept:update")<0){
		// 	$(".btn_update").hide();
		// }
		// if(permissions.indexOf("dept:delete")<0){
		// 	$(".btn_delete").hide();
		// }

		//permissions包涵所有的权限
		var permissions = pers.split(",");
		console.log(permissions);

		//部门权限 
		if (permissions.indexOf("dept:add") < 0) {
			$(".dept_btn_add").hide();
		}
		if (permissions.indexOf("dept:delete") < 0) {
			$(".dept_btn_delete").hide();
		}
		if (permissions.indexOf("dept:update") < 0) {
			$(".dept_btn_update").hide();
		}


		//菜单权限
		if (permissions.indexOf("menu:add") < 0) {
			$(".menu_btn_add").hide();
		}
		if (permissions.indexOf("menu:delete") < 0) {
			$(".menu_btn_delete").hide();
		}
		if (permissions.indexOf("menu:update") < 0) {
			$(".menu_btn_update").hide();
		}



		//部门权限结束
	} else {
		// $(".btn_add").hide();
		// $(".btn_update").hide();
		// $(".btn_delete").hide();
		// $(".btn_dispatch").hide();
		// $(".btn_reset").hide();


		// 全局类进行隐藏

		//权限为空时 表示 权限都无 都隐藏
		//添加按钮隐藏
		$("a[class$='_btn_add']").hide();
		$("button[class$='_btn_add']").hide();
		$("input[class$='_btn_add']").hide();
		//修改按钮隐藏
		$("a[class$='_btn_update']").hide();
		$("button[class$='_btn_update']").hide();
		$("input[class$='_btn_update']").hide();
		//删除按钮
		$("a[class$='_btn_delete']").hide();
		$("button[class$='_btn_delete']").hide();
		$("input[class$='_btn_delete']").hide();

		// 批量删除按钮
		$("a[class$='_btn_batchdelete']").hide();
		$("button[class$='_btn_batchdelete']").hide();
		$("input[class$='_btn_batchdelete']").hide();

		//分配按钮
		$("a[class$='_btn_dispatch']").hide();
		$("button[class$='_btn_dispatch']").hide();
		$("input[class$='_btn_dispatch']").hide();
		//分配按钮
		$("a[class$='_btn_reset']").hide();
		$("button[class$='_btn_reset']").hide();
		$("input[class$='_btn_reset']").hide();


	}
} else {
	// 不进行隐藏   超级管理员
}

//给页面显示登陆用户名
var username = sessionStorage.getItem("username");
$(".login_name").html(username);



/**
 * 封装全局编号
 * 传入ID
 * 如:200
 */
function generatingNumberingRules(id, numbers) {
	var designId = id;
	var time = new Date();
	var year = time.getFullYear();
	designId += year
	var month = time.getMonth() + 1
	if (month >= 0) {
		month = '0' + month
	}
	designId += month
	var date = time.getDate();
	designId += date

	var keyCode;
	if (numbers == 6) {
		keyCode = Math.floor(Math.random() * 900000 + 100000);
	} else {
		keyCode = Math.floor(Math.random() * 9000 + 1000);
	}
	designId + keyCode;
	console.log(designId)
	return designId;
}
