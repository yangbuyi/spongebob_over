package top.yangbuyi;

import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import top.yangbuyi.system.mapper.DeptMapper;

@SpringBootTest
@MapperScan(basePackages = {"top.yangbuyi.system.mapper", "top.yangbuyi.business.mapper"})
class YangbuyiErp2020ApplicationTests {
      //
      @Autowired
      RedisTemplate redisTemplate;
      
      @Autowired
      private DeptMapper deptMapper;
      
    /*
      @Test
      public void loginTest(){
    Object countObj = redisTemplate.execute(new RedisCallback() {  
        public Object doInRedis(RedisConnection connection) throws DataAccessException {  
            return connection.incr("用户id".getBytes());  
        }  
    });  
    int count = Integer.parseInt(countObj.toString());  
    //expire:设置key的时间,第三个参数是时间的单位,这里设置为"秒"  
    if(count == 1)  
        redisTemplate.expire("用户id",30, TimeUnit.SECONDS);  
    if(count <= 5)  
        System.out.println("第"+count+"次登录"+"================登陆成功");  
    else  
        System.out.println("第"+count+"次登录"+"================超过五次");  
      }  */
      
      
      @Test
      void contextLoads() {
      
          /*  for (int i = 0; i < 10; i++) {
                  Object countObj = redisTemplate.execute(new RedisCallback() {
                        @Override
                        public Object doInRedis(RedisConnection connection) throws DataAccessException {
                              return connection.incr("杨不易".getBytes());
                        }
                  });
                  System.out.println(countObj.toString());
                  int j = Integer.parseInt(countObj.toString());
                  // expire:设置key的时间,第三个参数是时间的单位,这里设置为"秒"  
                  if (j == 1)
                        redisTemplate.expire("杨不易", 30, TimeUnit.SECONDS);
                  if (j <= 5)
                        System.out.println("第" +j + "次登陆 登陆成功");
                  else
                        System.out.println("第" + i + "次登陆 超过五次了");
            }*/
            
        
                  
            
            /*
            Dept dept = deptMapper.selectById(1);
            System.out.println(
                  dept
            );
      
            ValueOperations valueOperations = redisTemplate.opsForValue();
            valueOperations.set("杨不易","好帅");
      
      
            Object 杨不易 = valueOperations.get("杨不易");
            System.out.println(杨不易);*/
//            ValueOperations valueOperations = redisTemplate.opsForValue();
//
//            System.out.println(valueOperations.get("shiro:session:11fbdb59-0d5e-49a0-9c9e-ea6728734614"));
      }
      
}
