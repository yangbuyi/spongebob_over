package top.yangbuyi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan(basePackages = {"top.yangbuyi.*.mapper","top.yangbuyi.*.*.mapper"})
@EnableCaching
public class YangbuyiErp2020Application {
    
    public static void main(String[] args) {
        SpringApplication.run(YangbuyiErp2020Application.class, args);
    }
    
}
