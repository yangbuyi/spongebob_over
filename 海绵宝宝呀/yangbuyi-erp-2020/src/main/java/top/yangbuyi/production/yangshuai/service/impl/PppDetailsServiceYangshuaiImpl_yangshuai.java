package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.PppDetails;
import top.yangbuyi.production.yangshuai.domain.PppDetailsExample;
import top.yangbuyi.production.yangshuai.mapper.PppDetails_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.service.PppDetailsService_yangshuai;

import java.util.List;
/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

@Service
public class PppDetailsServiceYangshuaiImpl_yangshuai extends ServiceImpl<PppDetails_yangshuai_Mapper, PppDetails> implements PppDetailsService_yangshuai {

    @Override
    public long countByExample(PppDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(PppDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<PppDetails> selectByExample(PppDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(PppDetails record,PppDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(PppDetails record,PppDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }
    
    @Override
    public void updateDetails(PppDetails pppDetails) {
        pppDetails.setDesignModuleTag("D002-2");
        baseMapper.updatePppDetails(pppDetails);
    }
}
