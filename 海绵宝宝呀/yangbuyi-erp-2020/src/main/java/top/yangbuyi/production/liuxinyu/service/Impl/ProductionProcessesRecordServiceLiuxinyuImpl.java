package top.yangbuyi.production.liuxinyu.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import top.yangbuyi.production.liuxinyu.mapper.ProductionProcessesRecord_liuxinyu_Mapper;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecord;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecordExample;
import top.yangbuyi.production.liuxinyu.service.ProductionProcessesRecordService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.ProductionRecordVo;
import top.yangbuyi.system.common.DataGridView;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Service
public class ProductionProcessesRecordServiceLiuxinyuImpl extends ServiceImpl<ProductionProcessesRecord_liuxinyu_Mapper, ProductionProcessesRecord> implements ProductionProcessesRecordService_liuxinyu {

    @Override
    public long countByExample(ProductionProcessesRecordExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductionProcessesRecordExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductionProcessesRecord> selectByExample(ProductionProcessesRecordExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductionProcessesRecord record,ProductionProcessesRecordExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductionProcessesRecord record,ProductionProcessesRecordExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public DataGridView querySelAllPlan(ProductionRecordVo productionRecordVo) {
            IPage<ProductionProcessesRecord> page = new Page<>(productionRecordVo.getPage(), productionRecordVo.getLimit());
            // 创建条件
            QueryWrapper<ProductionProcessesRecord> processesRecordQueryWrapper = new QueryWrapper<>();

            processesRecordQueryWrapper.eq(StringUtils.isNotBlank(productionRecordVo.getCheck_tag()),"check_tag",productionRecordVo.getCheck_tag());
            processesRecordQueryWrapper.like(StringUtils.isNotBlank(productionRecordVo.getProcedure_name()),"procedure_name",productionRecordVo.getProcedure_name());
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
//            System.out.println();// new Date()为获取当前系统时间
//            processesRecordQueryWrapper.ge(productionRecordVo.getRegisterTime() != null,"register_time",productionRecordVo.getRegisterTime());
//            productionPlanQueryWrapper.le(productionPlanVo.getRegisterTime() != null,"register_time",df.format(new Date()));
//            System.out.println(productionRecordVo.getRegisterTime());


            // 执行条件
            IPage<ProductionProcessesRecord> processesRecordIPage = this.baseMapper.selectPage(page, processesRecordQueryWrapper);

            return new DataGridView(processesRecordIPage.getTotal(),processesRecordIPage.getRecords());
      }

      @Override
      public int batchNoPass(String[] ids, String checker, Date checkTime) {

            return baseMapper.batchNoPass(ids,checker,checkTime);
      }

      @Override
      public int batchPass(String[] ids,String checker,Date checkTime) {
            return baseMapper.batchPass(ids, checker,checkTime);
      }

      @Override
      public ProductionProcessesRecord saveRecord(ProductionProcessesRecord processesRecord) {
            this.baseMapper.insert(processesRecord);
            return processesRecord;
      }

      @Override
      public int count(String procedureId) {
            return this.baseMapper.count(procedureId);
      }
}
