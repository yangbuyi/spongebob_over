package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetailsExample;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcess;
import top.yangbuyi.production.yangshuai.mapper.PppMaterialDetails_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.mapper.ProductMaterialDetails_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.mapper.ProductProductionProcess_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.service.ProductMaterialDetailsService_yangshuai;

import java.util.List;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/7/17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

@Service
public class ProductMaterialDetailsServiceYangshuaiImpl_yangshuai extends ServiceImpl<ProductMaterialDetails_yangshuai_Mapper, ProductMaterialDetails> implements ProductMaterialDetailsService_yangshuai {
      
      @Autowired
      private PppMaterialDetails_yangshuai_Mapper pppMaterialDetailsYangshuaiMapper;
      @Autowired
      private ProductProductionProcess_yangshuai_Mapper productProductionProcessYangshuaiMapper;
      
      //      ProductMaterialService_yangshuai
      @Override
      public long countByExample(ProductMaterialDetailsExample example) {
            return baseMapper.countByExample(example);
      }
      
      @Override
      public int deleteByExample(ProductMaterialDetailsExample example) {
            return baseMapper.deleteByExample(example);
      }
      
      @Override
      public List<ProductMaterialDetails> selectByExample(ProductMaterialDetailsExample example) {
            return baseMapper.selectByExample(example);
      }
      
      @Override
      public int updateByExampleSelective(ProductMaterialDetails record, ProductMaterialDetailsExample example) {
            return baseMapper.updateByExampleSelective(record, example);
      }
      
      @Override
      public int updateByExample(ProductMaterialDetails record, ProductMaterialDetailsExample example) {
            return baseMapper.updateByExample(record, example);
      }
      
      @Override
      public void addPppMaterialDetails(ProductMaterialDetails productMaterialDetails) {
            System.err.println(productMaterialDetails); //
            
      /*      ProductMaterialDetails s =new ProductMaterialDetails();
            productMaterialDetails.setResidualAmount(productMaterialDetails.getResidualAmount());
            productMaterialDetails.setAmount(productMaterialDetails.getAmount());
            productMaterialDetails.setProductId(productMaterialDetails.getProductId());
            System.err.println(s);*/
            
            
            //  减 可用数量
            baseMapper.updateResidual(productMaterialDetails);
            
            // 开始插入 物料 数据 一对多嘛
            PppMaterialDetails pppMaterialDetails = new PppMaterialDetails();
            pppMaterialDetails.setParentId(productMaterialDetails.getParentId());
            pppMaterialDetails.setDetailsNumber(productMaterialDetails.getDetailsNumber());
            pppMaterialDetails.setProductId(productMaterialDetails.getProductId());
            pppMaterialDetails.setProductName(productMaterialDetails.getProductName());
            pppMaterialDetails.setType("Y001-2");
            pppMaterialDetails.setAmount(productMaterialDetails.getAmount());
            pppMaterialDetails.setProductDescribe(productMaterialDetails.getProductDescribe());
            pppMaterialDetails.setAmountUnit(productMaterialDetails.getAmountUnit());
            pppMaterialDetails.setCostPrice(productMaterialDetails.getCostPrice());
            pppMaterialDetails.setSubtotal(productMaterialDetails.getSubtotal());
            // 插入
            pppMaterialDetailsYangshuaiMapper.insertPppMaterialDetails(pppMaterialDetails);
      }
      
      @Override
      public List<ProductMaterialDetails> getProductMaterialDetailsList(String productId) {
            return baseMapper.selectProductMaterialDetails(productId);
      }
      
      @Override
      public void updateProductionProcesses(ProductProductionProcess process) {
            this.productProductionProcessYangshuaiMapper.updateProductionProcesses(process);
      }
      
      @Override
      public void getproductmaterialdetails(Integer id) {
      
      }
}
