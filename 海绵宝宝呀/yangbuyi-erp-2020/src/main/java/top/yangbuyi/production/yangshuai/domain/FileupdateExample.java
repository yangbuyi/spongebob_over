package top.yangbuyi.production.yangshuai.domain;

import lombok.Data;

import java.util.Date;

/**
 * @description: 杨不易网站:www.yangbuyi.top
 * @program: yangbuyi-erp-2020
 * @ClassName: FileupdateExample
 * @create: 2020-07-20 11:49
 * @author: yangbuyi
 * @since： JDK1.8
 * @FileupdateExample:
 **/

@Data
public class FileupdateExample {
      private String productId;
      private Double realCostPrice;
      private String designProcedureTag;
      private String designCellTag;
      private ProductionProcesses ProductionProcesses;
      private PppDetails pppDetails;
      private PDetails pdetails;
      
      private Integer id;
      private String storeId;
      private String storeName;
      private String RproductId;
      private String productName;
      private String firstKindId;
      private String firstKindName;
      private String secondKindId;
      private String secondKindName;
      private String thirdKindId;
      private String thirdKindName;
      private Double minAmount;
      private Double maxAmount;
      private String storageUnitShorted;
      private String amountUnit;
      private Double maxCapacityAmount;
      private Double amount;
      private String config;
      private String register;
      private Date registerTime;
      private String checker;
      private Date checkTime;
      private String checkTag;
      
}
