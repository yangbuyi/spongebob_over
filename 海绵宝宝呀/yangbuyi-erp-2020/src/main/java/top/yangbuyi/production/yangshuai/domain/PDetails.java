package top.yangbuyi.production.yangshuai.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName(value = "p_details")
public class PDetails implements Serializable {
    @TableId(value = "procedureId", type = IdType.INPUT)
    private Integer procedureid;

    @TableField(value = "procedureName")
    private String procedurename;

    @TableField(value = "procedureDescribe")
    private String proceduredescribe;

    private static final long serialVersionUID = 1L;

    public static final String COL_PROCEDUREID = "procedureId";

    public static final String COL_PROCEDURENAME = "procedureName";

    public static final String COL_PROCEDUREDESCRIBE = "procedureDescribe";
}