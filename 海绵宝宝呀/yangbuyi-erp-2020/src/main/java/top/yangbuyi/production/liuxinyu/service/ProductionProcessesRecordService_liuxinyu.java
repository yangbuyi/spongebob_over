package top.yangbuyi.production.liuxinyu.service;

import java.util.Date;
import java.util.List;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecord;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecordExample;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.liuxinyu.vo.ProductionRecordVo;
import top.yangbuyi.system.common.DataGridView;

/**
@日复一日
@进入代码世界
@SmallNew
*/
public interface ProductionProcessesRecordService_liuxinyu extends IService<ProductionProcessesRecord>{


    long countByExample(ProductionProcessesRecordExample example);

    int deleteByExample(ProductionProcessesRecordExample example);

    List<ProductionProcessesRecord> selectByExample(ProductionProcessesRecordExample example);

    int updateByExampleSelective(ProductionProcessesRecord record, ProductionProcessesRecordExample example);

    int updateByExample(ProductionProcessesRecord record, ProductionProcessesRecordExample example);

    DataGridView querySelAllPlan(ProductionRecordVo productionRecordVo);

    int batchNoPass(String[] ids, String checker, Date checkTime);

    int batchPass(String[] ids, String checker, Date checkTime);

    ProductionProcessesRecord saveRecord(ProductionProcessesRecord processesRecord);

    int count(String procedureId);
}
