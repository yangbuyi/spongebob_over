package top.yangbuyi.production.liuxinyu.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import top.yangbuyi.system.vo.BaseVo;

import java.util.Date;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductionPlanVo extends BaseVo {
//      register_time
      @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:dd")
      private Date registerTime;

      private String product_name;

      private String check_tag;
}
