package top.yangbuyi.production.zhouzihao.vo;

import lombok.Data;

import java.io.Serializable;
@Data
public class ProductionProcessVo implements Serializable {

      private String productId;
      private String designId;
      private String productName;
}
