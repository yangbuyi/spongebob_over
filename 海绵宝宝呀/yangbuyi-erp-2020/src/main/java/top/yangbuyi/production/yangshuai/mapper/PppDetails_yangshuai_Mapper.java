package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.PppDetails;
import top.yangbuyi.production.yangshuai.domain.PppDetailsExample;

import java.util.List;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/7/17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface PppDetails_yangshuai_Mapper extends BaseMapper<PppDetails> {
    long countByExample(PppDetailsExample example);
    
    int deleteByExample(PppDetailsExample example);
    
    List<PppDetails> selectByExample(PppDetailsExample example);
    
    int updateByExampleSelective(@Param("record") PppDetails record, @Param("example") PppDetailsExample example);
    
    int updateByExample(@Param("record") PppDetails record, @Param("example") PppDetailsExample example);
      
      void updatePppDetails(PppDetails pppDetails);
}