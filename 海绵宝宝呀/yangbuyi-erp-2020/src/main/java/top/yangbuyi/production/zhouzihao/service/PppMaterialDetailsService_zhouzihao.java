package top.yangbuyi.production.zhouzihao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetails;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetailsExample;

import java.util.List;
public interface PppMaterialDetailsService_zhouzihao extends IService<PppMaterialDetails>{


    long countByExample(PppMaterialDetailsExample example);

    int deleteByExample(PppMaterialDetailsExample example);

    List<PppMaterialDetails> selectByExample(PppMaterialDetailsExample example);

    int updateByExampleSelective(PppMaterialDetails record, PppMaterialDetailsExample example);

    int updateByExample(PppMaterialDetails record, PppMaterialDetailsExample example);

      List<PppMaterialDetails> findAllByIdForAllList(String id);
      
      List<PppMaterialDetails> findPppDetailsById(Integer id);
}
