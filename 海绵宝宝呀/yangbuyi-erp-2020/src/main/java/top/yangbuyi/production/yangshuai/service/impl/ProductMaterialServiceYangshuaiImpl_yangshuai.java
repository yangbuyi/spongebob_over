package top.yangbuyi.production.yangshuai.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.yangshuai.mapper.ProductMaterial_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.domain.ProductMaterial;
import top.yangbuyi.production.yangshuai.service.ProductMaterialService_yangshuai;
/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/16
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

@Service
public class ProductMaterialServiceYangshuaiImpl_yangshuai extends ServiceImpl<ProductMaterial_yangshuai_Mapper, ProductMaterial> implements ProductMaterialService_yangshuai {

}
