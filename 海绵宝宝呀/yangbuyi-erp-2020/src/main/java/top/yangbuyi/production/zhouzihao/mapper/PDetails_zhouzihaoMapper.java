package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.PDetails;
import top.yangbuyi.production.zhouzihao.domain.PDetailsExample;

public interface PDetails_zhouzihaoMapper extends BaseMapper<PDetails> {
    long countByExample(PDetailsExample example);

    int deleteByExample(PDetailsExample example);

    List<PDetails> selectByExample(PDetailsExample example);

    int updateByExampleSelective(@Param("record") PDetails record, @Param("example") PDetailsExample example);

    int updateByExample(@Param("record") PDetails record, @Param("example") PDetailsExample example);
}