package top.yangbuyi.production.zhouzihao.service;

import java.util.List;

import top.yangbuyi.production.zhouzihao.domain.PDetails;
import top.yangbuyi.production.zhouzihao.domain.PDetailsExample;
import com.baomidou.mybatisplus.extension.service.IService;
public interface PDetailsService_zhouzihao extends IService<PDetails>{


    long countByExample(PDetailsExample example);

    int deleteByExample(PDetailsExample example);

    List<PDetails> selectByExample(PDetailsExample example);

    int updateByExampleSelective(PDetails record, PDetailsExample example);

    int updateByExample(PDetails record, PDetailsExample example);

}
