package top.yangbuyi.production.liuxinyu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "production_processes_record")
public class ProductionProcessesRecord implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "details_number")
    private Integer detailsNumber;

    @TableField(value = "procedure_id")
    private String procedureId;

    @TableField(value = "procedure_name")
    private String procedureName;

    @TableField(value = "labour_hour_amount")
    private BigDecimal labourHourAmount;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    @TableField(value = "procedure_describe")
    private String procedureDescribe;

    @TableField(value = "reg_count")
    private BigDecimal regCount;

    @TableField(value = "procedure_responsible_person")
    private String procedureResponsiblePerson;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_DETAILS_NUMBER = "details_number";

    public static final String COL_PROCEDURE_ID = "procedure_id";

    public static final String COL_PROCEDURE_NAME = "procedure_name";

    public static final String COL_LABOUR_HOUR_AMOUNT = "labour_hour_amount";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_PROCEDURE_DESCRIBE = "procedure_describe";

    public static final String COL_REG_COUNT = "reg_count";

    public static final String COL_PROCEDURE_RESPONSIBLE_PERSON = "procedure_responsible_person";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";
}