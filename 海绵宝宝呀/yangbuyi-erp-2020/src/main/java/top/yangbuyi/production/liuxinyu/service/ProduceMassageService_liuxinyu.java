package top.yangbuyi.production.liuxinyu.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.liuxinyu.domain.ProduceMassage;
import top.yangbuyi.production.liuxinyu.domain.ProduceMassageExample;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcesses;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesMaterial;
import top.yangbuyi.production.liuxinyu.vo.ProduceMassageVo;
import top.yangbuyi.system.common.DataGridView;

/**
@日复一日
@进入代码世界
@SmallNew
*/
public interface ProduceMassageService_liuxinyu extends IService<ProduceMassage>{


    long countByExample(ProduceMassageExample example);

    int deleteByExample(ProduceMassageExample example);

    List<ProduceMassage> selectByExample(ProduceMassageExample example);

    int updateByExampleSelective(ProduceMassage record, ProduceMassageExample example);

    int updateByExample(ProduceMassage record, ProduceMassageExample example);

    DataGridView querySelAllMassage(ProduceMassageVo produceMassageVo);

    DataGridView selProcesses(Integer parentId);

    DataGridView selPMaterial(Integer id);

    List<ProductionProcessesMaterial> selPM(Integer id);

    int updProcesses(Integer id, Integer hour);

    int updPMaterial(Integer id, Integer bcRenewAmount);

    ProductionProcesses selByIdPro(Integer id);
}
