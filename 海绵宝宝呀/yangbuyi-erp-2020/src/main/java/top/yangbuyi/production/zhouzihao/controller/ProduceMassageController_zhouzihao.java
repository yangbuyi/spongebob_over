package top.yangbuyi.production.zhouzihao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.zhouzihao.service.ProduceMassageService_zhouzihao;
import top.yangbuyi.production.zhouzihao.vo.ProduceMassageVo;
import top.yangbuyi.system.common.DataGridView;

import java.text.ParseException;

@RestController
@RequestMapping("api/zhouzihao/ProduceMassage")
public class ProduceMassageController_zhouzihao {

      @Autowired
      private ProduceMassageService_zhouzihao produceMassageService;

      @RequestMapping("insProduceMassage")
      public Object insProduceMassage(@RequestBody ProduceMassageVo produceMassage){
            int i=0;
            try {
                  i =produceMassageService.insProduceMassage(produceMassage);
            } catch (ParseException e) {
                  e.printStackTrace();
            }
            return i;
      }

      @RequestMapping("findAllPageByStatus")
      public Object findAllPage(Integer page,Integer limit,String status){
            DataGridView data=produceMassageService.findAllPage(page,limit,status);
            return data;
      }

      /**
       * 审核
       * @param
       * @param
       * @param status
       * @return
       */
      @RequestMapping("Audit")
      public Object findAllPage(Integer id,String status){
            int data=produceMassageService.Audit(id,status);
            return data;
      }
}
