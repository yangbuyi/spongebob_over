package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.ProductMaterial;
import top.yangbuyi.production.zhouzihao.domain.ProductMaterialExample;

public interface ProductMaterialMapper_zhouzihao extends BaseMapper<ProductMaterial> {
    long countByExample(ProductMaterialExample example);

    int deleteByExample(ProductMaterialExample example);

    List<ProductMaterial> selectByExample(ProductMaterialExample example);

    int updateByExampleSelective(@Param("record") ProductMaterial record, @Param("example") ProductMaterialExample example);

    int updateByExample(@Param("record") ProductMaterial record, @Param("example") ProductMaterialExample example);
}