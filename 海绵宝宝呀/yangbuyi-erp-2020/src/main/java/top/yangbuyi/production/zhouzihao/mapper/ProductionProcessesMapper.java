package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.production.zhouzihao.domain.ProductionProcesses;

public interface ProductionProcessesMapper extends BaseMapper<ProductionProcesses> {
}