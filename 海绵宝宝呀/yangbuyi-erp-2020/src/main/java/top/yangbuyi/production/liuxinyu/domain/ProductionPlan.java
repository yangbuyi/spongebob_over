package top.yangbuyi.production.liuxinyu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "production_plan")
public class ProductionPlan implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "apply_id")
    private String applyId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "type")
    private String type;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "cost_price_total")
    private BigDecimal costPriceTotal;

    @TableField(value = "designer")
    private String designer;

    @TableField(value = "remark")
    private String remark;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:dd")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_suggestion")
    private String checkSuggestion;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "manufacture_tag")
    private String manufactureTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_APPLY_ID = "apply_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_TYPE = "type";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_COST_PRICE_TOTAL = "cost_price_total";

    public static final String COL_DESIGNER = "designer";

    public static final String COL_REMARK = "remark";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_SUGGESTION = "check_suggestion";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_MANUFACTURE_TAG = "manufacture_tag";
}