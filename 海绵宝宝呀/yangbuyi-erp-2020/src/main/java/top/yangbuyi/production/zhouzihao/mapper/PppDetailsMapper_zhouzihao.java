package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.PppDetails;
import top.yangbuyi.production.zhouzihao.domain.PppDetailsExample;

public interface PppDetailsMapper_zhouzihao extends BaseMapper<PppDetails> {
    long countByExample(PppDetailsExample example);

    int deleteByExample(PppDetailsExample example);

    List<PppDetails> selectByExample(PppDetailsExample example);

    int updateByExampleSelective(@Param("record") PppDetails record, @Param("example") PppDetailsExample example);

    int updateByExample(@Param("record") PppDetails record, @Param("example") PppDetailsExample example);
}