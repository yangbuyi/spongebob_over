package top.yangbuyi.production.liuxinyu.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Insertrecord extends BaseVo{
      private Integer idp;
      private Integer hourp;
      private String tregister;//登记人

}
