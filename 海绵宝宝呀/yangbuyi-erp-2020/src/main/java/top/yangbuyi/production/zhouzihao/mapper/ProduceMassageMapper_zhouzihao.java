package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.ProduceMassage;
import top.yangbuyi.production.zhouzihao.domain.ProduceMassageExample;

public interface ProduceMassageMapper_zhouzihao extends BaseMapper<ProduceMassage> {
    long countByExample(ProduceMassageExample example);

    int deleteByExample(ProduceMassageExample example);

    List<ProduceMassage> selectByExample(ProduceMassageExample example);

    int updateByExampleSelective(@Param("record") ProduceMassage record, @Param("example") ProduceMassageExample example);

    int updateByExample(@Param("record") ProduceMassage record, @Param("example") ProduceMassageExample example);
}