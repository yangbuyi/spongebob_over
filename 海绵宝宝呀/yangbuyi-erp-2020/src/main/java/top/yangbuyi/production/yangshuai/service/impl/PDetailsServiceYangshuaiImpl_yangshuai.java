package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.PDetails;
import top.yangbuyi.production.yangshuai.domain.PDetailsExample;
import top.yangbuyi.production.yangshuai.mapper.PDetails_yangbuyi_Mapper;
import top.yangbuyi.production.yangshuai.service.PDetailsService_yangshuai;

import java.util.List;
@Service
public class PDetailsServiceYangshuaiImpl_yangshuai extends ServiceImpl<PDetails_yangbuyi_Mapper, PDetails> implements PDetailsService_yangshuai {

    @Override
    public long countByExample(PDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(PDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<PDetails> selectByExample(PDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(PDetails record,PDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(PDetails record,PDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }
}
