package top.yangbuyi.production.liuxinyu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "product_file")
public class ProductFile implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "factory_name")
    private String factoryName;

    @TableField(value = "first_kind_id")
    private String firstKindId;

    @TableField(value = "first_kind_name")
    private String firstKindName;

    @TableField(value = "second_kind_id")
    private String secondKindId;

    @TableField(value = "second_kind_name")
    private String secondKindName;

    @TableField(value = "third_kind_id")
    private String thirdKindId;

    @TableField(value = "third_kind_name")
    private String thirdKindName;

    @TableField(value = "product_nick")
    private String productNick;

    @TableField(value = "type")
    private String type;

    @TableField(value = "product_class")
    private String productClass;

    @TableField(value = "personal_unit")
    private String personalUnit;

    @TableField(value = "personal_value")
    private String personalValue;

    @TableField(value = "provider_group")
    private String providerGroup;

    @TableField(value = "warranty")
    private String warranty;

    @TableField(value = "twin_name")
    private String twinName;

    @TableField(value = "twin_id")
    private String twinId;

    @TableField(value = "lifecycle")
    private String lifecycle;

    @TableField(value = "list_price")
    private BigDecimal listPrice;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "real_cost_price")
    private BigDecimal realCostPrice;

    @TableField(value = "amount_unit")
    private String amountUnit;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "responsible_person")
    private String responsiblePerson;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "changer")
    private String changer;

    @TableField(value = "change_time")
    private Date changeTime;

    @TableField(value = "change_tag")
    private String changeTag;

    @TableField(value = "price_change_tag")
    private String priceChangeTag;

    @TableField(value = "file_change_amount")
    private Integer fileChangeAmount;

    @TableField(value = "delete_tag")
    private String deleteTag;

    @TableField(value = "design_module_tag")
    private String designModuleTag;

    @TableField(value = "design_procedure_tag")
    private String designProcedureTag;

    @TableField(value = "design_cell_tag")
    private String designCellTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_FACTORY_NAME = "factory_name";

    public static final String COL_FIRST_KIND_ID = "first_kind_id";

    public static final String COL_FIRST_KIND_NAME = "first_kind_name";

    public static final String COL_SECOND_KIND_ID = "second_kind_id";

    public static final String COL_SECOND_KIND_NAME = "second_kind_name";

    public static final String COL_THIRD_KIND_ID = "third_kind_id";

    public static final String COL_THIRD_KIND_NAME = "third_kind_name";

    public static final String COL_PRODUCT_NICK = "product_nick";

    public static final String COL_TYPE = "type";

    public static final String COL_PRODUCT_CLASS = "product_class";

    public static final String COL_PERSONAL_UNIT = "personal_unit";

    public static final String COL_PERSONAL_VALUE = "personal_value";

    public static final String COL_PROVIDER_GROUP = "provider_group";

    public static final String COL_WARRANTY = "warranty";

    public static final String COL_TWIN_NAME = "twin_name";

    public static final String COL_TWIN_ID = "twin_id";

    public static final String COL_LIFECYCLE = "lifecycle";

    public static final String COL_LIST_PRICE = "list_price";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_REAL_COST_PRICE = "real_cost_price";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_RESPONSIBLE_PERSON = "responsible_person";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_CHANGER = "changer";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHANGE_TAG = "change_tag";

    public static final String COL_PRICE_CHANGE_TAG = "price_change_tag";

    public static final String COL_FILE_CHANGE_AMOUNT = "file_change_amount";

    public static final String COL_DELETE_TAG = "delete_tag";

    public static final String COL_DESIGN_MODULE_TAG = "design_module_tag";

    public static final String COL_DESIGN_PROCEDURE_TAG = "design_procedure_tag";

    public static final String COL_DESIGN_CELL_TAG = "design_cell_tag";
}