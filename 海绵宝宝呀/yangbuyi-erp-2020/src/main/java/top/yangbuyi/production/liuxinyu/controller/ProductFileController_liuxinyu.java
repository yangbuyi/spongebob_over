package top.yangbuyi.production.liuxinyu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.liuxinyu.domain.ProductFile;
import top.yangbuyi.production.liuxinyu.domain.ProductMaterialDetails;
import top.yangbuyi.production.liuxinyu.service.ProductFileService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.ProductFileVo;
import top.yangbuyi.system.common.ResultObj;
import top.yangbuyi.system.common.ResultObj1;

import java.math.BigDecimal;
import java.util.List;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@RestController
@RequestMapping("api/liuxinyu/file")
public class ProductFileController_liuxinyu {

      @Autowired
      private ProductFileService_liuxinyu productFileServiceLiuxinyu;


      @RequestMapping("selAllFile")
      public Object selAllFile(ProductFileVo productFileVo) {
            System.out.println(productFileVo);
            return productFileServiceLiuxinyu.querySelAllFile(productFileVo);
      }

      @RequestMapping("selMaterialAmount")
      @ResponseBody
      public Object selMaterialAmount(@RequestParam String productId,@RequestParam BigDecimal amount){
            System.out.println("编号"+productId+"数量"+amount);
            List<ProductMaterialDetails> productMaterialDetails=this.productFileServiceLiuxinyu.selMaterial(productId);
            String rt="";
            ResultObj1 resultObj=new ResultObj1();
            Integer codeback=100;
            for (ProductMaterialDetails productMaterialDetail : productMaterialDetails) {
//                bigdecimal转int
                  Integer mAmount =productMaterialDetail.getAmount().intValue();
                  System.out.println("负数就是库存不足"+(this.productFileServiceLiuxinyu.selMaterialAmount(productMaterialDetail.getProductId())-mAmount*amount.intValue()));
                  Integer kc= this.productFileServiceLiuxinyu.selMaterialAmount(productMaterialDetail.getProductId())-mAmount*amount.intValue();
                  if (kc>0){
                        rt=rt+productMaterialDetail.getProductName()+"数量充足，";
                  }else{
                        rt=rt+"你需要进货"+(-kc)+"个"+productMaterialDetail.getProductName()+"物料，";
                        codeback=codeback+1;
                        System.out.println("我也是醉了"+codeback);
                  }
            }
            System.out.println(codeback);
            try {
                  System.out.println(codeback);
                  resultObj=new ResultObj1(200,rt,codeback);
                  return resultObj;
            } catch (Exception e) {
                  return ResultObj.AMOUNT_ERROR;
            }
      }

      @RequestMapping("selByIdFile")
      public Object selFileById(Integer id){
            ProductFile productFile=this.productFileServiceLiuxinyu.getById(id);
            return productFile;
      }
}
