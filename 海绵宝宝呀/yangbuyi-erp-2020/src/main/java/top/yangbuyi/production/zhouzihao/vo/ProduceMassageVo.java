package top.yangbuyi.production.zhouzihao.vo;

import lombok.Data;

@Data
public class ProduceMassageVo {
      private String amount;
      private String applyIdGroup;
      private String designer;
      private String labourCostPriceSum;
      private String moduleCostPriceSum;
      private String productId;
      private String productName;
      private String register;
      private String registerTime;
      private String sum;
      private String testedAmount;
      private String manufactureId;

}
