package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.ProductFile;
import top.yangbuyi.production.zhouzihao.domain.ProductFileExample;

public interface ProductFileMapper_zhouzihao extends BaseMapper<ProductFile> {
    long countByExample(ProductFileExample example);

    int deleteByExample(ProductFileExample example);

    List<ProductFile> selectByExample(ProductFileExample example);

    int updateByExampleSelective(@Param("record") ProductFile record, @Param("example") ProductFileExample example);

    int updateByExample(@Param("record") ProductFile record, @Param("example") ProductFileExample example);
}