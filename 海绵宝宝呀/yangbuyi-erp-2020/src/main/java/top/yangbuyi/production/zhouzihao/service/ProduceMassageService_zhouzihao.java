package top.yangbuyi.production.zhouzihao.service;

import java.text.ParseException;
import java.util.List;
import top.yangbuyi.production.zhouzihao.domain.ProduceMassageExample;
import top.yangbuyi.production.zhouzihao.domain.ProduceMassage;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.zhouzihao.vo.ProduceMassageVo;
import top.yangbuyi.system.common.DataGridView;

public interface ProduceMassageService_zhouzihao extends IService<ProduceMassage>{


    long countByExample(ProduceMassageExample example);

    int deleteByExample(ProduceMassageExample example);

    List<ProduceMassage> selectByExample(ProduceMassageExample example);

    int updateByExampleSelective(ProduceMassage record, ProduceMassageExample example);

    int updateByExample(ProduceMassage record, ProduceMassageExample example);

      int insProduceMassage(ProduceMassageVo produceMassage) throws ParseException;

      DataGridView findAllPage(Integer page, Integer limit, String status);

      int Audit(Integer id, String status);
}
