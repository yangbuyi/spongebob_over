package top.yangbuyi.production.zhouzihao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.zhouzihao.domain.*;
import top.yangbuyi.production.zhouzihao.mapper.*;
import top.yangbuyi.production.zhouzihao.service.ProductProductionProcessService_zhouzihao;
import top.yangbuyi.production.zhouzihao.vo.ProductionProcess;
import top.yangbuyi.production.zhouzihao.vo.ProductionProcessVo;
import top.yangbuyi.production.zhouzihao.vo.pDtest;
import top.yangbuyi.system.common.DataGridView;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class ProductProductionProcessServiceImpl extends ServiceImpl<ProductProductionProcessMapper_zhouzihao, ProductProductionProcess> implements ProductProductionProcessService_zhouzihao {

      @Autowired
      private ProductFileMapper_zhouzihao productFileMapper;
      @Autowired
      private ProductProductionProcessMapper_zhouzihao productProductionProcessMapper;

      @Autowired
      private ProductMaterialMapper_zhouzihao productMaterialMapper;
      @Autowired
      private PppDetailsMapper_zhouzihao pppDetailsMapper;
      @Autowired
      private ProductionProcessesMapper productionProcessesMapper;

    @Override
    public long countByExample(ProductProductionProcessExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductProductionProcessExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductProductionProcess> selectByExample(ProductProductionProcessExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductProductionProcess record,ProductProductionProcessExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductProductionProcess record,ProductProductionProcessExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public Integer insProductionProcess() {
            return null;
      }

      /**
       * 设计产品工序总业务
       * @param productionProcess
       * @return
       */
      @Override
      public int insProductionProcess(ProductionProcess productionProcess) {
            System.err.println(productionProcess);
          //先查询出传进的id的那条数据
            ProductFile productFile = productFileMapper.selectById(productionProcess.getId());
            System.err.println(productFile);
            //添加到product_production_process表
            ProductProductionProcess pr = new ProductProductionProcess();
            pr.setDesignId(productionProcess.getEnact());
            pr.setFirstKindId(productFile.getFirstKindId());
            pr.setFirstKindName(productFile.getFirstKindName());
            pr.setSecondKindId(productFile.getSecondKindId());
            pr.setSecondKindName(productFile.getSecondKindName());
            pr.setThirdKindId(productFile.getThirdKindId());
            pr.setThirdKindName(productFile.getThirdKindName());
            pr.setProductId(productFile.getProductId());
            pr.setProductName(productFile.getProductName());
            pr.setProcedureDescribe(productionProcess.getDesc());
            pr.setCostPriceSum(BigDecimal.valueOf(productionProcess.getProcessSum()));

            QueryWrapper<ProductMaterial> wrapper = new QueryWrapper<>();
            wrapper.eq(StringUtils.isNotBlank(productFile.getProductId()),"product_id",productFile.getProductId());
            ProductMaterial productMaterial = productMaterialMapper.selectOne(wrapper);
            System.err.println(productMaterial);

            pr.setModuleCostPriceSum(productMaterial.getCostPriceSum());
            pr.setDesigner(productFile.getRegister());
            pr.setRegister(productionProcess.getDoPeople());
            pr.setRegisterTime(productFile.getRegisterTime());
            pr.setChecker(productionProcess.getDoPeople());
            pr.setCheckTime(new Date());
            pr.setCheckTag("S001-1");
            pr.setChangeTag(productFile.getChangeTag());
            pr.setDesignModuleTag("G002-1");
            pr.setDesignModuleChangeTag("G003-1");
            int insert = productProductionProcessMapper.insert(pr);
            //获取刚刚添加数据的id，关联生成所需工序
            for (pDtest pDtest : productionProcess.getpDtest()) {
                  PppDetails pppDetails =null;
                  pppDetails=new PppDetails();
                  pppDetails.setParentId(pr.getId());
                  pppDetails.setDetailsNumber(pDtest.getProcedureId());
                  pppDetails.setProcedureId(String.valueOf(pDtest.getProcedureId()));
                  pppDetails.setProcedureName(pDtest.getProcedureName());
                  pppDetails.setLabourHourAmount(BigDecimal.valueOf(pDtest.getLabourHourAmount()));
                  pppDetails.setProcedureDescribe(pDtest.getProcedureDescribe());
                  pppDetails.setAmountUnit("h");
                  pppDetails.setCostPrice(BigDecimal.valueOf(pDtest.getCostPrice()));
                  pppDetails.setSubtotal(BigDecimal.valueOf(pDtest.getLabourHourAmount()*pDtest.getCostPrice()));
                  pppDetails.setRegisterTime(new Date());
                  pppDetails.setDesignModuleTag("D002-1");
                  pppDetails.setDesignModuleChangeTag("B003-1");
                  pppDetailsMapper.insert(pppDetails);
            }

            //改变product_file表工序组成状态字段
            ProductFile productFile1 = new ProductFile();
            productFile1.setId(productionProcess.getId());
            productFile1.setDesignProcedureTag("G001-2");
            QueryWrapper<ProductFile> fileQueryWrapper = new QueryWrapper<>();
            fileQueryWrapper.eq("id",productionProcess.getId());
            int i = productFileMapper.update(productFile1,fileQueryWrapper);
            return insert;
      }

      @Override
      public DataGridView quDataGridView(String productId) {
            return null;
      }

      @Override
      public List<ProductProductionProcess> findNotAuditPage(ProductFileExample example) {
            QueryWrapper<ProductProductionProcess> wrapper = new QueryWrapper<>();
            wrapper.eq("check_tag","S001-1");
            List<ProductProductionProcess> productProductionProcesses = baseMapper.selectList(wrapper);
            return productProductionProcesses;
      }

      @Override
      public int updateAuditStatus(Integer id, String status) {
            ProductProductionProcess process = new ProductProductionProcess();
            process.setId(id);
            process.setCheckTag(status);
            QueryWrapper<ProductProductionProcess> wrapper = new QueryWrapper<>();
            wrapper.eq("id",id);
            return baseMapper.update(process,wrapper);
      }

      @Override
      public DataGridView selectAll(Integer page, Integer limit, ProductionProcessVo processVo) {
            QueryWrapper<ProductProductionProcess> wrapper = new QueryWrapper<>();
            wrapper.eq(StringUtils.isNotBlank(processVo.getProductId()),"product_id",processVo.getProductId());
            wrapper.eq(StringUtils.isNotBlank(processVo.getDesignId()),"design_id",processVo.getDesignId());
            wrapper.like(StringUtils.isNotBlank(processVo.getProductName()),"product_name",processVo.getProductName());
            Page<ProductProductionProcess> p = new Page<>(page,limit);
            IPage<ProductProductionProcess> processIPage = baseMapper.selectPage(p,wrapper);
            return new DataGridView(processIPage.getTotal(),processIPage.getRecords());
      }

      /**
       * 重新审核工序
       * @param id
       * @param productionProcess
       * @return
       */
      @Override
      public int toResubmit(Integer id, ProductionProcess productionProcess) {
            //删除产品绑定的工序
            QueryWrapper<PppDetails> wrapper = new QueryWrapper<>();
            wrapper.eq("parent_id",id);
            int delete = pppDetailsMapper.delete(wrapper);
            if (delete>0){
                  //删除成功后再重新添加
                  //重新添加修改后的工序
                  for (pDtest pDtest : productionProcess.getpDtest()) {
                        PppDetails pppDetails =null;
                        pppDetails=new PppDetails();
                        pppDetails.setParentId(id);
                        pppDetails.setDetailsNumber(pDtest.getProcedureId());
                        pppDetails.setProcedureId(String.valueOf(pDtest.getProcedureId()));
                        pppDetails.setProcedureName(pDtest.getProcedureName());
                        pppDetails.setLabourHourAmount(BigDecimal.valueOf(pDtest.getLabourHourAmount()));
                        pppDetails.setProcedureDescribe(pDtest.getProcedureDescribe());
                        pppDetails.setAmountUnit("h");
                        pppDetails.setCostPrice(BigDecimal.valueOf(pDtest.getCostPrice()));
                        pppDetails.setSubtotal(BigDecimal.valueOf(pDtest.getLabourHourAmount()*pDtest.getCostPrice()));
                        pppDetails.setRegisterTime(new Date());
                        pppDetails.setDesignModuleTag("D002-1");
                        pppDetails.setDesignModuleChangeTag("B003-1");
                        pppDetailsMapper.insert(pppDetails);
                  }
            }
            //修改审核状态为待审核状态
            int i = this.updateAuditStatus(id, "S001-1");

            QueryWrapper<ProductProductionProcess> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("id",id);
            ProductProductionProcess process = new ProductProductionProcess();
            process.setCostPriceSum(BigDecimal.valueOf(productionProcess.getProcessSum()));
            int update = baseMapper.update(process,wrapper1);
            return update;
      }

      @Override
      public ProductProductionProcess findById(String id) {
            QueryWrapper<ProductProductionProcess> wrapper = new QueryWrapper<ProductProductionProcess>();
            wrapper.eq("product_id",id);
            ProductProductionProcess productProductionProcess = baseMapper.selectOne(wrapper);
            return productProductionProcess;
      }
}
