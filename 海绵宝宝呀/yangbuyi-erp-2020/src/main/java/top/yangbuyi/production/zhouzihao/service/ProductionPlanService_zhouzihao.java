package top.yangbuyi.production.zhouzihao.service;

import top.yangbuyi.production.zhouzihao.domain.ProductionPlanExample;
import java.util.List;
import top.yangbuyi.production.zhouzihao.domain.ProductionPlan;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.common.DataGridView;

public interface ProductionPlanService_zhouzihao extends IService<ProductionPlan>{


    long countByExample(ProductionPlanExample example);

    int deleteByExample(ProductionPlanExample example);

    List<ProductionPlan> selectByExample(ProductionPlanExample example);

    int updateByExampleSelective(ProductionPlan record, ProductionPlanExample example);

    int updateByExample(ProductionPlan record, ProductionPlanExample example);

      DataGridView findAllPage(Integer page, Integer limit);
}
