package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcess;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcessExample;
import top.yangbuyi.production.yangshuai.domain.ProductionProcessesExample;

import java.util.List;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/7/16
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface ProductProductionProcess_yangshuai_Mapper extends BaseMapper<ProductProductionProcess> {
      long countByExample(ProductProductionProcessExample example);
      
      int deleteByExample(ProductProductionProcessExample example);
      
      List<ProductProductionProcess> selectByExample(ProductProductionProcessExample example);
      
      int updateByExampleSelective(@Param("record") ProductProductionProcess record, @Param("example") ProductProductionProcessExample example);
      
      int updateByExample(@Param("record") ProductProductionProcess record, @Param("example") ProductProductionProcessExample example);
      
      List<ProductProductionProcess> selectByExamples(ProductProductionProcess productionProcessesExample);
      
      void updateProductionProcesses(ProductProductionProcess process);
}