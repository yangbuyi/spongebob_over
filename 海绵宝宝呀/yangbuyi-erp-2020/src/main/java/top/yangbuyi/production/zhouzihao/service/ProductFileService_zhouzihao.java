package top.yangbuyi.production.zhouzihao.service;

import java.util.List;
import top.yangbuyi.production.zhouzihao.domain.ProductFile;
import top.yangbuyi.production.zhouzihao.domain.ProductFileExample;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcess;

public interface ProductFileService_zhouzihao extends IService<ProductFile>{


    long countByExample(ProductFileExample example);

    int deleteByExample(ProductFileExample example);

    List<ProductFile> selectByExample(ProductFileExample example);

    int updateByExampleSelective(ProductFile record, ProductFileExample example);

    int updateByExample(ProductFile record, ProductFileExample example);


      List<ProductFile> selectAll(ProductFileExample example);
}
