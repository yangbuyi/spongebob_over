package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetailsExample;

import java.util.List;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface PppMaterialDetails_yangshuai_Mapper extends BaseMapper<PppMaterialDetails> {
    long countByExample(PppMaterialDetailsExample example);

    int deleteByExample(PppMaterialDetailsExample example);

    List<PppMaterialDetails> selectByExample(PppMaterialDetailsExample example);

    int updateByExampleSelective(@Param("record") PppMaterialDetails record, @Param("example") PppMaterialDetailsExample example);

    int updateByExample(@Param("record") PppMaterialDetails record, @Param("example") PppMaterialDetailsExample example);
      
      void insertPppMaterialDetails(PppMaterialDetails pppMaterialDetails);
}