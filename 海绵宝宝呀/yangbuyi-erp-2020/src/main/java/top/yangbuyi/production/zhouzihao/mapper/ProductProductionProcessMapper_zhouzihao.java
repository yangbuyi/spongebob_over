package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcess;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcessExample;

public interface ProductProductionProcessMapper_zhouzihao extends BaseMapper<ProductProductionProcess> {
    long countByExample(ProductProductionProcessExample example);

    int deleteByExample(ProductProductionProcessExample example);

    List<ProductProductionProcess> selectByExample(ProductProductionProcessExample example);

    int updateByExampleSelective(@Param("record") ProductProductionProcess record, @Param("example") ProductProductionProcessExample example);

    int updateByExample(@Param("record") ProductProductionProcess record, @Param("example") ProductProductionProcessExample example);
}