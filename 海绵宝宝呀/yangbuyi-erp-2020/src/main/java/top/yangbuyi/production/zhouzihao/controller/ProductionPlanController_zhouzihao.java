package top.yangbuyi.production.zhouzihao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.zhouzihao.service.ProductionPlanService_zhouzihao;
import top.yangbuyi.system.common.DataGridView;

@RestController
@RequestMapping("api/zhouzihao/ProductionPlan")
public class ProductionPlanController_zhouzihao {

      @Autowired
      private ProductionPlanService_zhouzihao productionPlanService;

      @RequestMapping("findAllPage")
      public Object findAllPage(Integer page,Integer limit){
            DataGridView all=productionPlanService.findAllPage(page,limit);
            return all;

      }

}
