package top.yangbuyi.production.yangshuai.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/16
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "product_material")
public class ProductMaterial implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "design_id")
    private String designId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "first_kind_id")
    private String firstKindId;

    @TableField(value = "first_kind_name")
    private String firstKindName;

    @TableField(value = "second_kind_id")
    private String secondKindId;

    @TableField(value = "second_kind_name")
    private String secondKindName;

    @TableField(value = "third_kind_id")
    private String thirdKindId;

    @TableField(value = "third_kind_name")
    private String thirdKindName;

    @TableField(value = "designer")
    private String designer;

    @TableField(value = "module_describe")
    private String moduleDescribe;

    @TableField(value = "cost_price_sum")
    private BigDecimal costPriceSum;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "changer")
    private String changer;

    @TableField(value = "change_time")
    private Date changeTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "change_tag")
    private String changeTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_DESIGN_ID = "design_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_FIRST_KIND_ID = "first_kind_id";

    public static final String COL_FIRST_KIND_NAME = "first_kind_name";

    public static final String COL_SECOND_KIND_ID = "second_kind_id";

    public static final String COL_SECOND_KIND_NAME = "second_kind_name";

    public static final String COL_THIRD_KIND_ID = "third_kind_id";

    public static final String COL_THIRD_KIND_NAME = "third_kind_name";

    public static final String COL_DESIGNER = "designer";

    public static final String COL_MODULE_DESCRIBE = "module_describe";

    public static final String COL_COST_PRICE_SUM = "cost_price_sum";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHANGER = "changer";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_CHANGE_TAG = "change_tag";
}