package top.yangbuyi.production.yangshuai.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.yangshuai.domain.FileupdateExample;
import top.yangbuyi.production.yangshuai.domain.PppDetails;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcess;
import top.yangbuyi.production.yangshuai.service.*;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.ResultObj;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * @Description: 杨不易个人网址:http://yangbuyi.top
 * 功能描述: 制定工序物料设计单
 * @Param: ProcessMaterialDesignController
 * @return:
 * @Author: TeouBle
 * @Date: 2020/7/16 11:18
 */

@RestController
@Slf4j
@RequestMapping("api/yangshuai/ProcessMaterialDesign")
public class ProcessMaterialDesignController_yangshuai {
      
      @Autowired
      private ProductMaterialService_yangshuai productMaterialServiceYangshuai;
      @Autowired
      private ProductMaterialDetailsService_yangshuai productMaterialDetailsServiceYangshuai;
      /**
       * 因为这个表是三个表组成的数据
       * 当前操作表为 Production
       */
      @Autowired
      private ProductProductionProcessService_yangshuai productProductionProcessServiceYangshuai;
      
      @Autowired
      private PppDetailsService_yangshuai pppDetailsServiceYangshuai;
      
      
      @Autowired
      private ProductionProcessesService_yangshuai productionProcessesServiceYangshuai;
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述: 查询物料工序  制定工序物料设计单
       * @Param: processesExample
       * @return: 物料工序
       * @Author: TeouBle
       * @Date: 2020/7/17 9:37
       */
      @GetMapping("loadProcesses")
      public Object listwlProcesses(ProductProductionProcess productionProcessesExample) throws ParseException {
            List<ProductProductionProcess> processes = productProductionProcessServiceYangshuai.selectProcessWlList(productionProcessesExample);
            return new DataGridView(Long.valueOf(processes.size()), processes);
      }
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述:
       * @Param: 工序物料设计单详细
       * @return:
       * @Author: TeouBle
       * @Date: 2020/7/17 10:18
       */
      @GetMapping("loadProDetails")
      public Object listwlProcessesDetails(String parentId) throws ParseException {
            List<PppDetails> processes = productProductionProcessServiceYangshuai.getProcessesList(parentId);
            return new DataGridView(Long.valueOf(processes.size()), processes);
      }
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述:
       * @Param: 设计组装包装的物料设计 productId 产品ID
       * @return:
       * @Author: TeouBle
       * @Date: 2020/7/17 16:41
       */
      @GetMapping("loadProMaterial")
      public Object selectProMaterial(@Param("productId") String productId) {
            List<ProductMaterialDetails> materialDetails = productMaterialDetailsServiceYangshuai.getProductMaterialDetailsList(productId);
            return new DataGridView(Long.valueOf(materialDetails.size()), materialDetails);
      }
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述:
       * @Param: 更新物料工序总价
       * @return:
       * @Author: TeouBle
       * @Date: 2020/7/19 13:43
       */
      @RequestMapping("updateDetails")
      public ResultObj updateDetailscotr(PppDetails pppDetails) {
            try {
                  
                  // 判断是否为一样的工序
             /*     pppDetailsServiceYangshuai.getById(pp)
                  if(pppDetails.getParentId()){
                  
                  }
                  */
                  log.warn("updateDetails:    " + pppDetails);
                  pppDetailsServiceYangshuai.updateDetails(pppDetails);
                  return ResultObj.UPDATE_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.UPDATE_ERROR;
            }
      }
      
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述:
       * @Param: 工序物料修改物料总成本 标识
       * @return:
       * @Author: TeouBle
       * @Date: 2020/7/20 16:10
       */
      @PostMapping("/updatePPProcess")
      public ResultObj updatePPProcess(ProductProductionProcess process) throws ParseException {
            try {
                  // 修改 物料总成标识
                  process.setDesignModuleTag("G002-2");
                  System.out.println(process);
                  this.productMaterialDetailsServiceYangshuai.updateProductionProcesses(process);
                  return ResultObj.SUBMIT_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.SUBMIT_ERROR;
            }
      }
      
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述:
       * @Param: 提交组装包装的物料设计
       * @return:
       * @Author: TeouBle
       * @Date: 2020/7/17 13:59
       */
      @RequestMapping("addProMater")
      public ResultObj addProMaterialDetail(ProductMaterialDetails productMaterialDetails) {
            try {
                  log.warn("addProMater:    " + productMaterialDetails);
                  // 执行添加组装包装的物料设计数据
                  productMaterialDetailsServiceYangshuai.addPppMaterialDetails(productMaterialDetails);
                  return ResultObj.ADD_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.ADD_ERROR;
            }
            
      }
      
      
      /**
       * @Description: 杨不易个人网址:http://yangbuyi.top
       * 功能描述:
       * @Param: 提交物料工序复核
       * @return:
       * @Author: TeouBle
       * @Date: 2020/7/20 16:10
       */
      @PostMapping("/updateMaterialProcesses")
      public ResultObj updateMaterialProcesses(ProductProductionProcess process, FileupdateExample fileupdateExample) throws ParseException {
            try {
                  // 修改产品金额
                  productionProcessesServiceYangshuai.updateRealCostPrice(fileupdateExample);
                  process.setCheckTime(new Date());
                  process.setDesignModuleTag("G002-3");
                  // 修改物料总成金额 和 审核状态
                  productMaterialDetailsServiceYangshuai.updateProductionProcesses(process);
                  return ResultObj.SUBMIT_SUCCESS;
            } catch (Exception e) {
                  e.printStackTrace();
                  return ResultObj.SUBMIT_ERROR;
            }
            
      }
      
      
}
