package top.yangbuyi.production.liuxinyu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.liuxinyu.domain.ProductionPlan;
import top.yangbuyi.production.liuxinyu.domain.ProductionPlanExample;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@Mapper
public interface ProductionPlan_liuxinyu_Mapper extends BaseMapper<ProductionPlan> {
      long countByExample(ProductionPlanExample example);

      int deleteByExample(ProductionPlanExample example);

      List<ProductionPlan> selectByExample(ProductionPlanExample example);

      int updateByExampleSelective(@Param("record") ProductionPlan record, @Param("example") ProductionPlanExample example);

      int updateByExample(@Param("record") ProductionPlan record, @Param("example") ProductionPlanExample example);

      int batchNoPass(@Param("ids") String[] ids, @Param("checker") String checker, @Param("check_time") Date checkTime);

      int batchPass(@Param("ids") String[] ids, @Param("checker") String checker, @Param("check_time") Date checkTime);
}