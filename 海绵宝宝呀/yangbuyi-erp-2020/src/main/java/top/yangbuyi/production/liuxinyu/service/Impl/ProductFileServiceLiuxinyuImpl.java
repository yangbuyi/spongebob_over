package top.yangbuyi.production.liuxinyu.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.liuxinyu.domain.ProductMaterialDetails;
import top.yangbuyi.production.liuxinyu.mapper.ProductFile_liuxinyu_Mapper;
import top.yangbuyi.production.liuxinyu.domain.ProductFile;
import top.yangbuyi.production.liuxinyu.domain.ProductFileExample;
import top.yangbuyi.production.liuxinyu.service.ProductFileService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.ProductFileVo;
import top.yangbuyi.system.common.DataGridView;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Service
public class ProductFileServiceLiuxinyuImpl extends ServiceImpl<ProductFile_liuxinyu_Mapper, ProductFile> implements ProductFileService_liuxinyu {

    @Override
    public long countByExample(ProductFileExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductFileExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductFile> selectByExample(ProductFileExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductFile record,ProductFileExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductFile record,ProductFileExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public DataGridView querySelAllFile(ProductFileVo productFileVo) {
            productFileVo.setCheck_tag("S001-2");
            productFileVo.setDelete_tag("C001-1");
            productFileVo.setType("Y001-1");
            productFileVo.setDesign_module_tag("W001-2");
            productFileVo.setDesign_procedure_tag("G001-2");
            IPage<ProductFile> page=new Page<>(productFileVo.getPage(),productFileVo.getLimit());

            QueryWrapper<ProductFile> productFileQueryWrapper=new QueryWrapper<>();

            productFileQueryWrapper.eq(StringUtils.isNotBlank(productFileVo.getDesign_procedure_tag()),"design_procedure_tag",productFileVo.getDesign_procedure_tag());
            productFileQueryWrapper.eq(StringUtils.isNotBlank(productFileVo.getDesign_module_tag()),"design_module_tag",productFileVo.getDesign_module_tag());
            productFileQueryWrapper.like(StringUtils.isNotBlank(productFileVo.getProduct_name()),"product_name",productFileVo.getProduct_name());
            productFileQueryWrapper.eq(StringUtils.isNotBlank(productFileVo.getFirst_kind_id()),"first_kind_id",productFileVo.getFirst_kind_id());
            productFileQueryWrapper.eq(StringUtils.isNotBlank(productFileVo.getCheck_tag()),"check_tag",productFileVo.getCheck_tag());
            productFileQueryWrapper.eq(StringUtils.isNotBlank(productFileVo.getDelete_tag()),"delete_tag",productFileVo.getDelete_tag());
            productFileQueryWrapper.eq(StringUtils.isNotBlank(productFileVo.getType()),"type",productFileVo.getType());


            IPage<ProductFile> page1=this.baseMapper.selectPage(page,productFileQueryWrapper);

            return new DataGridView(page1.getTotal(),page1.getRecords());
      }

      @Override
      public List<ProductMaterialDetails> selMaterial(String productId) {
            return baseMapper.selMaterial(productId);
      }

      @Override
      public int selMaterialAmount(String productId) {
            return baseMapper.selMaterialAmount(productId);
      }


}
