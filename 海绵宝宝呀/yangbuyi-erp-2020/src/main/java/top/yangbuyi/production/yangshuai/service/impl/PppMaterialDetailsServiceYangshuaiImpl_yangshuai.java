package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetailsExample;
import top.yangbuyi.production.yangshuai.mapper.PppMaterialDetails_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.service.PppMaterialDetailsService_yangshuai;

import java.util.List;
/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

@Service
public class PppMaterialDetailsServiceYangshuaiImpl_yangshuai extends ServiceImpl<PppMaterialDetails_yangshuai_Mapper, PppMaterialDetails> implements PppMaterialDetailsService_yangshuai {

    @Override
    public long countByExample(PppMaterialDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(PppMaterialDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<PppMaterialDetails> selectByExample(PppMaterialDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(PppMaterialDetails record,PppMaterialDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(PppMaterialDetails record,PppMaterialDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }
}
