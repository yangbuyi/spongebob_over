package top.yangbuyi.production.zhouzihao.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ppp_details")
public class PppDetails implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "details_number")
    private Integer detailsNumber;

    @TableField(value = "procedure_id")
    private String procedureId;

    @TableField(value = "procedure_name")
    private String procedureName;

    @TableField(value = "labour_hour_amount")
    private BigDecimal labourHourAmount;

    @TableField(value = "procedure_describe")
    private String procedureDescribe;

    @TableField(value = "amount_unit")
    private String amountUnit;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    @TableField(value = "module_subtotal")
    private BigDecimal moduleSubtotal;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "design_module_tag")
    private String designModuleTag;

    @TableField(value = "design_module_change_tag")
    private String designModuleChangeTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_DETAILS_NUMBER = "details_number";

    public static final String COL_PROCEDURE_ID = "procedure_id";

    public static final String COL_PROCEDURE_NAME = "procedure_name";

    public static final String COL_LABOUR_HOUR_AMOUNT = "labour_hour_amount";

    public static final String COL_PROCEDURE_DESCRIBE = "procedure_describe";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_MODULE_SUBTOTAL = "module_subtotal";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_DESIGN_MODULE_TAG = "design_module_tag";

    public static final String COL_DESIGN_MODULE_CHANGE_TAG = "design_module_change_tag";
}