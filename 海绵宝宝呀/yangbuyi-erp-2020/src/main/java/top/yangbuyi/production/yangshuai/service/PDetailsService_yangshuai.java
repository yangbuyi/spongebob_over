package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.yangshuai.domain.PDetails;
import top.yangbuyi.production.yangshuai.domain.PDetailsExample;

import java.util.List;
public interface PDetailsService_yangshuai extends IService<PDetails>{


    long countByExample(PDetailsExample example);

    int deleteByExample(PDetailsExample example);

    List<PDetails> selectByExample(PDetailsExample example);

    int updateByExampleSelective(PDetails record, PDetailsExample example);

    int updateByExample(PDetails record, PDetailsExample example);

}
