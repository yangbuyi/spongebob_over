package top.yangbuyi.production.liuxinyu.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.liuxinyu.domain.ProductionPlan;
import top.yangbuyi.production.liuxinyu.domain.ProductionPlanExample;
import top.yangbuyi.production.liuxinyu.mapper.ProductionPlan_liuxinyu_Mapper;
import top.yangbuyi.production.liuxinyu.service.ProductionPlanService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.ProductionPlanVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.Date;
import java.util.List;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Service
public class ProductionPlanServiceLiuxinyuImpl extends ServiceImpl<ProductionPlan_liuxinyu_Mapper, ProductionPlan> implements ProductionPlanService_liuxinyu {

    @Override
    public long countByExample(ProductionPlanExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductionPlanExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductionPlan> selectByExample(ProductionPlanExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductionPlan record,ProductionPlanExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductionPlan record,ProductionPlanExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public DataGridView querySelAllPlan(ProductionPlanVo productionPlanVo) {
//            productionPlanVo.setCheck_tag("S001-1");
            IPage<ProductionPlan> page = new Page<>(productionPlanVo.getPage(), productionPlanVo.getLimit());
            // 创建条件
            QueryWrapper<ProductionPlan> productionPlanQueryWrapper = new QueryWrapper<>();

            productionPlanQueryWrapper.eq(StringUtils.isNotBlank(productionPlanVo.getCheck_tag()),"check_tag",productionPlanVo.getCheck_tag());
            productionPlanQueryWrapper.
                  like(StringUtils.isNotBlank(productionPlanVo.getProduct_name()),"product_name",productionPlanVo.getProduct_name());
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
//            System.out.println();// new Date()为获取当前系统时间
            productionPlanQueryWrapper.ge(productionPlanVo.getRegisterTime() != null,"register_time",productionPlanVo.getRegisterTime());
//            productionPlanQueryWrapper.le(productionPlanVo.getRegisterTime() != null,"register_time",df.format(new Date()));
            System.out.println(productionPlanVo.getRegisterTime());


            // 执行条件
            IPage<ProductionPlan> productionPlanIPage = this.baseMapper.selectPage(page, productionPlanQueryWrapper);

            return new DataGridView(productionPlanIPage.getTotal(),productionPlanIPage.getRecords());
      }



      @Override
      public int batchNoPass(String[] ids,String checker,Date checkTime) {

            return baseMapper.batchNoPass(ids,checker,checkTime);
      }

      @Override
      public int batchPass(String[] ids,String checker,Date checkTime) {
            return baseMapper.batchPass(ids, checker,checkTime);
      }

      @Override
      public ProductionPlan savePlan(ProductionPlan productionPlan) {
          baseMapper.insert(productionPlan);
          return productionPlan;
      }
}
