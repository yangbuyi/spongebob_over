package top.yangbuyi.production.liuxinyu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "produce_massage")
public class ProduceMassage implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "manufacture_id")
    private String manufactureId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "tested_amount")
    private BigDecimal testedAmount;

    @TableField(value = "apply_id_group")
    private String applyIdGroup;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "module_cost_price_sum")
    private BigDecimal moduleCostPriceSum;

    @TableField(value = "real_module_cost_price_sum")
    private BigDecimal realModuleCostPriceSum;

    @TableField(value = "labour_cost_price_sum")
    private BigDecimal labourCostPriceSum;

    @TableField(value = "real_labour_cost_price_sum")
    private BigDecimal realLabourCostPriceSum;

    @TableField(value = "designer")
    private String designer;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "remark")
    private String remark;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "manufacture_procedure_tag")
    private String manufactureProcedureTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_MANUFACTURE_ID = "manufacture_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_TESTED_AMOUNT = "tested_amount";

    public static final String COL_APPLY_ID_GROUP = "apply_id_group";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_MODULE_COST_PRICE_SUM = "module_cost_price_sum";

    public static final String COL_REAL_MODULE_COST_PRICE_SUM = "real_module_cost_price_sum";

    public static final String COL_LABOUR_COST_PRICE_SUM = "labour_cost_price_sum";

    public static final String COL_REAL_LABOUR_COST_PRICE_SUM = "real_labour_cost_price_sum";

    public static final String COL_DESIGNER = "designer";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_REMARK = "remark";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_MANUFACTURE_PROCEDURE_TAG = "manufacture_procedure_tag";
}