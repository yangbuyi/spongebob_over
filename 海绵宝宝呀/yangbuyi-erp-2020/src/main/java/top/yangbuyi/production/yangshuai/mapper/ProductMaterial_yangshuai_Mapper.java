package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.production.yangshuai.domain.ProductMaterial;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/16
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface ProductMaterial_yangshuai_Mapper extends BaseMapper<ProductMaterial> {
}