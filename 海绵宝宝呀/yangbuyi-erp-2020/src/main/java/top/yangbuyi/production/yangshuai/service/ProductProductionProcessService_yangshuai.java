package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.PppDetails;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcess;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcessExample;

import java.util.List;

public interface ProductProductionProcessService_yangshuai extends IService<ProductProductionProcess> {
      
      
      long countByExample(ProductProductionProcessExample example);
      
      int deleteByExample(ProductProductionProcessExample example);
      
      List<ProductProductionProcess> selectByExample(ProductProductionProcessExample example);
      
      int updateByExampleSelective(ProductProductionProcess record, ProductProductionProcessExample example);
      
      int updateByExample(ProductProductionProcess record, ProductProductionProcessExample example);
      
      /**
       * 查询
       *
       * @param productionProcessesExample
       * @return
       */
      List<ProductProductionProcess> selectProcessWlList(@Param("processesExample") ProductProductionProcess productionProcessesExample);
      
      /**
       * 根据parentId 查询
       * @param parentId
       * @return
       */
      List<PppDetails> getProcessesList(String parentId);
}

