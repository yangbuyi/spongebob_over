package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.yangshuai.domain.FileupdateExample;
import top.yangbuyi.production.yangshuai.domain.ProductionProcesses;
import top.yangbuyi.production.yangshuai.domain.ProductionProcessesExample;

import java.util.List;
    /**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/20
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface ProductionProcessesService_yangshuai extends IService<ProductionProcesses>{


    long countByExample(ProductionProcessesExample example);

    int deleteByExample(ProductionProcessesExample example);

    List<ProductionProcesses> selectByExample(ProductionProcessesExample example);

    int updateByExampleSelective(ProductionProcesses record, ProductionProcessesExample example);

    int updateByExample(ProductionProcesses record, ProductionProcessesExample example);
      
          void updateRealCostPrice(FileupdateExample fileupdateExample);
    }
