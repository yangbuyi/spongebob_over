package top.yangbuyi.production.zhouzihao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.zhouzihao.domain.ProductionPlanExample;
import top.yangbuyi.production.zhouzihao.domain.ProductionPlan;
import top.yangbuyi.production.zhouzihao.mapper.ProductionPlanMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.ProductionPlanService_zhouzihao;
import top.yangbuyi.system.common.DataGridView;

@Service
public class ProductionPlanServiceImpl extends ServiceImpl<ProductionPlanMapper_zhouzihao, ProductionPlan> implements ProductionPlanService_zhouzihao {

    @Override
    public long countByExample(ProductionPlanExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductionPlanExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductionPlan> selectByExample(ProductionPlanExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductionPlan record,ProductionPlanExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductionPlan record,ProductionPlanExample example) {
        return baseMapper.updateByExample(record,example);
    }

      /**
       * 状态查询
       * @param page
       * @param limit
       * @return
       */
      @Override
      public DataGridView findAllPage(Integer page, Integer limit) {
            Page<ProductionPlan> planPage = new Page<>(page, limit);
            QueryWrapper<ProductionPlan> wrapper = new QueryWrapper<>();
            wrapper.eq("check_tag","S001-2");
            wrapper.eq("manufacture_tag","P001-1");
            Page<ProductionPlan> planPage1 = baseMapper.selectPage(planPage, wrapper);
            return new DataGridView(planPage1.getTotal(),planPage1.getRecords());
      }
}
