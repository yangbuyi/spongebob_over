package top.yangbuyi.production.zhouzihao.service;

import top.yangbuyi.production.zhouzihao.domain.ProductFileExample;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcessExample;
import java.util.List;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcess;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.zhouzihao.vo.ProductionProcess;
import top.yangbuyi.production.zhouzihao.vo.ProductionProcessVo;
import top.yangbuyi.system.common.DataGridView;

public interface ProductProductionProcessService_zhouzihao extends IService<ProductProductionProcess>{


    long countByExample(ProductProductionProcessExample example);

    int deleteByExample(ProductProductionProcessExample example);

    List<ProductProductionProcess> selectByExample(ProductProductionProcessExample example);

    int updateByExampleSelective(ProductProductionProcess record, ProductProductionProcessExample example);

    int updateByExample(ProductProductionProcess record, ProductProductionProcessExample example);

      Integer insProductionProcess();

      int insProductionProcess(ProductionProcess productionProcess);

      DataGridView quDataGridView(String productId);

      List<ProductProductionProcess> findNotAuditPage(ProductFileExample example);

      int updateAuditStatus(Integer id, String status);

      DataGridView selectAll(Integer page, Integer limit, ProductionProcessVo processVo);

      int toResubmit(Integer id, ProductionProcess productionProcess);

      ProductProductionProcess findById(String id);
}
