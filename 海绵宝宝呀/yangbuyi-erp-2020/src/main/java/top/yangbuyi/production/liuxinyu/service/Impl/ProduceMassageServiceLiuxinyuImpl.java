package top.yangbuyi.production.liuxinyu.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.liuxinyu.domain.ProduceMassage;
import top.yangbuyi.production.liuxinyu.domain.ProduceMassageExample;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcesses;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesMaterial;
import top.yangbuyi.production.liuxinyu.mapper.ProduceMassage_liuxinyu_Mapper;
import top.yangbuyi.production.liuxinyu.service.ProduceMassageService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.ProduceMassageVo;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.StringUtils;

import java.util.List;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Service
public class ProduceMassageServiceLiuxinyuImpl extends ServiceImpl<ProduceMassage_liuxinyu_Mapper, ProduceMassage> implements ProduceMassageService_liuxinyu {

    @Override
    public long countByExample(ProduceMassageExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProduceMassageExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProduceMassage> selectByExample(ProduceMassageExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProduceMassage record,ProduceMassageExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProduceMassage record,ProduceMassageExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public DataGridView querySelAllMassage(ProduceMassageVo produceMassageVo) {

            IPage<ProduceMassage> page=new Page<>(produceMassageVo.getPage(),produceMassageVo.getLimit());
            produceMassageVo.setCheck_tag("S001-2");
            QueryWrapper<ProduceMassage> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(StringUtils.isNotBlank(produceMassageVo.getCheck_tag()),"check_tag",produceMassageVo.getCheck_tag());
            queryWrapper.like(StringUtils.isNotBlank(produceMassageVo.getProduct_name()),"product_name",produceMassageVo.getProduct_name());

            IPage<ProduceMassage> produceMassageIPage=this.baseMapper.selectPage(page,queryWrapper);


            return new DataGridView(produceMassageIPage.getTotal(),produceMassageIPage.getRecords());
      }

      @Override
      public DataGridView selProcesses(Integer parentId) {
            List<ProductionProcesses> processes=this.baseMapper.selProcesses(parentId);
            return new DataGridView(processes);
      }

      @Override
      public DataGridView selPMaterial(Integer id) {
            List<ProductionProcessesMaterial> processesMaterials=this.baseMapper.selPMaterial(id);
            return new DataGridView(processesMaterials);
      }

      @Override
      public List<ProductionProcessesMaterial> selPM(Integer id) {
            return this.baseMapper.selPMaterial(id);
      }

      @Override
      public int updProcesses(Integer id,Integer hour) {
            return this.baseMapper.updProcesses(id,hour);
      }

      @Override
      public int updPMaterial(Integer id, Integer bcRenewAmount) {
            return this.baseMapper.updPMaterial(id,bcRenewAmount);
      }

      @Override
      public ProductionProcesses selByIdPro(Integer id) {
            return this.baseMapper.selByIdPro(id);
      }
}
