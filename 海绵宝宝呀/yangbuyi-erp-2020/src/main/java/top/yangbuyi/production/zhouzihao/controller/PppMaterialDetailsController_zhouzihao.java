package top.yangbuyi.production.zhouzihao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetails;
import top.yangbuyi.production.zhouzihao.service.PppMaterialDetailsService_zhouzihao;

import java.util.List;

@RestController
@RequestMapping("api/zhouzihao/PppMaterialDetails")
public class PppMaterialDetailsController_zhouzihao {

      @Autowired
      private PppMaterialDetailsService_zhouzihao pppMaterialDetailsService;

      @RequestMapping("findAllByIdForAllList")
      public Object findAllByProductIdForAllList(String id){
            List<PppMaterialDetails> all=pppMaterialDetailsService.findAllByIdForAllList(id);
            return all;
      }
}
