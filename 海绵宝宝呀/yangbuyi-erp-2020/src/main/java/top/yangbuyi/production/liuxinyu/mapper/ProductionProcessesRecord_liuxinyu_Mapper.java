package top.yangbuyi.production.liuxinyu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecord;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecordExample;

/**
@日复一日
@进入代码世界
@SmallNew
*/
public interface ProductionProcessesRecord_liuxinyu_Mapper extends BaseMapper<ProductionProcessesRecord> {
    long countByExample(ProductionProcessesRecordExample example);

    int deleteByExample(ProductionProcessesRecordExample example);

    List<ProductionProcessesRecord> selectByExample(ProductionProcessesRecordExample example);

    int updateByExampleSelective(@Param("record") ProductionProcessesRecord record, @Param("example") ProductionProcessesRecordExample example);

    int updateByExample(@Param("record") ProductionProcessesRecord record, @Param("example") ProductionProcessesRecordExample example);

    int batchNoPass(@Param("ids") String[] ids, @Param("checker") String checker, @Param("check_time") Date checkTime);

    int batchPass(@Param("ids") String[] ids, @Param("checker") String checker, @Param("check_time") Date checkTime);

    int count(String procedureId);
}