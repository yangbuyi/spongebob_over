package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.FileupdateExample;
import top.yangbuyi.production.yangshuai.domain.ProductionProcesses;
import top.yangbuyi.production.yangshuai.domain.ProductionProcessesExample;
import top.yangbuyi.production.yangshuai.mapper.ProductFile_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.mapper.ProductionProcesses_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.service.ProductionProcessesService_yangshuai;

import java.util.List;
/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/20
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

@Service
public class ProductionProcessesServiceYangshuaiImpl_yangshuai extends ServiceImpl<ProductionProcesses_yangshuai_Mapper, ProductionProcesses> implements ProductionProcessesService_yangshuai {

    @Autowired
    private ProductFile_yangshuai_Mapper productFileYangshuaiMapper;
    
    @Override
    public long countByExample(ProductionProcessesExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductionProcessesExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductionProcesses> selectByExample(ProductionProcessesExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductionProcesses record,ProductionProcessesExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductionProcesses record,ProductionProcessesExample example) {
        return baseMapper.updateByExample(record,example);
    }
    
    //计算总成本
    @Override
    public void updateRealCostPrice(FileupdateExample fileupdateExample) {
        productFileYangshuaiMapper.updateRealCostPrice(fileupdateExample);
    }
}
