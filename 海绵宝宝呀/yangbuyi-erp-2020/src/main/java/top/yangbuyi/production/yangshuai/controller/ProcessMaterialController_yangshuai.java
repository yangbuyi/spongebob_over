package top.yangbuyi.production.yangshuai.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 杨不易网站:www.yangbuyi.top
 * @program: yangbuyi-erp-2020
 * @ClassName: ProcessMaterialController
 * @create: 2020-07-16 14:49
 * @author: yangbuyi
 * @since： JDK1.8
 * @productProductionProcessController:
 **/

/**
 * 这里是制定工序物料设计
 */

@RestController
@Slf4j
@RequestMapping("api/yangshuai/ProcessMaterialController")
public class ProcessMaterialController_yangshuai {

      
      
}
