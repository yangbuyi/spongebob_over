package top.yangbuyi.production.zhouzihao.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.zhouzihao.domain.ProductMaterialExample;
import top.yangbuyi.production.zhouzihao.mapper.ProductMaterialMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.domain.ProductMaterial;
import top.yangbuyi.production.zhouzihao.service.ProductMaterialService_zhouzihao;
@Service
public class ProductMaterialServiceImpl extends ServiceImpl<ProductMaterialMapper_zhouzihao, ProductMaterial> implements ProductMaterialService_zhouzihao {

    @Override
    public long countByExample(ProductMaterialExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductMaterialExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductMaterial> selectByExample(ProductMaterialExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductMaterial record,ProductMaterialExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductMaterial record,ProductMaterialExample example) {
        return baseMapper.updateByExample(record,example);
    }
}
