package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.FileupdateExample;
import top.yangbuyi.production.yangshuai.domain.ProductFile;
import top.yangbuyi.production.yangshuai.domain.ProductFileExample;

import java.util.List;

public interface ProductFile_yangshuai_Mapper extends BaseMapper<ProductFile> {
    long countByExample(ProductFileExample example);

    int deleteByExample(ProductFileExample example);

    List<ProductFile> selectByExample(ProductFileExample example);

    int updateByExampleSelective(@Param("record") ProductFile record, @Param("example") ProductFileExample example);

    int updateByExample(@Param("record") ProductFile record, @Param("example") ProductFileExample example);
      
      void updateRealCostPrice(FileupdateExample fileupdateExample);
}