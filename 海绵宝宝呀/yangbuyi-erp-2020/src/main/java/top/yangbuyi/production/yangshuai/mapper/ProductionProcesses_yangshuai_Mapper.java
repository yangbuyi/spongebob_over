package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.ProductionProcesses;
import top.yangbuyi.production.yangshuai.domain.ProductionProcessesExample;

import java.util.List;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/20
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface ProductionProcesses_yangshuai_Mapper extends BaseMapper<ProductionProcesses> {
    long countByExample(ProductionProcessesExample example);

    int deleteByExample(ProductionProcessesExample example);

    List<ProductionProcesses> selectByExample(ProductionProcessesExample example);

    int updateByExampleSelective(@Param("record") ProductionProcesses record, @Param("example") ProductionProcessesExample example);

    int updateByExample(@Param("record") ProductionProcesses record, @Param("example") ProductionProcessesExample example);
}