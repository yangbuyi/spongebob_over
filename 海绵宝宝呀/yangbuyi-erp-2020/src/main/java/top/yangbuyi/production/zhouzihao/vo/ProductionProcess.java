package top.yangbuyi.production.zhouzihao.vo;

import top.yangbuyi.production.zhouzihao.domain.PDetails;

import java.io.Serializable;
import java.util.List;


public class ProductionProcess implements Serializable {
      private Integer id;
      private String enact;
      private String desc;

      public String getDesc() {
            return desc;
      }

      public void setDesc(String desc) {
            this.desc = desc;
      }

      private String doPeople;
      private Integer processSum;

      @Override
      public String toString() {
            return "ProductionProcess{" +
                  "id=" + id +
                  ", enact='" + enact + '\'' +
                  ", desc='" + desc + '\'' +
                  ", doPeople='" + doPeople + '\'' +
                  ", processSum=" + processSum +
                  ", pDtest=" + pDtest +
                  '}';
      }

      public Integer getProcessSum() {
            return processSum;
      }

      public void setProcessSum(Integer processSum) {
            this.processSum = processSum;
      }



      public String getDoPeople() {
            return doPeople;
      }

      public void setDoPeople(String doPeople) {
            this.doPeople = doPeople;
      }

      private List<pDtest> pDtest;

      public List<top.yangbuyi.production.zhouzihao.vo.pDtest> getpDtest() {
            return pDtest;
      }

      public void setpDtest(List<top.yangbuyi.production.zhouzihao.vo.pDtest> pDtest) {
            this.pDtest = pDtest;
      }


      public Integer getId() {
            return id;
      }

      public void setId(Integer id) {
            this.id = id;
      }

      public String getEnact() {
            return enact;
      }

      public void setEnact(String enact) {
            this.enact = enact;
      }

}
