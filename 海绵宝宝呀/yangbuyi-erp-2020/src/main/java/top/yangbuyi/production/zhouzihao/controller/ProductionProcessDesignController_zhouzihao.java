package top.yangbuyi.production.zhouzihao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.zhouzihao.domain.PppDetails;
import top.yangbuyi.production.zhouzihao.domain.ProductFile;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcess;
import top.yangbuyi.production.zhouzihao.service.PppDetailsService_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.ProductFileService_zhouzihao;
import top.yangbuyi.production.zhouzihao.vo.ProductionProcess;
import top.yangbuyi.production.zhouzihao.service.ProductProductionProcessService_zhouzihao;
import top.yangbuyi.production.zhouzihao.vo.ProductionProcessVo;
import top.yangbuyi.system.common.DataGridView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
/**
* @Description: 周子豪个人网址:http://yangzhuxian.top
* @Param:
* @return:
* @Author: Mr.zhou
* @Date: 2020/7/15
 *
*/
@RestController
@RequestMapping("api/zhouzihao/ProductionProcessDesign")
public class ProductionProcessDesignController_zhouzihao {
      @Autowired
      private ProductFileService_zhouzihao productFileService;
      @Resource
      private ProductProductionProcessService_zhouzihao productProductionProcessService;

      @Autowired
      private PppDetailsService_zhouzihao pppDetailsService;
      /**
       * 获取所有条件数据
       * @return
       */
      @RequestMapping("findAllList")
      public Object findAllList(Integer page, Integer limit, ProductionProcessVo processVo){
            System.err.println(processVo);
            DataGridView productFiles = productProductionProcessService.selectAll(page,limit,processVo);
            HashMap<String, Object> result = new HashMap<>();
            result.put("code",0);
            result.put("msg","");
            result.put("count",productFiles.getCount());
            result.put("data",productFiles.getData());
            return result;
      }

      /**
       * 获取复合条件数据
       * @return
       */
      @RequestMapping("findAllPage")
      public Object findAllPage(){
            //Integer page,Integer PageSize
            List<ProductFile> productFiles = productFileService.selectByExample(null);
            HashMap<String, Object> result = new HashMap<>();
            result.put("code",0);
            result.put("msg","");
            result.put("count",productFiles.size());
            result.put("data",productFiles);

//            System.out.println(pri);
            return result;
      }

      //@RequestBody

      /**
       * 添加产品工序
       * @param productionProcess
       * @return
       */
      @RequestMapping("insProductionProcess")
      public Object insProductionProcess(@RequestBody ProductionProcess productionProcess){
            int i=productProductionProcessService.insProductionProcess(productionProcess);
            System.err.println(i);
            return i;
      }

      /**
       * 查询未审核数据
       */
      @RequestMapping("findNotAuditPage")
      public Object findNotAuditPage(){
            List<ProductProductionProcess> productProductionProcesses = productProductionProcessService.findNotAuditPage(null);
            HashMap<String, Object> result = new HashMap<>();
            result.put("code",0);
            result.put("msg","");
            result.put("count",productProductionProcesses.size());
            result.put("data",productProductionProcesses);
            return result;
      }

      /**
       * 修改审核状态
       */
      @RequestMapping("updateAuditStatus")
      public Object updateAuditStatus(Integer id,String status){
            int i = productProductionProcessService.updateAuditStatus(id,status);
            return i;
      }

      /**
       * 重新审核
       * @param id
       * @param productionProcess
       * @return
       */
      @RequestMapping("toResubmit")
      public Object toResubmit(Integer id,@RequestBody ProductionProcess productionProcess){
            int i=productProductionProcessService.toResubmit(id,productionProcess);
            System.err.println(productionProcess);
            return 1;
      }

      /**
       * pppDetails根据id查询出对应工序
       */
      @RequestMapping("findPppDetailsById")
      public Object findPppDetailsById(Integer id){
            System.err.println(id);
            List<PppDetails> pppDetailsById = pppDetailsService.findPppDetailsById(id);

            return pppDetailsById;
      }

      /**
       * pppDetails根据父id查询出对应工序
       */
      @RequestMapping("findPppDetailsByParentId")
      public Object findPppDetailsByParentId(String parentId){
            List<PppDetails> pppDetailsById = pppDetailsService.findPppDetailsByParentId(parentId);

            return pppDetailsById;
      }

      /**
       * 根据id查数据
       * @param id
       * @return
       */
      @RequestMapping("findById")
      public Object findById(String id){
            ProductProductionProcess re = productProductionProcessService.findById(id);
            return re;
      }


}
