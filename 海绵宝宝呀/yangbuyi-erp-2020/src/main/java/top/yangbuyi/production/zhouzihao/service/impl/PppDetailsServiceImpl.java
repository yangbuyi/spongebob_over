package top.yangbuyi.production.zhouzihao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcess;
import top.yangbuyi.production.zhouzihao.mapper.PppDetailsMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.domain.PppDetailsExample;
import top.yangbuyi.production.zhouzihao.domain.PppDetails;
import top.yangbuyi.production.zhouzihao.mapper.ProductProductionProcessMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.PppDetailsService_zhouzihao;
@Service
public class PppDetailsServiceImpl extends ServiceImpl<PppDetailsMapper_zhouzihao, PppDetails> implements PppDetailsService_zhouzihao {

      @Autowired
      private ProductProductionProcessMapper_zhouzihao productProductionProcessMapper;
    @Override
    public long countByExample(PppDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(PppDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<PppDetails> selectByExample(PppDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(PppDetails record,PppDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(PppDetails record,PppDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public List<PppDetails> findPppDetailsById(Integer id) {
            QueryWrapper<PppDetails> wrapper = new QueryWrapper<>();
            wrapper.eq("parent_id",id);
            List<PppDetails> pppDetails = baseMapper.selectList(wrapper);
            return pppDetails;
      }

      /**
       * 根据产品编号查询产品工序
       * @param parentId
       * @return
       */
      @Override
      public List<PppDetails> findPppDetailsByParentId(String parentId) {
            QueryWrapper<ProductProductionProcess> wrapper = new QueryWrapper<ProductProductionProcess>();
            wrapper.eq("product_id",parentId);
            ProductProductionProcess productProductionProcess = productProductionProcessMapper.selectOne(wrapper);

            List<PppDetails> pppDetailsById = this.findPppDetailsById(productProductionProcess.getId());
            return pppDetailsById;
      }
}
