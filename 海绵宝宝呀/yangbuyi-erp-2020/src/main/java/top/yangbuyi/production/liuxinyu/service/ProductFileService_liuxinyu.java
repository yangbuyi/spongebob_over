package top.yangbuyi.production.liuxinyu.service;

import top.yangbuyi.production.liuxinyu.domain.ProductFile;
import java.util.List;
import top.yangbuyi.production.liuxinyu.domain.ProductFileExample;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.liuxinyu.domain.ProductMaterialDetails;
import top.yangbuyi.production.liuxinyu.vo.ProductFileVo;
import top.yangbuyi.system.common.DataGridView;

/**
@日复一日
@进入代码世界
@SmallNew
*/
public interface ProductFileService_liuxinyu extends IService<ProductFile>{


    long countByExample(ProductFileExample example);

    int deleteByExample(ProductFileExample example);

    List<ProductFile> selectByExample(ProductFileExample example);

    int updateByExampleSelective(ProductFile record, ProductFileExample example);

    int updateByExample(ProductFile record, ProductFileExample example);

    DataGridView querySelAllFile(ProductFileVo productFileVo);

    List<ProductMaterialDetails> selMaterial(String productId);

    int selMaterialAmount(String productId);
}
