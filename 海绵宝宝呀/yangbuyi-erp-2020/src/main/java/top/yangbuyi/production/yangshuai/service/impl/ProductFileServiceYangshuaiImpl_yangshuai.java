package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.ProductFile;
import top.yangbuyi.production.yangshuai.domain.ProductFileExample;
import top.yangbuyi.production.yangshuai.mapper.ProductFile_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.service.ProductFileService_yangshuai;

import java.util.List;
@Service
public class ProductFileServiceYangshuaiImpl_yangshuai extends ServiceImpl<ProductFile_yangshuai_Mapper, ProductFile> implements ProductFileService_yangshuai {

    @Override
    public long countByExample(ProductFileExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductFileExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProductFile> selectByExample(ProductFileExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProductFile record,ProductFileExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductFile record,ProductFileExample example) {
        return baseMapper.updateByExample(record,example);
    }
}
