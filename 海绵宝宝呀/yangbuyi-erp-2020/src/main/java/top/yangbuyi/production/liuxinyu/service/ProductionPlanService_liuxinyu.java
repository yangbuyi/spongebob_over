package top.yangbuyi.production.liuxinyu.service;

import top.yangbuyi.production.liuxinyu.domain.ProductionPlanExample;

import java.util.Date;
import java.util.List;
import top.yangbuyi.production.liuxinyu.domain.ProductionPlan;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.liuxinyu.vo.ProductionPlanVo;
import top.yangbuyi.system.common.DataGridView;

/**
@日复一日
@进入代码世界
@SmallNew
*/
public interface ProductionPlanService_liuxinyu extends IService<ProductionPlan>{


    long countByExample(ProductionPlanExample example);

    int deleteByExample(ProductionPlanExample example);

    List<ProductionPlan> selectByExample(ProductionPlanExample example);

    int updateByExampleSelective(ProductionPlan record, ProductionPlanExample example);

    int updateByExample(ProductionPlan record, ProductionPlanExample example);

    DataGridView querySelAllPlan(ProductionPlanVo productionPlanVo);

    int batchNoPass(String[] ids, String checker, Date checkTime);

    int batchPass(String[] ids, String checker, Date checkTime);

    ProductionPlan savePlan(ProductionPlan productionPlan);

}
