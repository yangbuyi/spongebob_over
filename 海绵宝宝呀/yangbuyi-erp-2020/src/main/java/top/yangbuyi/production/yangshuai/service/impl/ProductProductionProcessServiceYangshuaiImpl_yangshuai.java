package top.yangbuyi.production.yangshuai.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.yangshuai.domain.PppDetails;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcess;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcessExample;
import top.yangbuyi.production.yangshuai.mapper.PppDetails_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.mapper.ProductProductionProcess_yangshuai_Mapper;
import top.yangbuyi.production.yangshuai.service.ProductProductionProcessService_yangshuai;

import java.util.List;

@Service
public class ProductProductionProcessServiceYangshuaiImpl_yangshuai extends ServiceImpl<ProductProductionProcess_yangshuai_Mapper, ProductProductionProcess> implements ProductProductionProcessService_yangshuai {
      
      @Autowired
      private PppDetails_yangshuai_Mapper pppDetailsYangshuaiMapper;
      @Override
      public long countByExample(ProductProductionProcessExample example) {
            return baseMapper.countByExample(example);
      }
      
      @Override
      public int deleteByExample(ProductProductionProcessExample example) {
            return baseMapper.deleteByExample(example);
      }
      
      @Override
      public List<ProductProductionProcess> selectByExample(ProductProductionProcessExample example) {
            return baseMapper.selectByExample(example);
      }
      
      @Override
      public int updateByExampleSelective(ProductProductionProcess record, ProductProductionProcessExample example) {
            return baseMapper.updateByExampleSelective(record, example);
      }
      
      @Override
      public int updateByExample(ProductProductionProcess record, ProductProductionProcessExample example) {
            return baseMapper.updateByExample(record, example);
      }
      
      
      /**
       * 查询
       *
       * @param productionProcessesExample
       * @return
       */
      @Override
      public List<ProductProductionProcess> selectProcessWlList(ProductProductionProcess productionProcessesExample) {
            return baseMapper.selectByExamples(productionProcessesExample);
      }
      
      /**
       * 根据parentId 查询PppDetails里面的数据集合
       *
       * @param parentId
       * @return
       */
      @Override
      public List<PppDetails> getProcessesList(String parentId) {
//            select * from  ppp_details where PARENT_ID = #{parentId}
            QueryWrapper<PppDetails> pppDetailsQueryWrapper = new QueryWrapper<>();
            // select * from ppp_details where PARENT_ID = ?
            pppDetailsQueryWrapper.eq(StringUtils.isNotBlank(parentId), "parent_id", Integer.parseInt(parentId));
            List<PppDetails> pppDetails = pppDetailsYangshuaiMapper.selectList(pppDetailsQueryWrapper);
            return pppDetails;
      }
      
}

