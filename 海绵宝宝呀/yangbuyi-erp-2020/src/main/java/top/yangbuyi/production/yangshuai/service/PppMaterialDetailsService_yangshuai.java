package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.PppMaterialDetailsExample;

import java.util.List;
    /**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface PppMaterialDetailsService_yangshuai extends IService<PppMaterialDetails>{


    long countByExample(PppMaterialDetailsExample example);

    int deleteByExample(PppMaterialDetailsExample example);

    List<PppMaterialDetails> selectByExample(PppMaterialDetailsExample example);

    int updateByExampleSelective(PppMaterialDetails record, PppMaterialDetailsExample example);

    int updateByExample(PppMaterialDetails record, PppMaterialDetailsExample example);

}
