package top.yangbuyi.production.zhouzihao.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.zhouzihao.domain.PDetails;
import top.yangbuyi.production.zhouzihao.domain.PDetailsExample;
import top.yangbuyi.production.zhouzihao.mapper.PDetails_zhouzihaoMapper;
import top.yangbuyi.production.zhouzihao.service.PDetailsService_zhouzihao;
@Service
public class PDetailsServiceImpl extends ServiceImpl<PDetails_zhouzihaoMapper, PDetails> implements PDetailsService_zhouzihao {

    @Override
    public long countByExample(PDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(PDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<PDetails> selectByExample(PDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(PDetails record,PDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(PDetails record,PDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }
}
