package top.yangbuyi.production.zhouzihao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.zhouzihao.domain.ProductFile;
import top.yangbuyi.production.zhouzihao.mapper.ProductFileMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.domain.ProductFileExample;
import top.yangbuyi.production.zhouzihao.service.ProductFileService_zhouzihao;
@Service
public class ProductFileServiceImpl extends ServiceImpl<ProductFileMapper_zhouzihao, ProductFile> implements ProductFileService_zhouzihao {

    @Override
    public long countByExample(ProductFileExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProductFileExample example) {
        return baseMapper.deleteByExample(example);
    }

      /**
       * 查询所有审核通过的未设计工序的产品
       * @param example
       * @return
       */
    @Override
    public List<ProductFile> selectByExample(ProductFileExample example) {
          QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
          wrapper.eq("check_tag","S001-2");
//          wrapper.or().eq("check_tag","S001-3");
          wrapper.eq("type","Y001-1");
          wrapper.eq("delete_tag","C001-1");
          wrapper.eq("design_module_tag","W001-2");
          wrapper.eq("design_procedure_tag","G001-1");
          return baseMapper.selectList(wrapper);
    }
    @Override
    public int updateByExampleSelective(ProductFile record,ProductFileExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProductFile record,ProductFileExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public List<ProductFile> selectAll(ProductFileExample example) {
            return baseMapper.selectByExample(null);
      }


}
