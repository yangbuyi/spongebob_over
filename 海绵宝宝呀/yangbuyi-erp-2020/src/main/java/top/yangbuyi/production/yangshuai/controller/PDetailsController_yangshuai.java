package top.yangbuyi.production.yangshuai.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.yangshuai.service.PDetailsService_yangshuai;

@RestController
@RequestMapping("api/PDetails")
public class PDetailsController_yangshuai {
      @Autowired
      private PDetailsService_yangshuai pDetailsServiceYangshuai;

      /**
      * @Description: 养猪先个人网址:http://yangzhuxian.top
      * @Param: 
      * @return: 
      * @Author: Mr.zhou
      * @Date: 2020/7/16
      */
      @RequestMapping("findAll")
      public Object findAll(){
            return pDetailsServiceYangshuai.selectByExample(null);
      }
}
