package top.yangbuyi.production.liuxinyu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
@日复一日
@进入代码世界
@SmallNew
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "production_processes")
public class ProductionProcesses implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "details_number")
    private Integer detailsNumber;

    @TableField(value = "procedure_id")
    private String procedureId;

    @TableField(value = "procedure_name")
    private String procedureName;

    @TableField(value = "labour_hour_amount")
    private BigDecimal labourHourAmount;

    @TableField(value = "real_labour_hour_amount")
    private BigDecimal realLabourHourAmount;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    @TableField(value = "real_subtotal")
    private BigDecimal realSubtotal;

    @TableField(value = "module_subtotal")
    private BigDecimal moduleSubtotal;

    @TableField(value = "real_module_subtotal")
    private BigDecimal realModuleSubtotal;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "demand_amount")
    private BigDecimal demandAmount;

    @TableField(value = "real_amount")
    private BigDecimal realAmount;

    @TableField(value = "procedure_finish_tag")
    private String procedureFinishTag;

    @TableField(value = "procedure_transfer_tag")
    private String procedureTransferTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_DETAILS_NUMBER = "details_number";

    public static final String COL_PROCEDURE_ID = "procedure_id";

    public static final String COL_PROCEDURE_NAME = "procedure_name";

    public static final String COL_LABOUR_HOUR_AMOUNT = "labour_hour_amount";

    public static final String COL_REAL_LABOUR_HOUR_AMOUNT = "real_labour_hour_amount";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_REAL_SUBTOTAL = "real_subtotal";

    public static final String COL_MODULE_SUBTOTAL = "module_subtotal";

    public static final String COL_REAL_MODULE_SUBTOTAL = "real_module_subtotal";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_DEMAND_AMOUNT = "demand_amount";

    public static final String COL_REAL_AMOUNT = "real_amount";

    public static final String COL_PROCEDURE_FINISH_TAG = "procedure_finish_tag";

    public static final String COL_PROCEDURE_TRANSFER_TAG = "procedure_transfer_tag";
}