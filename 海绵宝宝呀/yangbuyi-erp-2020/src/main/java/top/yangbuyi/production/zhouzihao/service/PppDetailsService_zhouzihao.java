package top.yangbuyi.production.zhouzihao.service;

import java.util.List;
import top.yangbuyi.production.zhouzihao.domain.PppDetailsExample;
import top.yangbuyi.production.zhouzihao.domain.PppDetails;
import com.baomidou.mybatisplus.extension.service.IService;
public interface PppDetailsService_zhouzihao extends IService<PppDetails>{


    long countByExample(PppDetailsExample example);

    int deleteByExample(PppDetailsExample example);

    List<PppDetails> selectByExample(PppDetailsExample example);

    int updateByExampleSelective(PppDetails record, PppDetailsExample example);

    int updateByExample(PppDetails record, PppDetailsExample example);

      List<PppDetails> findPppDetailsById(Integer id);

      List<PppDetails> findPppDetailsByParentId(String parentId);
}
