package top.yangbuyi.production.zhouzihao.domain;

import java.util.ArrayList;
import java.util.List;

public class PDetailsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PDetailsExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andProcedureidIsNull() {
            addCriterion("procedureId is null");
            return (Criteria) this;
        }

        public Criteria andProcedureidIsNotNull() {
            addCriterion("procedureId is not null");
            return (Criteria) this;
        }

        public Criteria andProcedureidEqualTo(Integer value) {
            addCriterion("procedureId =", value, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidNotEqualTo(Integer value) {
            addCriterion("procedureId <>", value, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidGreaterThan(Integer value) {
            addCriterion("procedureId >", value, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidGreaterThanOrEqualTo(Integer value) {
            addCriterion("procedureId >=", value, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidLessThan(Integer value) {
            addCriterion("procedureId <", value, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidLessThanOrEqualTo(Integer value) {
            addCriterion("procedureId <=", value, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidIn(List<Integer> values) {
            addCriterion("procedureId in", values, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidNotIn(List<Integer> values) {
            addCriterion("procedureId not in", values, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidBetween(Integer value1, Integer value2) {
            addCriterion("procedureId between", value1, value2, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedureidNotBetween(Integer value1, Integer value2) {
            addCriterion("procedureId not between", value1, value2, "procedureid");
            return (Criteria) this;
        }

        public Criteria andProcedurenameIsNull() {
            addCriterion("procedureName is null");
            return (Criteria) this;
        }

        public Criteria andProcedurenameIsNotNull() {
            addCriterion("procedureName is not null");
            return (Criteria) this;
        }

        public Criteria andProcedurenameEqualTo(String value) {
            addCriterion("procedureName =", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameNotEqualTo(String value) {
            addCriterion("procedureName <>", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameGreaterThan(String value) {
            addCriterion("procedureName >", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameGreaterThanOrEqualTo(String value) {
            addCriterion("procedureName >=", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameLessThan(String value) {
            addCriterion("procedureName <", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameLessThanOrEqualTo(String value) {
            addCriterion("procedureName <=", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameLike(String value) {
            addCriterion("procedureName like", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameNotLike(String value) {
            addCriterion("procedureName not like", value, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameIn(List<String> values) {
            addCriterion("procedureName in", values, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameNotIn(List<String> values) {
            addCriterion("procedureName not in", values, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameBetween(String value1, String value2) {
            addCriterion("procedureName between", value1, value2, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProcedurenameNotBetween(String value1, String value2) {
            addCriterion("procedureName not between", value1, value2, "procedurename");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeIsNull() {
            addCriterion("procedureDescribe is null");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeIsNotNull() {
            addCriterion("procedureDescribe is not null");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeEqualTo(String value) {
            addCriterion("procedureDescribe =", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeNotEqualTo(String value) {
            addCriterion("procedureDescribe <>", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeGreaterThan(String value) {
            addCriterion("procedureDescribe >", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeGreaterThanOrEqualTo(String value) {
            addCriterion("procedureDescribe >=", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeLessThan(String value) {
            addCriterion("procedureDescribe <", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeLessThanOrEqualTo(String value) {
            addCriterion("procedureDescribe <=", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeLike(String value) {
            addCriterion("procedureDescribe like", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeNotLike(String value) {
            addCriterion("procedureDescribe not like", value, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeIn(List<String> values) {
            addCriterion("procedureDescribe in", values, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeNotIn(List<String> values) {
            addCriterion("procedureDescribe not in", values, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeBetween(String value1, String value2) {
            addCriterion("procedureDescribe between", value1, value2, "proceduredescribe");
            return (Criteria) this;
        }

        public Criteria andProceduredescribeNotBetween(String value1, String value2) {
            addCriterion("procedureDescribe not between", value1, value2, "proceduredescribe");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}