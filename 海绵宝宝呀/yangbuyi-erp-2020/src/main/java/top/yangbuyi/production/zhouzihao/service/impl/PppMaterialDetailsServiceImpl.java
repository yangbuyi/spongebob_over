package top.yangbuyi.production.zhouzihao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.zhouzihao.domain.PppDetails;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetails;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetailsExample;
import top.yangbuyi.production.zhouzihao.domain.ProductProductionProcess;
import top.yangbuyi.production.zhouzihao.mapper.PppMaterialDetailsMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.PppDetailsService_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.PppMaterialDetailsService_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.ProductProductionProcessService_zhouzihao;

import java.util.List;

@Service
public class PppMaterialDetailsServiceImpl extends ServiceImpl<PppMaterialDetailsMapper_zhouzihao, PppMaterialDetails> implements PppMaterialDetailsService_zhouzihao {

      @Autowired
      private ProductProductionProcessService_zhouzihao productProductionProcessService;
      @Autowired
      private PppDetailsService_zhouzihao pppDetailsService;

    @Override
    public long countByExample(PppMaterialDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(PppMaterialDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<PppMaterialDetails> selectByExample(PppMaterialDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(PppMaterialDetails record,PppMaterialDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(PppMaterialDetails record,PppMaterialDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public List<PppMaterialDetails> findAllByIdForAllList(String id) {
            ProductProductionProcess byId = productProductionProcessService.findById(id);
            System.err.println(byId);
            List<PppDetails> pppDetailsById = pppDetailsService.findPppDetailsById(byId.getId());
            QueryWrapper<PppMaterialDetails> wrapper= new QueryWrapper<>();;
            for (PppDetails pppDetails : pppDetailsById) {
                  wrapper.or().eq("parent_id",pppDetails.getId());
            }
            wrapper.orderByAsc("parent_id");
            List<PppMaterialDetails> pppMaterialDetails = baseMapper.selectList(wrapper);
            return pppMaterialDetails;
      }
      
      @Override
      public List<PppMaterialDetails> findPppDetailsById(Integer id) {
            QueryWrapper<PppMaterialDetails> wrapper = new QueryWrapper<>();
            wrapper.eq("parent_id",id);
            List<PppMaterialDetails> pppMaterialDetails = baseMapper.selectList(wrapper);
            return pppMaterialDetails;
      }
}
