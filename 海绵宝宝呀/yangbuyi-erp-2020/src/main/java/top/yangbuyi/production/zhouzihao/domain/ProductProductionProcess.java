package top.yangbuyi.production.zhouzihao.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName(value = "product_production_process")
public class ProductProductionProcess implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "design_id")
    private String designId;

    @TableField(value = "first_kind_id")
    private String firstKindId;

    @TableField(value = "first_kind_name")
    private String firstKindName;

    @TableField(value = "second_kind_id")
    private String secondKindId;

    @TableField(value = "second_kind_name")
    private String secondKindName;

    @TableField(value = "third_kind_id")
    private String thirdKindId;

    @TableField(value = "third_kind_name")
    private String thirdKindName;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "procedure_describe")
    private String procedureDescribe;

    @TableField(value = "cost_price_sum")
    private BigDecimal costPriceSum;

    @TableField(value = "module_cost_price_sum")
    private BigDecimal moduleCostPriceSum;

    @TableField(value = "designer")
    private String designer;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_suggestion")
    private String checkSuggestion;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "changer")
    private String changer;

    @TableField(value = "change_time")
    private Date changeTime;

    @TableField(value = "change_tag")
    private String changeTag;

    @TableField(value = "design_module_tag")
    private String designModuleTag;

    @TableField(value = "design_module_change_tag")
    private String designModuleChangeTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_DESIGN_ID = "design_id";

    public static final String COL_FIRST_KIND_ID = "first_kind_id";

    public static final String COL_FIRST_KIND_NAME = "first_kind_name";

    public static final String COL_SECOND_KIND_ID = "second_kind_id";

    public static final String COL_SECOND_KIND_NAME = "second_kind_name";

    public static final String COL_THIRD_KIND_ID = "third_kind_id";

    public static final String COL_THIRD_KIND_NAME = "third_kind_name";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_PROCEDURE_DESCRIBE = "procedure_describe";

    public static final String COL_COST_PRICE_SUM = "cost_price_sum";

    public static final String COL_MODULE_COST_PRICE_SUM = "module_cost_price_sum";

    public static final String COL_DESIGNER = "designer";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_SUGGESTION = "check_suggestion";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_CHANGER = "changer";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHANGE_TAG = "change_tag";

    public static final String COL_DESIGN_MODULE_TAG = "design_module_tag";

    public static final String COL_DESIGN_MODULE_CHANGE_TAG = "design_module_change_tag";
}