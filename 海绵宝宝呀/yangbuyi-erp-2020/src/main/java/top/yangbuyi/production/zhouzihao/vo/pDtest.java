package top.yangbuyi.production.zhouzihao.vo;

public class pDtest {
      private Integer procedureId;
      private String procedureName;
      private String procedureDescribe;
      private Integer labourHourAmount;
      private Integer costPrice;

      @Override
      public String toString() {
            return "pDtest{" +
                  "procedureId=" + procedureId +
                  ", procedureName='" + procedureName + '\'' +
                  ", procedureDescribe='" + procedureDescribe + '\'' +
                  ", labourHourAmount=" + labourHourAmount +
                  ", costPrice=" + costPrice +
                  '}';
      }

      public Integer getProcedureId() {
            return procedureId;
      }

      public void setProcedureId(Integer procedureId) {
            this.procedureId = procedureId;
      }

      public String getProcedureName() {
            return procedureName;
      }

      public void setProcedureName(String procedureName) {
            this.procedureName = procedureName;
      }

      public String getProcedureDescribe() {
            return procedureDescribe;
      }

      public void setProcedureDescribe(String procedureDescribe) {
            this.procedureDescribe = procedureDescribe;
      }

      public Integer getLabourHourAmount() {
            return labourHourAmount;
      }

      public void setLabourHourAmount(Integer labourHourAmount) {
            this.labourHourAmount = labourHourAmount;
      }

      public Integer getCostPrice() {
            return costPrice;
      }

      public void setCostPrice(Integer costPrice) {
            this.costPrice = costPrice;
      }


}
