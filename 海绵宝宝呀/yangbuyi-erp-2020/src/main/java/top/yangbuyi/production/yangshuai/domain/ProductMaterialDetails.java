package top.yangbuyi.production.yangshuai.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "product_material_details")
public class ProductMaterialDetails implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "details_number")
    private Integer detailsNumber;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "type")
    private String type;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "amount_unit")
    private String amountUnit;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "residual_amount")
    private BigDecimal residualAmount;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_DETAILS_NUMBER = "details_number";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_TYPE = "type";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_RESIDUAL_AMOUNT = "residual_amount";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_SUBTOTAL = "subtotal";
}