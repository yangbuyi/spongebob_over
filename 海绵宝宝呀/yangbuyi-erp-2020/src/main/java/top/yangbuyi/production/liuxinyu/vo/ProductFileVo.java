package top.yangbuyi.production.liuxinyu.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import top.yangbuyi.system.vo.BaseVo;

import java.util.Date;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductFileVo extends BaseVo {
//      register_time
      @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:dd")
      private Date registerTime;

      private String product_name;

      private String first_kind_id;

      private String delete_tag;

      private String check_tag;

      private String type;

      private String design_module_tag;

      private String design_procedure_tag;
}
