package top.yangbuyi.production.liuxinyu.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProduceMassageVo extends BaseVo {

      private String product_name;

      private String check_tag;
}
