package top.yangbuyi.production.liuxinyu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.liuxinyu.domain.ProductionPlan;
import top.yangbuyi.production.liuxinyu.service.ProductionPlanService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.ProductionPlanVo;
import top.yangbuyi.system.common.ResultObj;

import java.util.Date;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@RestController
@RequestMapping("api/liuxinyu/plan")
public class PlanController_liuxinyu {

      @Autowired
      private ProductionPlanService_liuxinyu productionPlanServiceLiuxinyu;


      @RequestMapping("selAllPlan")
      public Object selAllPlan(ProductionPlanVo productionPlanVo){
            System.out.println(productionPlanVo);
            return productionPlanServiceLiuxinyu.querySelAllPlan(productionPlanVo);
      }

//      @RequestMapping("selAllPlan")
//      public Object selAllPlan(ProductionPlanVo productionPlanVo){
//            System.out.println(productionPlanVo);
//            return productionPlanServiceLiuxinyu.querySelAllPlan(productionPlanVo);
//      }

      @RequestMapping("selByIdPlan")
      public Object selByIdPlan(Integer id){
            System.out.println(123);
            try {
                  ProductionPlan productionPlan= this.productionPlanServiceLiuxinyu.getById(id);
                  return productionPlan;
            } catch (Exception e) {
                  e.printStackTrace();
                  return ResultObj.SEE_ERROR;
            }
      }

      @PostMapping("batchNoPass")
      public Object batchNoPass(String[] ids,String checker) {
            System.out.println("进来了controller"+checker+"ids="+ids[0]);
            Date checkTime=new Date();
            try {
                  this.productionPlanServiceLiuxinyu.batchNoPass(ids,checker,checkTime);
                  return ResultObj.CHECK_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.CHECK_ERROR;
            }
      }

      @PostMapping("batchPass")
      public Object batchPass(String[] ids,String checker) {
            System.out.println("进来了controller"+checker+"ids="+ids[0]);
            Date checkTime=new Date();
            try {
                  this.productionPlanServiceLiuxinyu.batchPass(ids,checker,checkTime);
                  return ResultObj.CHECK_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.CHECK_ERROR;
            }
      }

      @PostMapping("addPlan")
      public Object addPlan(ProductionPlan productionPlan) {
            try {
                  productionPlan.setCheckTag("S001-1");
                  productionPlan.setRegisterTime(new Date());
                  productionPlan.setManufactureTag("P001-1");
                  this.productionPlanServiceLiuxinyu.savePlan(productionPlan);
                  return ResultObj.ADD_SUCCESS;
            } catch (Exception e) {
                  e.printStackTrace();
                  return ResultObj.ADD_ERROR;
            }
      }

//      @GetMapping("getAllAvailableProvider")
//      public Object getAllAvailableProvider(){
//            return this.providerService.getAllAvailableProvider();
//      }


}
