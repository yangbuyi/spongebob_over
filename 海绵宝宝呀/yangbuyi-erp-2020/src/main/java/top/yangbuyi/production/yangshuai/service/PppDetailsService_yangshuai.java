package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.PppDetails;
import top.yangbuyi.production.yangshuai.domain.PppDetailsExample;

import java.util.List;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/7/17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface PppDetailsService_yangshuai extends IService<PppDetails> {
    
    
    long countByExample(PppDetailsExample example);
    
    int deleteByExample(PppDetailsExample example);
    
    List<PppDetails> selectByExample(PppDetailsExample example);
    
    int updateByExampleSelective(PppDetails record, PppDetailsExample example);
    
    int updateByExample(PppDetails record, PppDetailsExample example);
    
    void updateDetails(@Param("pppDetails") PppDetails pppDetails);
}
