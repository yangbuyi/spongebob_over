package top.yangbuyi.production.yangshuai.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.yangshuai.domain.ProductFile;
import top.yangbuyi.production.yangshuai.service.ProductFileService_yangshuai;

import java.util.List;
/**
* @Description: 周子豪个人网址:http://yangzhuxian.top
* @Param:
* @return:
* @Author: Mr.zhou
* @Date: 2020/7/15
*/
@RestController
@RequestMapping("api/yangshuai/ProductionProcessDesign")
public class ProductionProcessDesignController_yangshuai {
      @Autowired
      private ProductFileService_yangshuai productFileServiceYangshuai;

      @RequestMapping("findAllPage")
      public Object findAllPage(){
            //Integer page,Integer PageSize
            List<ProductFile> productFiles = productFileServiceYangshuai.selectByExample(null);
//            System.out.println(pri);
            return productFiles;
      }
      
      
      
      
}
