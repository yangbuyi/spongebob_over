package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.production.yangshuai.domain.ProductFile;
import top.yangbuyi.production.yangshuai.domain.ProductFileExample;

import java.util.List;
public interface ProductFileService_yangshuai extends IService<ProductFile>{


    long countByExample(ProductFileExample example);

    int deleteByExample(ProductFileExample example);

    List<ProductFile> selectByExample(ProductFileExample example);

    int updateByExampleSelective(ProductFile record, ProductFileExample example);

    int updateByExample(ProductFile record, ProductFileExample example);

}
