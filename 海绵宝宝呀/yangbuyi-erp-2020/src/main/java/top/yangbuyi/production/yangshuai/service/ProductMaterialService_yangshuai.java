package top.yangbuyi.production.yangshuai.service;

import top.yangbuyi.production.yangshuai.domain.ProductMaterial;
import com.baomidou.mybatisplus.extension.service.IService;
    /**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/16
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface ProductMaterialService_yangshuai extends IService<ProductMaterial>{


}
