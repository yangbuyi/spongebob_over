package top.yangbuyi.production.liuxinyu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcesses;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesMaterial;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesRecord;
import top.yangbuyi.production.liuxinyu.service.ProduceMassageService_liuxinyu;
import top.yangbuyi.production.liuxinyu.service.ProductionProcessesRecordService_liuxinyu;
import top.yangbuyi.production.liuxinyu.vo.Insertrecord;
import top.yangbuyi.production.liuxinyu.vo.ProduceMassageVo;
import top.yangbuyi.system.common.ResultObj;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@RestController
@RequestMapping("api/liuxinyu/massage")
public class MassageController_liuxinyu {

      @Autowired
      private ProduceMassageService_liuxinyu produceMassageServiceLiuxinyu;

      @Autowired
      private ProductionProcessesRecordService_liuxinyu processesRecordService;


      @RequestMapping("selAllMassage")
      public Object selAllMassage(ProduceMassageVo produceMassageVo){
//            System.out.println(produceMassageVo);
            return produceMassageServiceLiuxinyu.querySelAllMassage(produceMassageVo);
      }

      @RequestMapping("selProcesses")
      public Object selProcesses(Integer parentId){
//            System.out.println(parentId);
            return produceMassageServiceLiuxinyu.selProcesses(parentId);
      }

      @RequestMapping("selPMaterial")
      public Object selPMaterial(Integer id){
//            System.out.println(id);
            return produceMassageServiceLiuxinyu.selPMaterial(id);
      }

      @PostMapping("updPM")
      public Object updPM(Insertrecord insertrecord, @RequestBody  List<ProductionProcessesMaterial> processesMaterials) {
            try {
                  this.produceMassageServiceLiuxinyu.updProcesses(insertrecord.getIdp(),insertrecord.getHourp());
                  if (null!=processesMaterials){
                        for (ProductionProcessesMaterial p : processesMaterials) {
                              Integer bcRenewAmount = p.getRenewAmount().intValue();
                              this.produceMassageServiceLiuxinyu.updPMaterial(p.getId(),bcRenewAmount);
                        }
                  }
                  
                  ProductionProcesses processes=this.produceMassageServiceLiuxinyu.selByIdPro(insertrecord.getIdp());

                  ProductionProcessesRecord processesRecord=new ProductionProcessesRecord();
                  processesRecord.setDetailsNumber(processes.getDetailsNumber());
                  processesRecord.setProcedureId(processes.getProcedureId());
                  processesRecord.setProcedureName(processes.getProcedureName());
                  processesRecord.setLabourHourAmount(BigDecimal.valueOf((Integer)insertrecord.getHourp()));
                  processesRecord.setCostPrice(processes.getCostPrice());
                  Integer costp = processes.getCostPrice().intValue();
                  Integer ps=costp*insertrecord.getHourp();
                  processesRecord.setSubtotal(BigDecimal.valueOf((Integer)ps));
                  Integer count= this.processesRecordService.count(processes.getProcedureId());
                  if (count<1){
                        Integer rc=1;
                        processesRecord.setRegCount(BigDecimal.valueOf((Integer)rc));
                  }else{
                        count++;
                        processesRecord.setRegCount(BigDecimal.valueOf((Integer)count));
                  }
                  processesRecord.setRegister(insertrecord.getTregister());
                  processesRecord.setRegisterTime(new Date());
                  processesRecord.setCheckTag("S001-1");
                  processesRecordService.saveRecord(processesRecord);
                  return ResultObj.UPDPM_SUCCESS;
            } catch (Exception e) {
                  // parentId
                  e.printStackTrace();
                  return ResultObj.UPDPM_ERROR;
            }
      }

//      @RequestMapping("selAllPlan")
//      public Object selAllPlan(ProductionPlanVo productionPlanVo){
//            System.out.println(productionPlanVo);
//            return productionPlanService.querySelAllPlan(productionPlanVo);
//      }

//      @RequestMapping("selByIdPlan")
//      public Object selByIdPlan(Integer id){
//            System.out.println(123);
//            try {
//                  ProductionPlan productionPlan= this.productionPlanService.getById(id);
//                  return productionPlan;
//            } catch (Exception e) {
//                  e.printStackTrace();
//                  return ResultObj.SEE_ERROR;
//            }
//      }
//
//      @PostMapping("batchNoPass")
//      public Object batchNoPass(String[] ids,String checker) {
//            Date checkTime=new Date();
//            try {
//                  this.productionPlanService.batchNoPass(ids,checker,checkTime);
//                  return ResultObj.CHECK_SUCCESS;
//            } catch (Exception e) {
//                  return ResultObj.CHECK_ERROR;
//            }
//      }
//
//      @PostMapping("batchPass")
//      public Object batchPass(String[] ids,String checker) {
//            Date checkTime=new Date();
//            try {
//                  this.productionPlanService.batchPass(ids,checker,checkTime);
//                  return ResultObj.CHECK_SUCCESS;
//            } catch (Exception e) {
//                  return ResultObj.CHECK_ERROR;
//            }
//      }
//
//      @PostMapping("addPlan")
//      public Object addPlan(ProductionPlan productionPlan) {
//            try {
//                  productionPlan.setCheckTag("S001-1");
//                  productionPlan.setRegisterTime(new Date());
//                  productionPlan.setManufactureTag("P001-2");
//                  this.productionPlanService.savePlan(productionPlan);
//                  return ResultObj.ADD_SUCCESS;
//            } catch (Exception e) {
//                  e.printStackTrace();
//                  return ResultObj.ADD_ERROR;
//            }
//      }

//      @GetMapping("getAllAvailableProvider")
//      public Object getAllAvailableProvider(){
//            return this.providerService.getAllAvailableProvider();
//      }


}
