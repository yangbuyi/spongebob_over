package top.yangbuyi.production.liuxinyu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.liuxinyu.domain.ProductFile;
import top.yangbuyi.production.liuxinyu.domain.ProductFileExample;
import top.yangbuyi.production.liuxinyu.domain.ProductMaterialDetails;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
@Mapper
public interface ProductFile_liuxinyu_Mapper extends BaseMapper<ProductFile> {
      long countByExample(ProductFileExample example);

      int deleteByExample(ProductFileExample example);

      List<ProductFile> selectByExample(ProductFileExample example);

      int updateByExampleSelective(@Param("record") ProductFile record, @Param("example") ProductFileExample example);

      int updateByExample(@Param("record") ProductFile record, @Param("example") ProductFileExample example);

      List<ProductMaterialDetails> selMaterial(String productId);

      int selMaterialAmount(String productId);
}