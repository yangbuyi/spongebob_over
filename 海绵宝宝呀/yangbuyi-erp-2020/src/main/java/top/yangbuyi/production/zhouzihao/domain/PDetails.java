package top.yangbuyi.production.zhouzihao.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName(value = "p_details")
public class PDetails implements Serializable {
      @TableField(exist = false)
      private Integer tile;
      @TableField(exist = false)
      private Integer cost;


      @TableId(value = "procedureId", type = IdType.INPUT)
      private Integer procedureId;

      @TableField(value = "procedureName")
      private String procedureName;

      @TableField(value = "procedureDescribe")
      private String procedureDescribe;

    private static final long serialVersionUID = 1L;

    public static final String COL_PROCEDUREID = "procedureId";

    public static final String COL_PROCEDURENAME = "procedureName";

    public static final String COL_PROCEDUREDESCRIBE = "procedureDescribe";
}