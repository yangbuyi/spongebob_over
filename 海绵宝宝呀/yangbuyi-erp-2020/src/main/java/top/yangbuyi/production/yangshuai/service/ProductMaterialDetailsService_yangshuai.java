package top.yangbuyi.production.yangshuai.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetailsExample;
import top.yangbuyi.production.yangshuai.domain.ProductProductionProcess;

import java.util.List;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/7/17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface ProductMaterialDetailsService_yangshuai extends IService<ProductMaterialDetails> {
      
      
      long countByExample(ProductMaterialDetailsExample example);
      
      int deleteByExample(ProductMaterialDetailsExample example);
      
      List<ProductMaterialDetails> selectByExample(ProductMaterialDetailsExample example);
      
      int updateByExampleSelective(ProductMaterialDetails record, ProductMaterialDetailsExample example);
      
      int updateByExample(ProductMaterialDetails record, ProductMaterialDetailsExample example);
      
      void addPppMaterialDetails(@Param("productMaterialDetails") ProductMaterialDetails productMaterialDetails);
      
      List<ProductMaterialDetails> getProductMaterialDetailsList(String productId);
      
      void updateProductionProcesses(ProductProductionProcess process);
      
      void getproductmaterialdetails(Integer id);
}
