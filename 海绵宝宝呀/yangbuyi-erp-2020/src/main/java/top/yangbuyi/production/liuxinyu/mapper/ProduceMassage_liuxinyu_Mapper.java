package top.yangbuyi.production.liuxinyu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.liuxinyu.domain.ProduceMassage;
import top.yangbuyi.production.liuxinyu.domain.ProduceMassageExample;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcesses;
import top.yangbuyi.production.liuxinyu.domain.ProductionProcessesMaterial;

/**
 * @日复一日
 * @进入代码世界
 * @SmallNew
 */
public interface ProduceMassage_liuxinyu_Mapper extends BaseMapper<ProduceMassage> {
      long countByExample(ProduceMassageExample example);

      int deleteByExample(ProduceMassageExample example);

      List<ProduceMassage> selectByExample(ProduceMassageExample example);

      int updateByExampleSelective(@Param("record") ProduceMassage record, @Param("example") ProduceMassageExample example);

      int updateByExample(@Param("record") ProduceMassage record, @Param("example") ProduceMassageExample example);

      List<ProductionProcesses> selProcesses(Integer parentId);

      List<ProductionProcessesMaterial> selPMaterial(Integer id);

      int updProcesses(@Param("id") Integer id, @Param("hour") Integer hour);

      int updPMaterial(@Param("id") Integer id, @Param("bcRenewAmount") Integer bcRenewAmount);

      ProductionProcesses selByIdPro(Integer id);
}