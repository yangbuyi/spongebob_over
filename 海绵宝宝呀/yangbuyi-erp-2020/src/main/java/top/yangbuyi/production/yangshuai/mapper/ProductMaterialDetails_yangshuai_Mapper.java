package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetails;
import top.yangbuyi.production.yangshuai.domain.ProductMaterialDetailsExample;

import java.util.List;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/7/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface ProductMaterialDetails_yangshuai_Mapper extends BaseMapper<ProductMaterialDetails> {
    long countByExample(ProductMaterialDetailsExample example);

    int deleteByExample(ProductMaterialDetailsExample example);

    List<ProductMaterialDetails> selectByExample(ProductMaterialDetailsExample example);

    int updateByExampleSelective(@Param("record") ProductMaterialDetails record, @Param("example") ProductMaterialDetailsExample example);

    int updateByExample(@Param("record") ProductMaterialDetails record, @Param("example") ProductMaterialDetailsExample example);
      
      List<ProductMaterialDetails> selectProductMaterialDetails(String productId);
      
      void updateResidual(ProductMaterialDetails s);
}