package top.yangbuyi.production.zhouzihao.service;

import top.yangbuyi.production.zhouzihao.domain.ProductMaterialExample;
import java.util.List;
import top.yangbuyi.production.zhouzihao.domain.ProductMaterial;
import com.baomidou.mybatisplus.extension.service.IService;
public interface ProductMaterialService_zhouzihao extends IService<ProductMaterial>{


    long countByExample(ProductMaterialExample example);

    int deleteByExample(ProductMaterialExample example);

    List<ProductMaterial> selectByExample(ProductMaterialExample example);

    int updateByExampleSelective(ProductMaterial record, ProductMaterialExample example);

    int updateByExample(ProductMaterial record, ProductMaterialExample example);

}
