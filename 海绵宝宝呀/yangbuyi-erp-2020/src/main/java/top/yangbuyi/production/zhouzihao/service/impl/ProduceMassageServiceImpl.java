package top.yangbuyi.production.zhouzihao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.production.zhouzihao.domain.*;
import top.yangbuyi.production.zhouzihao.mapper.ProduceMassageMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.mapper.ProductionPlanMapper_zhouzihao;
import top.yangbuyi.production.zhouzihao.mapper.ProductionProcessesMapper;
import top.yangbuyi.production.zhouzihao.mapper.ProductionProcessesMaterialMapper;
import top.yangbuyi.production.zhouzihao.service.PppDetailsService_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.PppMaterialDetailsService_zhouzihao;
import top.yangbuyi.production.zhouzihao.service.ProduceMassageService_zhouzihao;
import top.yangbuyi.production.zhouzihao.vo.ProduceMassageVo;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertory;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.yinshengjie.mapper.OutRepertoryDetailsMapper_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.mapper.OutRepertoryMapper_yinshengjie;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class ProduceMassageServiceImpl extends ServiceImpl<ProduceMassageMapper_zhouzihao, ProduceMassage> implements ProduceMassageService_zhouzihao {

      @Autowired
      private ProductionPlanMapper_zhouzihao productionPlanMapper;
      
      @Autowired
      private OutRepertoryMapper_yinshengjie outRepertoryMapper_yinshengjie;
      @Autowired
      private PppMaterialDetailsService_zhouzihao pppMaterialDetailsService_zhouzihao;
      @Autowired
      private OutRepertoryDetailsMapper_yinshengjie outRepertoryDetailsMapper_yinshengjie;
      @Autowired
      private PppDetailsService_zhouzihao pppDetailsService_zhouzihao;
      @Autowired
      private ProductionProcessesMapper productionProcessesMapper;
      @Autowired
      private ProductionProcessesMaterialMapper productionProcessesMaterialMapper;
    @Override
    public long countByExample(ProduceMassageExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProduceMassageExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProduceMassage> selectByExample(ProduceMassageExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProduceMassage record,ProduceMassageExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProduceMassage record,ProduceMassageExample example) {
        return baseMapper.updateByExample(record,example);
    }

      @Override
      public int insProduceMassage(ProduceMassageVo produceMassage) throws ParseException {
          ProduceMassage promassage = new ProduceMassage();
            promassage.setManufactureId(produceMassage.getManufactureId());
            promassage.setProductId(produceMassage.getProductId());
            promassage.setProductName(produceMassage.getProductName());
            promassage.setAmount(BigDecimal.valueOf(Integer.valueOf(produceMassage.getAmount())));
            promassage.setApplyIdGroup(produceMassage.getApplyIdGroup());
            promassage.setModuleCostPriceSum(BigDecimal.valueOf(Integer.valueOf(produceMassage.getModuleCostPriceSum())));
            promassage.setLabourCostPriceSum(BigDecimal.valueOf(Integer.valueOf(produceMassage.getLabourCostPriceSum())));
            promassage.setRegister(produceMassage.getDesigner());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//yyyy-mm-dd, 会出现时间不对, 因为小写的mm是代表: 秒
            Date utilDate = sdf.parse(produceMassage.getRegisterTime());
            promassage.setRegisterTime(utilDate);
            promassage.setChecker(produceMassage.getDesigner());
            promassage.setCheckTime(new Date());
            promassage.setCheckTag("S001-1");
            promassage.setManufactureProcedureTag("S002-1");
            int insert = baseMapper.insert(promassage);
            if (insert>0){
                  ProductionPlan plan = new ProductionPlan();
                  plan.setManufactureTag("P001-2");
                  QueryWrapper<ProductionPlan> wrapper = new QueryWrapper<>();
                  wrapper.eq("product_id",promassage.getProductId());
                  productionPlanMapper.update(plan,wrapper);
            }
            /**
             * 直接指定出库单
             */
            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
            Date date = new Date(System.currentTimeMillis());
            String d  = formatter.format(date);
            Random random = new Random();
            int i = random.nextInt(9999);
            OutRepertory outRe = new OutRepertory();
            outRe.setPayId("ck"+d+i);
            outRe.setStorer(produceMassage.getDesigner());
            outRe.setReason("C002-1");
            outRe.setRemark("派工单生产");
            outRe.setRegister(produceMassage.getDesigner());
            outRe.setRegisterTime(new Date());
            outRe.setCheckTag("S001-1");
            outRe.setStoreTag("K002-2");
            int insert1 = outRepertoryMapper_yinshengjie.insert(outRe);
            List<PppMaterialDetails> allByIdForAllList = pppMaterialDetailsService_zhouzihao.findAllByIdForAllList(produceMassage.getProductId());
            for (PppMaterialDetails pppMaterialDetails : allByIdForAllList) {
                  OutRepertoryDetails outRepertoryDetails =null;
                  outRepertoryDetails = new OutRepertoryDetails();
                  outRepertoryDetails.setParentId(outRe.getId());
                  outRepertoryDetails.setProductId(pppMaterialDetails.getProductId());
                  outRepertoryDetails.setProductName(pppMaterialDetails.getProductName());
                  outRepertoryDetails.setProductDescribe(pppMaterialDetails.getProductDescribe());
                  outRepertoryDetails.setAmount(pppMaterialDetails.getAmount().intValue()*Integer.valueOf(produceMassage.getAmount()));
                  outRepertoryDetails.setCostPrice(pppMaterialDetails.getCostPrice().intValue());
                  outRepertoryDetails.setSubtotal(pppMaterialDetails.getAmount().intValue()*pppMaterialDetails.getCostPrice().intValue());
                  outRepertoryDetails.setPayTag("K002-2");
                  outRepertoryDetailsMapper_yinshengjie.insert(outRepertoryDetails);
            }
            /**
             * 计算出产品总工序 production_processes
             */
            List<PppDetails> parentId = pppDetailsService_zhouzihao.findPppDetailsByParentId(produceMassage.getProductId());
            for (PppDetails pppDetails : parentId) {
                  ProductionProcesses productionProcesses = null;
                  productionProcesses = new ProductionProcesses();
                  productionProcesses.setParentId(promassage.getId());
                  productionProcesses.setDetailsNumber(pppDetails.getId());
                  productionProcesses.setProcedureId(pppDetails.getProcedureId());
                  productionProcesses.setProcedureName(pppDetails.getProcedureName());
                  productionProcesses.setLabourHourAmount(BigDecimal.valueOf(pppDetails.getLabourHourAmount().intValue()*Integer.valueOf(produceMassage.getAmount())));
//                  productionProcesses.setRealLabourHourAmount(BigDecimal.valueOf(pppDetails.getLabourHourAmount().intValue()*Integer.valueOf(produceMassage.getAmount())));
                  productionProcesses.setSubtotal(BigDecimal.valueOf(pppDetails.getSubtotal().intValue()*Integer.valueOf(produceMassage.getAmount())));
                  productionProcesses.setCostPrice(pppDetails.getCostPrice());
//                  productionProcesses.setRealSubtotal(BigDecimal.valueOf(pppDetails.getSubtotal().intValue()*Integer.valueOf(produceMassage.getAmount())));
                  productionProcesses.setProcedureFinishTag("G004-0");
                  productionProcesses.setProcedureTransferTag("G005-0");
                  productionProcessesMapper.insert(productionProcesses);
                  /**
                   * 最后物料
                   */
                  List<PppMaterialDetails> pppDetailsById = pppMaterialDetailsService_zhouzihao.findPppDetailsById(pppDetails.getId());
                  for (PppMaterialDetails pppDetails1 : pppDetailsById) {
                        ProductionProcessesMaterial p = null;
                        p = new ProductionProcessesMaterial();
                        p.setParentId(productionProcesses.getId());
                        p.setDetailsNumber(pppDetails1.getId());
                        p.setProductId(pppDetails1.getProductId());
                        p.setProductName(pppDetails1.getProductName());
                        p.setCostPrice(pppDetails1.getCostPrice());
                        p.setAmount(BigDecimal.valueOf(pppDetails1.getAmount().intValue()*Integer.valueOf(produceMassage.getAmount())));
                        p.setSubtotal(pppDetails1.getSubtotal());
                        p.setRenewAmount(BigDecimal.valueOf(0));
                        p.setRealAmount(BigDecimal.valueOf(0));
                        int insert2 = productionProcessesMaterialMapper.insert(p);
                  }
            }
            
            
            return insert;
      }

      /**
       * 根据状态查询
       * @param page
       * @param limit
       * @param status
       * @return
       */
      @Override
      public DataGridView findAllPage(Integer page, Integer limit,String status) {
            System.err.println(status);
            Page<ProduceMassage> page1 = new Page<>(page,limit);
            QueryWrapper<ProduceMassage> wrapper = new QueryWrapper<>();
            wrapper.eq(StringUtils.isNotBlank(status),"check_tag",status);
            Page<ProduceMassage> produceMassagePage = baseMapper.selectPage(page1, wrapper);
            return new DataGridView(produceMassagePage.getTotal(),produceMassagePage.getRecords());
      }

      /**
       * 审核
       */
      @Override
      public int Audit(Integer id, String status) {
            ProduceMassage massage = new ProduceMassage();
            massage.setCheckTag(status);
            QueryWrapper<ProduceMassage> produceMassageQueryWrapper = new QueryWrapper<>();
            produceMassageQueryWrapper.eq("id",id);
            return baseMapper.update(massage,produceMassageQueryWrapper);
      }



}
