package top.yangbuyi.production.yangshuai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.yangshuai.domain.PDetails;
import top.yangbuyi.production.yangshuai.domain.PDetailsExample;

import java.util.List;

public interface PDetails_yangbuyi_Mapper extends BaseMapper<PDetails> {
    long countByExample(PDetailsExample example);

    int deleteByExample(PDetailsExample example);

    List<PDetails> selectByExample(PDetailsExample example);

    int updateByExampleSelective(@Param("record") PDetails record, @Param("example") PDetailsExample example);

    int updateByExample(@Param("record") PDetails record, @Param("example") PDetailsExample example);
}