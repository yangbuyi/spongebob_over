package top.yangbuyi.production.zhouzihao.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "production_processes_material")
public class ProductionProcessesMaterial implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "details_number")
    private Integer detailsNumber;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "renew_amount")
    private BigDecimal renewAmount;

    @TableField(value = "real_amount")
    private BigDecimal realAmount;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    @TableField(value = "real_subtotal")
    private BigDecimal realSubtotal;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_DETAILS_NUMBER = "details_number";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_RENEW_AMOUNT = "renew_amount";

    public static final String COL_REAL_AMOUNT = "real_amount";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_REAL_SUBTOTAL = "real_subtotal";
}