package top.yangbuyi.production.zhouzihao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.production.zhouzihao.service.ProductFileService_zhouzihao;

@RestController
@RequestMapping("api/zhouzihao/ProductFile")
public class ProductFileController_zhouzihao {
      @Autowired
      private ProductFileService_zhouzihao productFileService;

      /**
      * @Description: 养猪先个人网址:http://yangzhuxian.top
      * @Param:
      * @return: 
      * @Author: Mr.zhou
      * @Date: 2020/7/16
      */
      @RequestMapping("findAll")
      public Object findAll(){
            return productFileService.selectByExample(null);
      }
}
