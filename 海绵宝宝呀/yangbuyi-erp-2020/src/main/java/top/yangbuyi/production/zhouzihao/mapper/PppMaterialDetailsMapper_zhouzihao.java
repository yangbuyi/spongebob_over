package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetails;
import top.yangbuyi.production.zhouzihao.domain.PppMaterialDetailsExample;

public interface PppMaterialDetailsMapper_zhouzihao extends BaseMapper<PppMaterialDetails> {
    long countByExample(PppMaterialDetailsExample example);

    int deleteByExample(PppMaterialDetailsExample example);

    List<PppMaterialDetails> selectByExample(PppMaterialDetailsExample example);

    int updateByExampleSelective(@Param("record") PppMaterialDetails record, @Param("example") PppMaterialDetailsExample example);

    int updateByExample(@Param("record") PppMaterialDetails record, @Param("example") PppMaterialDetailsExample example);
}