package top.yangbuyi.production.zhouzihao.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.production.zhouzihao.mapper.ProductionProcessesMaterialMapper;
import top.yangbuyi.production.zhouzihao.domain.ProductionProcessesMaterial;
import top.yangbuyi.production.zhouzihao.service.ProductionProcessesMaterialService;
@Service
public class ProductionProcessesMaterialServiceImpl extends ServiceImpl<ProductionProcessesMaterialMapper, ProductionProcessesMaterial> implements ProductionProcessesMaterialService{

}
