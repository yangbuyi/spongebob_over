package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.production.zhouzihao.domain.ProductionPlan;
import top.yangbuyi.production.zhouzihao.domain.ProductionPlanExample;

public interface ProductionPlanMapper_zhouzihao extends BaseMapper<ProductionPlan> {
    long countByExample(ProductionPlanExample example);

    int deleteByExample(ProductionPlanExample example);

    List<ProductionPlan> selectByExample(ProductionPlanExample example);

    int updateByExampleSelective(@Param("record") ProductionPlan record, @Param("example") ProductionPlanExample example);

    int updateByExample(@Param("record") ProductionPlan record, @Param("example") ProductionPlanExample example);
}