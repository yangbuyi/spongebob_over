package top.yangbuyi.production.zhouzihao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.production.zhouzihao.domain.ProductionProcessesMaterial;

public interface ProductionProcessesMaterialMapper extends BaseMapper<ProductionProcessesMaterial> {
}