package top.yangbuyi.product.liuchengyang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import top.yangbuyi.product.liuchengyang.domain.ProductKind;
import top.yangbuyi.product.liuchengyang.service.ProductFileService_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.ProductKindService_liuchengyang;
import top.yangbuyi.product.liuchengyang.vo.ProductFileVo;
import top.yangbuyi.system.common.ResultObj;

import java.util.List;

/**
 * @ClassName: ProductFileController
 * @Author: campsis-tk
 * @Date: 2020/7/15 21:58
 * @Description: 产品档案登记控制器
 **/
@RestController
@RequestMapping("api/liuchengyang/productfile")
public class ProductFileController_liuchengyang {

	@Autowired
	private ProductFileService_liuchengyang productFileServiceLiuchengyang;


	@Autowired
	private ProductKindService_liuchengyang productKindServiceLiuchengyang;

	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(删除状态为已删除[C001 - 2])
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileDelete")
	public Object queryAllProductFileDelete(ProductFileVo productFileVo) {
		return this.productFileServiceLiuchengyang.queryAllProductFileDelete(productFileVo);
	}

	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(审核状态为等待审核[S001 - 1])
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileByCheckTag")
	public Object queryAllProductFileByCheckTag(ProductFileVo productFileVo) {
		return this.productFileServiceLiuchengyang.queryAllProductFileByCheckTag(productFileVo);
	}

	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(审核状态为审核通过[S001 - 2])
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileByCheckTagS2")
	public Object queryAllProductFileByCheckTagS2(ProductFileVo productFileVo) {
		return this.productFileServiceLiuchengyang.queryAllProductFileByCheckTagS2(productFileVo);
	}

	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(删除状态为未删除[C001 - 1])
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileByDeleteTag")
	public Object queryAllProductFileByDeleteTag(ProductFileVo productFileVo) {
		return this.productFileServiceLiuchengyang.queryAllProductFileByDeleteTag(productFileVo);
	}


	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(类型为商品的数据)
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileByGoods")
	public Object queryAllProductFileByGoods(ProductFileVo productFileVo) {
		return this.productFileServiceLiuchengyang.queryAllProductFileByGoods(productFileVo);
	}

	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(类型为物料的数据)
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileByMaterial")
	public Object queryAllProductFileByMaterial(ProductFileVo productFileVo) {
		return this.productFileServiceLiuchengyang.queryAllProductFileByMaterial(productFileVo);
	}

	/**
	 * @param kindName
	 * @Description: 根据分类查询该类别下的物料
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@GetMapping("queryAllProductFileByMaterialByClass")
	public Object queryAllProductFileByMaterialByClass(String kindName) {
		return this.productFileServiceLiuchengyang.queryAllProductFileByMaterialByClass(kindName);
		// return this.productFileServiceLiuchengyang.queryAllProductFileByMaterial(productFileVo);
	}


	/**
	 * @param productFile
	 * @Description: 保存产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("addProductFile")
	public Object addProductFile(ProductFile productFile) {
		try {
			/**
			 * 设置选中的一级分类
			 */
			ProductKind firstKid = this.productKindServiceLiuchengyang.getById(productFile.getFirstKindId());
			productFile.setFirstKindId(firstKid.getKindId());
			productFile.setFirstKindName(firstKid.getKindName());
			/**
			 * 设置选中的二级分类
			 */
			ProductKind secondKid = this.productKindServiceLiuchengyang.getById(productFile.getSecondKindId());
			productFile.setSecondKindId(secondKid.getKindId());
			productFile.setSecondKindName(secondKid.getKindName());
			/**
			 * 设置选中的三级分类
			 */
			ProductKind thirdKid = this.productKindServiceLiuchengyang.getById(productFile.getThirdKindId());
			productFile.setThirdKindId(thirdKid.getKindId());
			productFile.setThirdKindName(thirdKid.getKindName());

			this.productFileServiceLiuchengyang.saveProductFile(productFile);
			return ResultObj.ADD_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.ADD_ERROR;
		}
	}

	/**
	 * @param productFile
	 * @Description: 修改产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("updateProductFile")
	public Object updateProductFile(ProductFile productFile) {
		try {
			/**
			 * 设置选中的一级分类
			 */
			ProductKind firstKid = this.productKindServiceLiuchengyang.getById(productFile.getFirstKindId());
			productFile.setFirstKindId(firstKid.getKindId());
			productFile.setFirstKindName(firstKid.getKindName());
			/**
			 * 设置选中的二级分类
			 */
			ProductKind secondKid = this.productKindServiceLiuchengyang.getById(productFile.getSecondKindId());
			productFile.setSecondKindId(secondKid.getKindId());
			productFile.setSecondKindName(secondKid.getKindName());
			/**
			 * 设置选中的三级分类
			 */
			ProductKind thirdKid = this.productKindServiceLiuchengyang.getById(productFile.getThirdKindId());
			productFile.setThirdKindId(thirdKid.getKindId());
			productFile.setThirdKindName(thirdKid.getKindName());

			this.productFileServiceLiuchengyang.updateProductFile(productFile);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param id
	 * @Description: 永久删除产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("deleteProductFile")
	public Object deleteProductFile(Integer id) {
		try {
			this.productFileServiceLiuchengyang.removeById(id);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param ids
	 * @Description: 永久批量删除产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("batchDeleteProductFile")
	public Object batchDeleteProductFile(Integer[] ids) {
		try {
			this.productFileServiceLiuchengyang.batchDeleteProductFile(ids);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param id
	 * @Description: 状态删除产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("updateDeleteProductFile")
	public Object updateDeleteProductFile(Integer id) {
		try {
			this.productFileServiceLiuchengyang.updateDeleteProductFile(id);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param ids
	 * @Description: 删除状态恢复产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("updateDeleteBackProductFile")
	public Object updateDeleteBackProductFile(Integer[] ids) {
		try {
			this.productFileServiceLiuchengyang.updateDeleteBackProductFile(ids);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param ids
	 * @Description: 审核通过产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("updateCheckYesProductFile")
	public Object updateCheckYesProductFile(Integer[] ids) {
		try {
			this.productFileServiceLiuchengyang.updateCheckYesProductFile(ids);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param ids
	 * @Description: 审核不通过产品档案记录
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("updateCheckNoProductFile")
	public Object updateCheckNoProductFile(Integer[] ids) {
		try {
			this.productFileServiceLiuchengyang.updateCheckNoProductFile(ids);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param productFile
	 * @Description: 添加产品档案记录产品的关系
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("addProductFileAll")
	public Object addProductFileAll(@RequestBody List<ProductFile> productFile) {
		try {
			this.productFileServiceLiuchengyang.addProductFileAll(productFile);
			return ResultObj.ADD_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.ADD_ERROR;
		}
	}

	/**
	 * @param productFile
	 * @Description: 修改产品档案记录产品的关系
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("updProductFileAll")
	public Object updProductFileAll(@RequestBody List<ProductFile> productFile) {
		try {
			this.productFileServiceLiuchengyang.updProductFileAll(productFile);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @param id 商品id
	 * @Description: 删除产品档案记录产品的关系
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	@RequestMapping("deleteProductAll")
	public Object deleteProductAll(Integer id) {
		try {
			this.productFileServiceLiuchengyang.deleteProductAll(id);
			return ResultObj.DELETE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.DELETE_ERROR;
		}
	}
}



