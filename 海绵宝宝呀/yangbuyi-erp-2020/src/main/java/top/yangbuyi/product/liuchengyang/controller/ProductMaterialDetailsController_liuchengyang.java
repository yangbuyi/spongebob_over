package top.yangbuyi.product.liuchengyang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.product.liuchengyang.service.ProductMaterialDetailsService_liuchengyang;

/**
 * @ClassName: ProductMaterialDetailsController
 * @Author: campsis-tk
 * @Date: 2020/7/17 19:31
 * @Description:
 **/
@RestController
@RequestMapping("/api/liuchengyang/ProductMaterialDetails")
public class ProductMaterialDetailsController_liuchengyang {

	@Autowired
	private ProductMaterialDetailsService_liuchengyang productMaterialDetailsServiceLiuchengyang;

	/**
	 * @Description: 根据产品id查出对应的物料
	 * @param parentId
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@GetMapping("getProductMaterialDetailsBy")
	public Object getProductMaterialDetailsBy(Integer parentId){
		return this.productMaterialDetailsServiceLiuchengyang.getProductMaterialDetailsBy(parentId);
	}
}
