package top.yangbuyi.product.liuchengyang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.product.liuchengyang.domain.DropDownBox;
import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterial;
import top.yangbuyi.product.liuchengyang.mapper.ProductFileMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.mapper.ProductMaterialMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.DropDownBoxService_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.ProductFileService_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.ProductMaterialDetailsService_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.ProductMaterialService_liuchengyang;
import top.yangbuyi.product.liuchengyang.vo.ProductFileVo;
import top.yangbuyi.system.common.ActiveUser;
import top.yangbuyi.system.common.Constant;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @ClassName: ProductFileServiceLiuchengyangImpl
 * @Author: campsis-tk
 * @Date: 2020/7/15 21:53
 * @Description: 产品档案登记服务层
 **/
@Slf4j
@Service
public class ProductFileServiceLiuchengyangImpl extends ServiceImpl<ProductFileMapper_liuchengyang, ProductFile> implements ProductFileService_liuchengyang {
      
      @Autowired
      private ProductFileMapper_liuchengyang productFileMapperLiuchengyang;
      
      @Autowired
      private DropDownBoxService_liuchengyang dropDownBoxServiceLiuchengyang;
      
      @Autowired
      private ProductMaterialDetailsService_liuchengyang productMaterialDetailsServiceLiuchengyang;
      
      @Autowired
      private ProductMaterialService_liuchengyang productMaterialServiceLiuchengyang;
      
      @Autowired
      private ProductMaterialMapper_liuchengyang productMaterialMapperLiuchengyang;
      
      
      @Override
      /**
       * @Description: 查询产品档案登记(删除状态为C001 - 2)
       * @param productFileVo
       * @return: top.yangbuyi.system.common.DataGridView
       * @Author: campsis-tk
       * @Date: 2020/7/17
       */
      public DataGridView queryAllProductFileDelete(ProductFileVo productFileVo) {
            IPage<ProductFile> page = new Page<>(productFileVo.getPage(), productFileVo.getLimit());
            QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
            wrapper.eq("delete_tag", Constant.DELETE_C001_2);
            wrapper.like(StringUtils.isNotBlank(productFileVo.getProductName()), "product_name", productFileVo.getProductName());
            wrapper.eq(StringUtils.isNotBlank(productFileVo.getCheckTag()), "check_tag", productFileVo.getCheckTag());
            wrapper.ge(productFileVo.getStartTime() != null, "register_time", productFileVo.getStartTime());
            wrapper.le(productFileVo.getEndTime() != null, "register_time", productFileVo.getEndTime());
            wrapper.orderByDesc("register_time");
            this.productFileMapperLiuchengyang.selectPage(page, wrapper);
            List<ProductFile> records = page.getRecords();
            for (ProductFile record : records) {
                  if (record.getCheckTag() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryTextDescribeByCheckTag(record.getCheckTag());
                        record.setAuditStatus(dropDownBox.getTextDescribe());
                  }
                  if (record.getType() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByType(record.getType());
                        record.setTheType(dropDownBox.getTextDescribe());
                  }
                  if (record.getProductClass() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByLevel(record.getProductClass());
                        record.setLevel(dropDownBox.getTextDescribe());
                  }
            }
            return new DataGridView(page.getTotal(), records);
      }
      
      
      @Override
      /**
       * @Description: 查询产品档案登记(审核状态为S001 - 1)
       * @param productFileVo
       * @return: top.yangbuyi.system.common.DataGridView
       * @Author: campsis-tk
       * @Date: 2020/7/17
       */
      public DataGridView queryAllProductFileByCheckTag(ProductFileVo productFileVo) {
            IPage<ProductFile> page = new Page<>(productFileVo.getPage(), productFileVo.getLimit());
            QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
            wrapper.eq("delete_tag", Constant.DELETE_C001_1);//删除状态
            wrapper.eq("check_tag", Constant.APPROVE_WAITE);//等待审核
            wrapper.like(StringUtils.isNotBlank(productFileVo.getProductName()), "product_name", productFileVo.getProductName());
            wrapper.ge(productFileVo.getStartTime() != null, "register_time", productFileVo.getStartTime());
            wrapper.le(productFileVo.getEndTime() != null, "register_time", productFileVo.getEndTime());
            wrapper.orderByDesc("register_time");
            this.productFileMapperLiuchengyang.selectPage(page, wrapper);
            List<ProductFile> records = page.getRecords();
            for (ProductFile record : records) {
                  if (record.getCheckTag() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryTextDescribeByCheckTag(record.getCheckTag());
                        record.setAuditStatus(dropDownBox.getTextDescribe());
                  }
                  if (record.getType() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByType(record.getType());
                        record.setTheType(dropDownBox.getTextDescribe());
                  }
                  if (record.getProductClass() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByLevel(record.getProductClass());
                        record.setLevel(dropDownBox.getTextDescribe());
                  }
            }
            return new DataGridView(page.getTotal(), records);
      }
      
      @Override
      /**
       * @Description: 查询产品档案登记(审核状态为S001 - 2)
       * @param productFileVo
       * @return: top.yangbuyi.system.common.DataGridView
       * @Author: campsis-tk
       * @Date: 2020/7/17
       */
      public DataGridView queryAllProductFileByCheckTagS2(ProductFileVo productFileVo) {
            IPage<ProductFile> page = new Page<>(productFileVo.getPage(), productFileVo.getLimit());
            QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
            wrapper.eq("delete_tag", Constant.DELETE_C001_1);//删除状态
            wrapper.eq("check_tag", Constant.APPROVE_YES);//审核通过
            wrapper.like(StringUtils.isNotBlank(productFileVo.getProductName()), "product_name", productFileVo.getProductName());
            wrapper.ge(productFileVo.getStartTime() != null, "register_time", productFileVo.getStartTime());
            wrapper.le(productFileVo.getEndTime() != null, "register_time", productFileVo.getEndTime());
            wrapper.orderByDesc("register_time");
            this.productFileMapperLiuchengyang.selectPage(page, wrapper);
            List<ProductFile> records = page.getRecords();
            for (ProductFile record : records) {
                  if (record.getCheckTag() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryTextDescribeByCheckTag(record.getCheckTag());
                        record.setAuditStatus(dropDownBox.getTextDescribe());
                  }
                  if (record.getType() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByType(record.getType());
                        record.setTheType(dropDownBox.getTextDescribe());
                  }
                  if (record.getProductClass() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByLevel(record.getProductClass());
                        record.setLevel(dropDownBox.getTextDescribe());
                  }
            }
            return new DataGridView(page.getTotal(), records);
      }
      
      @Override
      /**
       * @Description: 查询产品档案登记(删除状态为C001 - 1)
       * @param productFileVo
       * @return: top.yangbuyi.system.common.DataGridView
       * @Author: campsis-tk
       * @Date: 2020/7/17
       */
      public DataGridView queryAllProductFileByDeleteTag(ProductFileVo productFileVo) {
            IPage<ProductFile> page = new Page<>(productFileVo.getPage(), productFileVo.getLimit());
            QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
            wrapper.eq("delete_tag", Constant.DELETE_C001_1);
            wrapper.like(StringUtils.isNotBlank(productFileVo.getProductName()), "product_name", productFileVo.getProductName());
            wrapper.eq(StringUtils.isNotBlank(productFileVo.getCheckTag()), "check_tag", productFileVo.getCheckTag());
            wrapper.ge(productFileVo.getStartTime() != null, "register_time", productFileVo.getStartTime());
            wrapper.le(productFileVo.getEndTime() != null, "register_time", productFileVo.getEndTime());
            wrapper.orderByDesc("register_time");
            this.productFileMapperLiuchengyang.selectPage(page, wrapper);
            List<ProductFile> records = page.getRecords();
            for (ProductFile record : records) {
                  if (record.getCheckTag() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryTextDescribeByCheckTag(record.getCheckTag());
                        record.setAuditStatus(dropDownBox.getTextDescribe());
                  }
                  if (record.getType() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByType(record.getType());
                        record.setTheType(dropDownBox.getTextDescribe());
                  }
                  if (record.getProductClass() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByLevel(record.getProductClass());
                        record.setLevel(dropDownBox.getTextDescribe());
                  }
            }
            return new DataGridView(page.getTotal(), records);
      }
      
      @Override
      /**
       * @param productFileVo
       * @Description: 查询产品档案登记(类型为商品的数据)
       * @return: java.lang.Object
       * @Author: campsis-tk
       * @Date: 2020/7/15
       */
      public DataGridView queryAllProductFileByGoods(ProductFileVo productFileVo) {
            IPage<ProductFile> page = new Page<>(productFileVo.getPage(), productFileVo.getLimit());
            QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
            wrapper.eq("type", Constant.TYPE_Y001_1);//类型为商品
            wrapper.eq("delete_tag", Constant.DELETE_C001_1);//删除状态
            wrapper.eq("check_tag", Constant.APPROVE_YES);//审核通过
            wrapper.eq("design_module_tag", Constant.DESGIN_NO);//未设计
            wrapper.like(StringUtils.isNotBlank(productFileVo.getProductName()), "product_name", productFileVo.getProductName());
            wrapper.ge(productFileVo.getStartTime() != null, "register_time", productFileVo.getStartTime());
            wrapper.le(productFileVo.getEndTime() != null, "register_time", productFileVo.getEndTime());
            wrapper.orderByDesc("register_time");
            this.productFileMapperLiuchengyang.selectPage(page, wrapper);
            List<ProductFile> records = page.getRecords();
            for (ProductFile record : records) {
                  if (record.getCheckTag() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryTextDescribeByCheckTag(record.getCheckTag());
                        record.setAuditStatus(dropDownBox.getTextDescribe());
                  }
                  if (record.getType() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByType(record.getType());
                        record.setTheType(dropDownBox.getTextDescribe());
                  }
                  if (record.getProductClass() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByLevel(record.getProductClass());
                        record.setLevel(dropDownBox.getTextDescribe());
                  }
            }
            return new DataGridView(page.getTotal(), records);
      }
      
      @Override
      /**
       * @param productFileVo
       * @Description: 查询产品档案登记(类型为物料的数据)
       * @return: DataGridView
       * @Author: campsis-tk
       * @Date: 2020/7/17
       */
      public DataGridView queryAllProductFileByMaterial(ProductFileVo productFileVo) {
            IPage<ProductFile> page = new Page<>(productFileVo.getPage(), productFileVo.getLimit());
            QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
            wrapper.eq("type", Constant.TYPE_Y001_2);//类型为物料
            wrapper.eq("delete_tag", Constant.DELETE_C001_1);//删除状态
            wrapper.eq("check_tag", Constant.APPROVE_YES);//审核通过
            wrapper.like(StringUtils.isNotBlank(productFileVo.getProductName()), "product_name", productFileVo.getProductName());
            wrapper.ge(productFileVo.getStartTime() != null, "register_time", productFileVo.getStartTime());
            wrapper.le(productFileVo.getEndTime() != null, "register_time", productFileVo.getEndTime());
            wrapper.orderByDesc("register_time");
            this.productFileMapperLiuchengyang.selectPage(page, wrapper);
            List<ProductFile> records = page.getRecords();
            for (ProductFile record : records) {
                  if (record.getCheckTag() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryTextDescribeByCheckTag(record.getCheckTag());
                        record.setAuditStatus(dropDownBox.getTextDescribe());
                  }
                  if (record.getType() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByType(record.getType());
                        record.setTheType(dropDownBox.getTextDescribe());
                  }
                  if (record.getProductClass() != null) {
                        DropDownBox dropDownBox = this.dropDownBoxServiceLiuchengyang.queryByLevel(record.getProductClass());
                        record.setLevel(dropDownBox.getTextDescribe());
                  }
            }
            return new DataGridView(page.getTotal(), records);
      }
      
      @Override
      public ProductFile addProductFileAll(List<ProductFile> productFile) {
            System.err.println(productFile);
            ProductMaterial productMaterial = null;
            for (ProductFile file : productFile) {
                  if (file.getType().equals(Constant.TYPE_Y001_1)) {
                        productMaterial = this.productMaterialServiceLiuchengyang.addProductMaterial(file);
                  }
            }
            this.productMaterialDetailsServiceLiuchengyang.addProductMaterialDetails(productFile, productMaterial.getId());
            return null;
      }
      
      @Override
      public void deleteProductAll(Integer id) {
            //更新file表中的设计状态
            ProductMaterial productMaterial = this.productMaterialMapperLiuchengyang.selectById(id);
//		     select * from product_file where id = #{id};
//            QueryWrapper<ProductFile> productFileQueryWrapper = new QueryWrapper<>();
//            productFileQueryWrapper.eq("product_id", productMaterial.getProductId());
//            ProductFile productFile = this.productFileMapperLiuchengyang.selectOne(productFileQueryWrapper);
//            ProductFile productFile = this.productFileMapperLiuchengyang.selectById(productMaterial.getProductId());
            ProductFile productFile = this.productFileMapperLiuchengyang.selectByProductId(productMaterial.getProductId());
            productFile.setDesignModuleTag(Constant.DESGIN_NO);
            this.productFileMapperLiuchengyang.updateById(productFile);
            //删除产品表中
            this.productMaterialServiceLiuchengyang.removeById(id);
            //删除物料表中的
            this.productMaterialDetailsServiceLiuchengyang.deleteByProductMaterialId(id);
      }
      
      @Override
      public ProductFile updProductFileAll(List<ProductFile> productFile) {
            for (ProductFile file : productFile) {
                  if (file.getType() == null) {
                        this.productMaterialServiceLiuchengyang.updProductMaterial(file);
                  }
            }
            this.productMaterialDetailsServiceLiuchengyang.updProductMaterialDetails(productFile);
            return null;
      }
      
      @Override
      public DataGridView queryAllProductFileByMaterialByClass(String kindName) {
            if (kindName.equals("电子")) {
                  IPage<ProductFile> page = new Page<>();
                  QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
                  wrapper.eq("third_kind_name", "计算机组件");
                  wrapper.eq("check_tag", Constant.APPROVE_YES);
                  wrapper.eq("delete_tag", Constant.DELETE_C001_1);
                  this.productFileMapperLiuchengyang.selectPage(page, wrapper);
                  return new DataGridView(page.getTotal(), page.getRecords());
            } else {
                  IPage<ProductFile> page = new Page<>();
                  QueryWrapper<ProductFile> wrapper = new QueryWrapper<>();
                  wrapper.eq("third_kind_name", "自行车组件");
                  wrapper.eq("check_tag", Constant.APPROVE_YES);
                  wrapper.eq("delete_tag", Constant.DELETE_C001_1);
                  this.productFileMapperLiuchengyang.selectPage(page, wrapper);
                  return new DataGridView(page.getTotal(), page.getRecords());
            }
      }
      
      
      @Override
      /**
       * @Description: 添加产品设计物料关联商品表和物料表
       * @param productFile
       * @return: top.yangbuyi.product.liuchengyang.domain.ProductFile
       * @Author: campsis-tk
       * @Date: 2020/7/21
       */
      public ProductFile saveProductFile(ProductFile productFile) {
            //设置产品档案的流水号
            Random random = new Random();
            int number = random.nextInt(99999) + 1;
            log.info("产品档案的流水号: " + number);
            ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
            productFile.setRegister(activeUser.getUser().getName());    //设置设计人
            productFile.setProductId("100" + productFile.getFirstKindId() + "" + productFile.getSecondKindId() + "" + productFile.getThirdKindId() + "" + number);
            productFile.setRegisterTime(new Date());
            productFile.setCheckTag("S001-1");           //审核状态 默认等待审核
            productFile.setChangeTag("D002-1");          //
            productFile.setPriceChangeTag("J001-1");     //
            productFile.setDeleteTag("C001-1");          //删除状态  默认不删除
            productFile.setDesignCellTag("K001-1");      //
            productFile.setDesignModuleTag("W001-1");    //
            productFile.setDesignProcedureTag("G001-1"); //
            productFile.setFileChangeAmount(0);
            log.info("ProductFile: " + productFile.toString());
            this.productFileMapperLiuchengyang.insert(productFile);
            return productFile;
      }
      
      @Override
      public ProductFile updateProductFile(ProductFile productFile) {
            productFile.setCheckTag("S001-1"); //修改产品档案之后需要再次审核
            if (productFile.getFileChangeAmount() != null) {
                  ProductFile changeAmount = this.productFileMapperLiuchengyang.selectById(productFile.getFileChangeAmount());
                  productFile.setFileChangeAmount(changeAmount.getFileChangeAmount() + 1);    //修改次数
            }
            this.productFileMapperLiuchengyang.updateById(productFile);
            return productFile;
      }
      
      @Override
      public ProductFile updateDeleteProductFile(Integer id) {
            ProductFile productFile = this.productFileMapperLiuchengyang.selectById(id);
            //修改删除状态
            productFile.setDeleteTag("C001-2");
            if (productFile.getFileChangeAmount() != null) {
                  ProductFile changeAmount = this.productFileMapperLiuchengyang.selectById(productFile.getFileChangeAmount());
//			productFile.setFileChangeAmount(changeAmount.getFileChangeAmount() + 1);    //修改次数
            }
            this.productFileMapperLiuchengyang.updateById(productFile);
            return productFile;
      }
      
      @Override
      public ProductFile updateCheckYesProductFile(Integer[] ids) {
            ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
            if (ids != null) {
                  ProductFile productFile = null;
                  for (Integer id : ids) {
                        productFile = this.productFileMapperLiuchengyang.selectById(id);
                        //修改审核状态  通过
//				productFile.setFileChangeAmount(productFile.getFileChangeAmount() + 1);    //修改次数
                        productFile.setCheckTag(Constant.APPROVE_YES);
                        productFile.setChanger(activeUser.getUser().getName());    //设置审核人
                        productFile.setCheckTime(new Date());           //设置审核时间
                        this.productFileMapperLiuchengyang.updateById(productFile);
                  }
                  return productFile;
            } else {
                  return null;
            }
      }
      
      @Override
      public ProductFile updateCheckNoProductFile(Integer[] ids) {
            ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
            if (ids != null) {
                  ProductFile productFile = null;
                  for (Integer id : ids) {
                        productFile = this.productFileMapperLiuchengyang.selectById(id);
                        //修改审核状态  不通过
                        productFile.setFileChangeAmount(productFile.getFileChangeAmount() + 1);    //修改次数
                        productFile.setCheckTag(Constant.APPROVE_NO);
                        productFile.setChanger(activeUser.getUser().getName());    //设置审核人
                        productFile.setCheckTime(new Date());           //设置审核时间
                        this.productFileMapperLiuchengyang.updateById(productFile);
                  }
                  return productFile;
            } else {
                  return null;
            }
      }
      
      @Override
      public ProductFile updateDeleteBackProductFile(Integer[] ids) {
            ProductFile productFile = null;
            if (ids != null) {
                  for (Integer id : ids) {
                        productFile = this.productFileMapperLiuchengyang.selectById(id);
                        //修改删除状态
                        productFile.setFileChangeAmount(productFile.getFileChangeAmount() + 1);    //修改次数
                        productFile.setDeleteTag("C001-1");
                        this.productFileMapperLiuchengyang.updateById(productFile);
                  }
                  return productFile;
            } else {
                  return null;
            }
      }
      
      @Override
      public void batchDeleteProductFile(Integer[] ids) {
            if (ids != null) {
                  for (Integer id : ids) {
                        this.productFileMapperLiuchengyang.deleteById(id);
                  }
            }
      }
      
      @Override
      public ProductFile getById(Serializable id) {
            return super.getById(id);
      }
}
