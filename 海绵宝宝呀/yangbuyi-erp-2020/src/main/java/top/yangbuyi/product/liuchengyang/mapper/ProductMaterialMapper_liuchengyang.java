package top.yangbuyi.product.liuchengyang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterial;

/**
  * @ClassName: ProductMaterialMapper_liuchengyang
  * @Author: campsis-tk
  * @Date: 2020/7/19 12:29
  * @Description:
  **/
public interface ProductMaterialMapper_liuchengyang extends BaseMapper<ProductMaterial> {
}