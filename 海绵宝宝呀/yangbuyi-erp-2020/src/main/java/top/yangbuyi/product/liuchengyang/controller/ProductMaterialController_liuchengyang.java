package top.yangbuyi.product.liuchengyang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterial;
import top.yangbuyi.product.liuchengyang.service.ProductMaterialService_liuchengyang;
import top.yangbuyi.product.liuchengyang.vo.ProductMaterialVo;
import top.yangbuyi.system.common.ResultObj;

/**
 * @ClassName: ProductMaterialController
 * @Author: campsis-tk
 * @Date: 2020/7/19 12:30
 * @Description:
 **/
@RestController
@RequestMapping("/api/liuchengyang/ProductMaterial")
public class ProductMaterialController_liuchengyang {

	@Autowired
	private ProductMaterialService_liuchengyang productMaterialServiceLiuchengyang;

	/**
	 * @Description: 查询商品表
	 * @param productMaterialVo
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@GetMapping("queryAllProductMaterial")
	public Object queryAllProductMaterial(ProductMaterialVo productMaterialVo) {
		return this.productMaterialServiceLiuchengyang.queryAllProductMaterial(productMaterialVo);
	}

	/**
	 * @Description: 查询商品表中未审核的
	 * @param productMaterialVo
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@GetMapping("queryAllProductMaterialByCheck")
	public Object queryAllProductMaterialByCheck(ProductMaterialVo productMaterialVo) {
		return this.productMaterialServiceLiuchengyang.queryAllProductMaterialByCheck(productMaterialVo);
	}

	/**
	 * @Description: 修改商品表的商品
	 * @param productMaterial
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@RequestMapping("updateProductMaterial")
	public Object updateProductMaterial(ProductMaterial productMaterial){
		try {
			this.productMaterialServiceLiuchengyang.updateProductMaterial(productMaterial);
			return ResultObj.UPDATE_SUCCESS;
		}catch (Exception e){
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @Description: 审核通过产品档案记录
	 * @param ids
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@RequestMapping("updateCheckYesProductMaterial")
	public Object updateCheckYesProductMaterial(Integer[] ids){
		try {
			this.productMaterialServiceLiuchengyang.updateCheckYesProductMaterial(ids);
			return ResultObj.UPDATE_SUCCESS;
		}catch (Exception e){
			return ResultObj.UPDATE_ERROR;
		}
	}

	/**
	 * @Description: 审核不通过产品档案记录
	 * @param ids
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@RequestMapping("updateCheckNoProductMaterial")
	public Object updateCheckNoProductMaterial(Integer[] ids){
		try {
			this.productMaterialServiceLiuchengyang.updateCheckNoProductMaterial(ids);
			return ResultObj.UPDATE_SUCCESS;
		}catch (Exception e){
			return ResultObj.UPDATE_ERROR;
		}
	}

}
