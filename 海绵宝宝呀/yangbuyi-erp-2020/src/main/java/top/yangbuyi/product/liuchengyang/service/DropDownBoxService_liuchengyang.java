package top.yangbuyi.product.liuchengyang.service;

import top.yangbuyi.product.liuchengyang.domain.DropDownBox;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.common.DataGridView;

/**
 * @ClassName: DropDownBoxService_liuchengyang
 * @Author: campsis-tk
 * @Date: 2020/7/16 9:02
 * @Description:
 **/
public interface DropDownBoxService_liuchengyang extends IService<DropDownBox> {

	/**
	 * @Description: 审核下拉框查询
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DataGridView queryAllCheckTag();

	/**
	 * @Description: 用途类型下拉框查询
	 * @return:
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DataGridView queryAllType();


	/**
	 * @Description: 用途类型下拉框查询
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DataGridView queryAllLevel();

	/**
	 * @Description: 根据checkTag查询DropDownBox
	 * @return:
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DropDownBox queryTextDescribeByCheckTag(String checkTag);

	/**
	 * @Description: 根据type查询DropDownBox
	 * @param type
	 * @return: DropDownBox
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DropDownBox queryByType(String type);

	/**
	 * @Description: 根据levle查询DropDownBox
	 * @param level
	 * @return:
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	DropDownBox queryByLevel(String level);
}
