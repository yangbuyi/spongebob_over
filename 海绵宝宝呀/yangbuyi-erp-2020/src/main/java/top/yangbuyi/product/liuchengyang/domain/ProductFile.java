package top.yangbuyi.product.liuchengyang.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
  * @ClassName: ProductFile
  * @Author: campsis-tk
  * @Date: 2020/7/15 21:53
  * @Description:
  **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductFile implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

	/**
	 * 产品ID
	 */
	@TableField(value = "product_id")
    private String productId;

	/**
	 * 产品名称
	 */
    @TableField(value = "product_name")
    private String productName;

	/**
	 * 厂商
	 */
	@TableField(value = "factory_name")
    private String factoryName;

	/**
	 * 一级分类ID
	 */
    @TableField(value = "first_kind_id")
    private String firstKindId;

	/**
	 * 一级分类名称
	 */
    @TableField(value = "first_kind_name")
    private String firstKindName;

	/**
	 * 二级分类ID
	 */
    @TableField(value = "second_kind_id")
    private String secondKindId;

	/**
	 * 二级分类名称
	 */
    @TableField(value = "second_kind_name")
    private String secondKindName;

	/**
	 * 三级分类ID
	 */
    @TableField(value = "third_kind_id")
    private String thirdKindId;

	/**
	 * 三级分类名称
	 */
    @TableField(value = "third_kind_name")
    private String thirdKindName;

	/**
	 * 简介
	 */
    @TableField(value = "product_nick")
    private String productNick;

	/**
	 * 用途类型
	 */
	@TableField(value = "type")
    private String type;

	/**
	 * 产品档次
	 */
	@TableField(value = "product_class")
    private String productClass;

	/**
	 * 计量单位
	 */
    @TableField(value = "personal_unit")
    private String personalUnit;

	/**
	 * 计量值
	 */
	@TableField(value = "personal_value")
    private String personalValue;

	/**
	 * 供应商集合
	 */
    @TableField(value = "provider_group")
    private String providerGroup;

	/**
	 * 保修期
	 */
	@TableField(value = "warranty")
    private String warranty;

	/**
	 * 替代品名称
	 */
    @TableField(value = "twin_name")
    private String twinName;

	/**
	 * 替代品编号
	 */
    @TableField(value = "twin_id")
    private String twinId;

	/**
	 * 生命周期
	 */
	@TableField(value = "lifecycle")
    private String lifecycle;

	/**
	 * 市场单价
	 */
    @TableField(value = "list_price")
    private BigDecimal listPrice;

	/**
	 * 计划成本单价
	 */
	@TableField(value = "cost_price")
    private BigDecimal costPrice;

	/**
	 * 成本单价
	 */
    @TableField(value = "real_cost_price")
    private BigDecimal realCostPrice;

	/**
	 * 单位
	 */
    @TableField(value = "amount_unit")
    private String amountUnit;

	/**
	 * 产品描述
	 */
    @TableField(value = "product_describe")
    private String productDescribe;

	/**
	 * 产品经理
	 */
    @TableField(value = "responsible_person")
    private String responsiblePerson;

	/**
	 * 登记人
	 */
    @TableField(value = "register")
    private String register;

	/**
	 * 建档时间
	 */
	@TableField(value = "register_time")
    private Date registerTime;

	/**
	 * 审核人
	 */
    @TableField(value = "checker")
    private String checker;

	/**
	 * 审核时间
	 */
    @TableField(value = "check_time")
    private Date checkTime;

	/**
	 * 审核状态
	 * S001-0: 等待审核
	 * S001-1: 审核通过
	 * S001-2: 审核不通过
	 */
	@TableField(value = "check_tag")
    private String checkTag;

	/**
	 * 变更人
	 */
    @TableField(value = "changer")
    private String changer;

	/**
	 * 变更时间
	 */
	@TableField(value = "change_time")
    private Date changeTime;

	/**
	 * 档案变更标志
	 * D002-0: 未变更
	 * D002-1: 已变更
	 */
    @TableField(value = "change_tag")
    private String changeTag;

	/**
	 *价格变更标志
	 * J001-0：未变更
	 * J001-1：已变更
	 */
    @TableField(value = "price_change_tag")
    private String priceChangeTag;

	/**
	 * 档案变更累计
	 */
    @TableField(value = "file_change_amount")
    private Integer fileChangeAmount;

	/**
	 * 产品删除标志
	 * C001-0: 未删除
	 * C001-1: 已删除
	 */
    @TableField(value = "delete_tag")
    private String deleteTag;

	/**
	 * 物料组成标志
	 * W001-0: 未设计
	 * W001-1: 已设计
	 */
    @TableField(value = "design_module_tag")
    private String designModuleTag;

	/**
	 * 工序组成标志
	 * G001-0: 未设计
	 * G001-1: 已设计
	 */
    @TableField(value = "design_procedure_tag")
    private String designProcedureTag;

	/**
	 * 库存分配标志
	 * K001-0: 未设计
	 * K001-1: 已设计
	 */
    @TableField(value = "design_cell_tag")
    private String designCellTag;

	/**
	 * 用途类型-自写
	 */
    @TableField(exist = false)
	private String auditStatus;

	/**
	 * 审核状态-自写
	 */
	@TableField(exist = false)
	private String theType;

	/**
	 * 档次
	 */
	@TableField(exist = false)
	private String level;

	/**
	 * 小计
	 */
	@TableField(exist = false)
	private BigDecimal subtotal;

	/**
	 * 总价
	 */
	@TableField(exist = false)
	private BigDecimal alltotal;

	/**
	 * 设计要求
	 */
	@TableField(exist = false)
	private String moduleDescribe;

	/**
	 * 数量
	 */
	@TableField(exist = false)
	private BigDecimal amount;

	/**
	 * 描述
	 */
	@TableField(exist = false)
	private String describe;

	private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_FACTORY_NAME = "factory_name";

    public static final String COL_FIRST_KIND_ID = "first_kind_id";

    public static final String COL_FIRST_KIND_NAME = "first_kind_name";

    public static final String COL_SECOND_KIND_ID = "second_kind_id";

    public static final String COL_SECOND_KIND_NAME = "second_kind_name";

    public static final String COL_THIRD_KIND_ID = "third_kind_id";

    public static final String COL_THIRD_KIND_NAME = "third_kind_name";

    public static final String COL_PRODUCT_NICK = "product_nick";

    public static final String COL_TYPE = "type";

    public static final String COL_PRODUCT_CLASS = "product_class";

    public static final String COL_PERSONAL_UNIT = "personal_unit";

    public static final String COL_PERSONAL_VALUE = "personal_value";

    public static final String COL_PROVIDER_GROUP = "provider_group";

    public static final String COL_WARRANTY = "warranty";

    public static final String COL_TWIN_NAME = "twin_name";

    public static final String COL_TWIN_ID = "twin_id";

    public static final String COL_LIFECYCLE = "lifecycle";

    public static final String COL_LIST_PRICE = "list_price";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_REAL_COST_PRICE = "real_cost_price";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_RESPONSIBLE_PERSON = "responsible_person";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_CHANGER = "changer";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHANGE_TAG = "change_tag";

    public static final String COL_PRICE_CHANGE_TAG = "price_change_tag";

    public static final String COL_FILE_CHANGE_AMOUNT = "file_change_amount";

    public static final String COL_DELETE_TAG = "delete_tag";

    public static final String COL_DESIGN_MODULE_TAG = "design_module_tag";

    public static final String COL_DESIGN_PROCEDURE_TAG = "design_procedure_tag";

    public static final String COL_DESIGN_CELL_TAG = "design_cell_tag";
}