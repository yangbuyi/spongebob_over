package top.yangbuyi.product.liuchengyang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.product.liuchengyang.service.DropDownBoxService_liuchengyang;

/**
 * @ClassName: DropDownBoxController
 * @Author: campsis-tk
 * @Date: 2020/7/16
 * @Description: DropDownBox
 **/
@RestController
@RequestMapping("/api/liuchengyang/dropdownbox")
public class DropDownBoxController_liuchengyang {

	@Autowired
	private DropDownBoxService_liuchengyang dropDownBoxServiceLiuchengyang;


	/**
	 * @Description: 审核下拉框查询
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	@GetMapping("queryAllCheckTag")
	public Object queryAllCheckTag(){
		return this.dropDownBoxServiceLiuchengyang.queryAllCheckTag();
	}

	/**
	 * @Description: 用途类型下拉框查询
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	@GetMapping("queryAllType")
	public Object queryAllType(){
		return this.dropDownBoxServiceLiuchengyang.queryAllType();
	}

	/**
	 * @Description: 档次下拉框查询
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	@GetMapping("queryAllLevel")
	public Object queryAllLevel(){
		return this.dropDownBoxServiceLiuchengyang.queryAllLevel();
	}

}
