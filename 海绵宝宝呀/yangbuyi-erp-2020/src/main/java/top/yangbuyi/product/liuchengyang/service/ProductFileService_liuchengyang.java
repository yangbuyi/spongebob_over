package top.yangbuyi.product.liuchengyang.service;

import top.yangbuyi.product.liuchengyang.vo.ProductFileVo;
import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

/**
 * @ClassName: ProductFileService_liuchengyang
 * @Author: campsis-tk
 * @Date: 2020/7/15 21:53
 * @Description:
 **/
public interface ProductFileService_liuchengyang extends IService<ProductFile> {

	/**
	 * @Description: 查询产品档案登记
	 * @param productFileVo
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	DataGridView queryAllProductFileDelete(ProductFileVo productFileVo);

	/**
	 * @Description: 保存产品档案记录
	 * @param productFile
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	ProductFile saveProductFile(ProductFile productFile);

	/**
	 * @Description: 修改产品档案记录
	 * @param productFile
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	ProductFile updateProductFile(ProductFile productFile);

	/**
	 * @Description: 状态删除产品档案记录
	 * @param id
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	ProductFile updateDeleteProductFile(Integer id);

	/**
	 * @Description: 审核通过产品档案记录
	 * @param ids
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	ProductFile updateCheckYesProductFile(Integer[] ids);

	/**
	 * @Description: 审核不通过产品档案记录
	 * @param ids
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	ProductFile updateCheckNoProductFile(Integer[] ids);

	/**
	 * @Description: 删除状态恢复产品档案记录
	 * @param ids
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	ProductFile updateDeleteBackProductFile(Integer[] ids);

	/**
	 * @Description: 永久批量删除产品档案记录
	 * @param ids
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	void batchDeleteProductFile(Integer[] ids);

	/**
	 * @Description: 查询产品档案登记(审核状态为S001-1)
	 * @param productFileVo
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	DataGridView queryAllProductFileByCheckTag(ProductFileVo productFileVo);

	/**
	 * @Description: 查询产品档案登记(审核状态为S001-2)
	 * @param productFileVo
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	DataGridView queryAllProductFileByCheckTagS2(ProductFileVo productFileVo);

	/**
	 * @Description: 查询产品档案登记(删除状态为C001-1)
	 * @param productFileVo
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	DataGridView queryAllProductFileByDeleteTag(ProductFileVo productFileVo);

	/**
	 * @Description: 查询产品档案登记(类型为商品的数据)
	 * @param productFileVo
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	DataGridView queryAllProductFileByGoods(ProductFileVo productFileVo);

	/**
	 * @param productFileVo
	 * @Description: 查询产品档案登记(类型为物料的数据)
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	DataGridView queryAllProductFileByMaterial(ProductFileVo productFileVo);

	/**
	 * @param productFile
	 * @Description: 添加产品和物料的关系
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	ProductFile addProductFileAll(List<ProductFile> productFile);


	/**
	 * @param id
	 * @Description: 删除产品和物料的关系
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	void deleteProductAll(Integer id);

	/**
	 * @param productFile
	 * @Description: 修改产品和物料的关系
	 * @return: ProductFile
	 * @Author: campsis-tk
	 * @Date: 2020/7/17
	 */
	ProductFile updProductFileAll(List<ProductFile> productFile);

	/**
	 * @param kindName
	 * @Description: 根据分类查询该类别下的物料
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/15
	 */
	DataGridView queryAllProductFileByMaterialByClass(String kindName);
}
