package top.yangbuyi.product.liuchengyang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.product.liuchengyang.service.ProductKindService_liuchengyang;

/**
 * @ClassName: ProductKindController
 * @Author: campsis-tk
 * @Date: 2020/7/16 14:35
 * @Description:
 **/
@RestController
@RequestMapping("/api/liuchengyang/productkind")
public class ProductKindController_liuchengyang {

	@Autowired
	private ProductKindService_liuchengyang productKindServiceLiuchengyang;

	/**
	 * @Description: 查询出一级分类
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	@GetMapping("queryFirstProductKind")
	public Object queryFirstProductKind() {
		return this.productKindServiceLiuchengyang.queryFirstProductKind();
	}

	/**
	 * @Description: 查询出其他分类
	 * @param id
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	@GetMapping("queryProductKind")
	public Object queryProductKind(Integer id) {
		return this.productKindServiceLiuchengyang.queryProductKind(id);
	}

}
