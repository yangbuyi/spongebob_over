package top.yangbuyi.product.liuchengyang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.product.liuchengyang.domain.ProductKind;

/**
  * @ClassName: ProductKindMapper_liuchengyang
  * @Author: campsis-tk
  * @Date: 2020/7/16 14:32
  * @Description:
  **/
public interface ProductKindMapper_liuchengyang extends BaseMapper<ProductKind> {
}