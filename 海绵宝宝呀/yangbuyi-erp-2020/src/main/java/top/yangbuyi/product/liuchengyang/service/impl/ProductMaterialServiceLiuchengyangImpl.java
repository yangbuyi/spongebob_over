package top.yangbuyi.product.liuchengyang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterial;
import top.yangbuyi.product.liuchengyang.mapper.ProductFileMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.mapper.ProductMaterialMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.ProductMaterialService_liuchengyang;
import top.yangbuyi.product.liuchengyang.vo.ProductMaterialVo;
import top.yangbuyi.system.common.ActiveUser;
import top.yangbuyi.system.common.Constant;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.StringUtils;

import java.util.Date;
import java.util.Random;

/**
 * @ClassName: ProductMaterialServiceLiuchengyangImpl
 * @Author: campsis-tk
 * @Date: 2020/7/19 12:29
 * @Description:
 **/
@Slf4j
@Service
public class ProductMaterialServiceLiuchengyangImpl extends ServiceImpl<ProductMaterialMapper_liuchengyang, ProductMaterial> implements ProductMaterialService_liuchengyang {

	@Autowired
	private ProductMaterialMapper_liuchengyang productMaterialMapperLiuchengyang;

	@Autowired
	private ProductFileMapper_liuchengyang productFileMapperLiuchengyang;

	/**
	 * @param productMaterialVo
	 * @Description: 查询商品表中未审核的
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@Override
	public DataGridView queryAllProductMaterialByCheck(ProductMaterialVo productMaterialVo) {
		IPage<ProductMaterial> page = new Page<>(productMaterialVo.getPage(), productMaterialVo.getLimit());
		QueryWrapper<ProductMaterial> wrapper = new QueryWrapper<>();
		wrapper.eq("check_tag", Constant.APPROVE_WAITE);//未审核的
		wrapper.like(StringUtils.isNotBlank(productMaterialVo.getProductName()), "product_name", productMaterialVo.getProductName());
		wrapper.ge(productMaterialVo.getStartTime() != null, "register_time", productMaterialVo.getStartTime());
		wrapper.le(productMaterialVo.getEndTime() != null, "register_time", productMaterialVo.getEndTime());
		wrapper.orderByDesc("register_time");
		this.productMaterialMapperLiuchengyang.selectPage(page, wrapper);
		return new DataGridView(page.getTotal(), page.getRecords());
	}

	/**
	 * @Description: 修改商品表的商品
	 * @param: productMaterial
	 * @return: productMaterial
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@Override
	public ProductMaterial updateProductMaterial(ProductMaterial productMaterial) {
		productMaterial.setCheckTag(Constant.APPROVE_WAITE);   //修改商品之后需要再次审核
		this.productMaterialMapperLiuchengyang.updateById(productMaterial);
		return productMaterial;
	}

	/**
	 * @Description: 审核通过产品档案记录
	 * @param: ids
	 * @return: ProductMaterial
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@Override
	public ProductMaterial updateCheckYesProductMaterial(Integer[] ids) {
		ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
		if (ids != null) {
			ProductMaterial productMaterial = null;
			for (Integer id : ids) {
				productMaterial = this.productMaterialMapperLiuchengyang.selectById(id);
				//修改审核状态  通过
				productMaterial.setCheckTag(Constant.APPROVE_YES);
				productMaterial.setChanger(activeUser.getUser().getName());    //设置审核人
				productMaterial.setCheckTime(new Date());           //设置审核时间
				this.productMaterialMapperLiuchengyang.updateById(productMaterial);
			}
			return productMaterial;
		} else {
			return null;
		}
	}

	/**
	 * @Description: 审核不通过产品档案记录
	 * @param: ids
	 * @return: ProductMaterial
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@Override
	public ProductMaterial updateCheckNoProductMaterial(Integer[] ids) {
		ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
		if (ids != null) {
			ProductMaterial productMaterial = null;
			for (Integer id : ids) {
				productMaterial = this.productMaterialMapperLiuchengyang.selectById(id);
				//修改审核状态  不通过
				productMaterial.setCheckTag(Constant.APPROVE_NO);
				productMaterial.setChanger(activeUser.getUser().getName());    //设置审核人
				productMaterial.setCheckTime(new Date());           //设置审核时间
				this.productMaterialMapperLiuchengyang.updateById(productMaterial);
			}
			return productMaterial;
		} else {
			return null;
		}
	}

	/**
	 * @param productMaterialVo
	 * @Description: 查询商品表
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	@Override
	public DataGridView queryAllProductMaterial(ProductMaterialVo productMaterialVo) {
		IPage<ProductMaterial> page = new Page<>(productMaterialVo.getPage(), productMaterialVo.getLimit());
		QueryWrapper<ProductMaterial> wrapper = new QueryWrapper<>();
		wrapper.eq("check_tag", Constant.APPROVE_WAITE).or().eq("check_tag", Constant.APPROVE_NO);
		wrapper.like(StringUtils.isNotBlank(productMaterialVo.getProductName()), "product_name", productMaterialVo.getProductName());
		wrapper.eq(StringUtils.isNotBlank(productMaterialVo.getCheckTag()), "check_tag", productMaterialVo.getCheckTag());
		wrapper.ge(productMaterialVo.getStartTime() != null, "register_time", productMaterialVo.getStartTime());
		wrapper.le(productMaterialVo.getEndTime() != null, "register_time", productMaterialVo.getEndTime());
		wrapper.orderByDesc("register_time");
		this.productMaterialMapperLiuchengyang.selectPage(page, wrapper);
		return new DataGridView(page.getTotal(), page.getRecords());
	}

	@Override
	public ProductMaterial addProductMaterial(ProductFile productFile) {
		ProductFile productFile1 = this.productFileMapperLiuchengyang.selectProductByProductId(productFile.getId());
		productFile1.setDesignModuleTag(Constant.DESGIN_YES);
		System.err.println(productFile1);
		this.productFileMapperLiuchengyang.updateById(productFile1);//修改productFile表的设计状态
		ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
		ProductMaterial productMaterial = new ProductMaterial();
		Random random = new Random();
		int number = random.nextInt(9999) + 1;
		productMaterial.setDesignId("20020200715" + number);
		productMaterial.setProductId(productFile.getProductId());//将产品ID设置到商品表中
		productMaterial.setProductName(productFile.getProductName());//将产品名字设置到商品表中
		productMaterial.setFirstKindId(productFile.getFirstKindId());//将一级ID设置到商品表中
		productMaterial.setFirstKindName(productFile.getFirstKindName());//将一级设置到商品表中
		productMaterial.setSecondKindId(productFile.getSecondKindId());//将二级ID设置到商品表中
		productMaterial.setSecondKindName(productFile.getSecondKindName());//将二级设置到商品表中
		productMaterial.setThirdKindId(productFile.getThirdKindId());//将三级ID设置到商品表中
		productMaterial.setThirdKindName(productFile.getThirdKindName());//将三级设置到商品表中
		productMaterial.setDesigner(activeUser.getUser().getName());//将设计人设置到商品表中
		productMaterial.setRegister(activeUser.getUser().getName());//将登记人设置到商品表中
		productMaterial.setModuleDescribe(productFile.getModuleDescribe());//将设计要求设置到商品表中
		productMaterial.setCostPriceSum(productFile.getAlltotal());//将总计设置到商品表中
		productMaterial.setRegisterTime(productFile.getRegisterTime());//将登记时间设置到商品表中
		productMaterial.setCheckTag(Constant.APPROVE_WAITE);//设置未审核
		productMaterial.setChangeTag(Constant.CHANGE_TAG_1);//更改状态
		// System.err.println("productMaterial:" + productMaterial);
		this.productMaterialMapperLiuchengyang.insert(productMaterial);
		return productMaterial;
	}

	@Override
	public ProductMaterial updProductMaterial(ProductFile productFile) {
		System.err.println("updProductMaterial" + productFile);
		ProductMaterial productMaterial = new ProductMaterial();
		ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
		productMaterial.setId(productFile.getId());
		productMaterial.setProductName(productFile.getProductName());//将产品名字设置到商品表中
		productMaterial.setFirstKindId(productFile.getFirstKindId());//将一级ID设置到商品表中
		productMaterial.setFirstKindName(productFile.getFirstKindName());//将一级设置到商品表中
		productMaterial.setSecondKindId(productFile.getSecondKindId());//将二级ID设置到商品表中
		productMaterial.setSecondKindName(productFile.getSecondKindName());//将二级设置到商品表中
		productMaterial.setThirdKindId(productFile.getThirdKindId());//将三级ID设置到商品表中
		productMaterial.setThirdKindName(productFile.getThirdKindName());//将三级设置到商品表中
		productMaterial.setDesigner(activeUser.getUser().getName());//将设计人设置到商品表中
		productMaterial.setRegister(activeUser.getUser().getName());//将登记人设置到商品表中
		productMaterial.setModuleDescribe(productFile.getModuleDescribe());//将设计要求设置到商品表中
		productMaterial.setCostPriceSum(productFile.getAlltotal());//将总计设置到商品表中
		productMaterial.setRegisterTime(productFile.getRegisterTime());//将登记时间设置到商品表中
		productMaterial.setChangeTag(Constant.CHANGE_TAG_2);//变更状态
		productMaterial.setCheckTag(Constant.APPROVE_WAITE);//重新审核
		productMaterial.setChangeTime(new Date());//设置变更时间
		productMaterial.setChanger(activeUser.getUser().getName());//设置变更人
		productMaterial.setChecker(null);
		productMaterial.setCheckTime(null);
		System.err.println("productMaterial:" + productMaterial);
		this.productMaterialMapperLiuchengyang.updateById(productMaterial);
		return productMaterial;
	}


}
