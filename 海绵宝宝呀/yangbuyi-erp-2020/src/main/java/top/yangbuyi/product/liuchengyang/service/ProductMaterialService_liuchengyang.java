package top.yangbuyi.product.liuchengyang.service;

import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterial;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.product.liuchengyang.vo.ProductMaterialVo;
import top.yangbuyi.system.common.DataGridView;

/**
 * @ClassName: ProductMaterialService_liuchengyang
 * @Author: campsis-tk
 * @Date: 2020/7/19 12:29
 * @Description:
 **/
public interface ProductMaterialService_liuchengyang extends IService<ProductMaterial> {

	/**
	 * @param productMaterialVo
	 * @Description: 查询商品表中未审核的
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	DataGridView queryAllProductMaterialByCheck(ProductMaterialVo productMaterialVo);

	/**
	 * @Description: 修改商品表的商品
	 * @param: productMaterial
	 * @return: productMaterial
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	ProductMaterial updateProductMaterial(ProductMaterial productMaterial);

	/**
	 * @Description: 审核通过产品档案记录
	 * @param: ids
	 * @return: ProductMaterial
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	ProductMaterial updateCheckYesProductMaterial(Integer[] ids);

	/**
	 * @Description: 审核不通过产品档案记录
	 * @param: ids
	 * @return: ProductMaterial
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	ProductMaterial updateCheckNoProductMaterial(Integer[] ids);

	/**
	 * @param productMaterialVo
	 * @Description: 查询商品表
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	DataGridView queryAllProductMaterial(ProductMaterialVo productMaterialVo);

	/**
	 * @Description: 添加商品
	 * @param productFile
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	ProductMaterial addProductMaterial(ProductFile productFile);

	/**
	 * @Description: 修改商品
	 * @param productFile
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	ProductMaterial updProductMaterial(ProductFile productFile);
}
