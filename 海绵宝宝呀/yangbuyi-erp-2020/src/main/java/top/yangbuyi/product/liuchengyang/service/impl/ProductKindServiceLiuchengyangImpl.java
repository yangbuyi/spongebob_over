package top.yangbuyi.product.liuchengyang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.product.liuchengyang.domain.ProductKind;
import top.yangbuyi.product.liuchengyang.mapper.ProductKindMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.service.ProductKindService_liuchengyang;
import top.yangbuyi.system.common.Constant;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

/**
 * @ClassName: ProductKindServiceLiuchengyangImpl
 * @Author: campsis-tk
 * @Date: 2020/7/16 14:32
 * @Description:
 **/
@Service
public class ProductKindServiceLiuchengyangImpl extends ServiceImpl<ProductKindMapper_liuchengyang, ProductKind> implements ProductKindService_liuchengyang {

	@Autowired
	private ProductKindMapper_liuchengyang productKindMapperLiuchengyang;

	@Override
	public DataGridView queryFirstProductKind() {
		QueryWrapper<ProductKind> wrapper = new QueryWrapper<>();
		wrapper.eq("p_id", Constant.PID);
		List<ProductKind> kinds = this.productKindMapperLiuchengyang.selectList(wrapper);
		return new DataGridView(kinds);
	}

	@Override
	public DataGridView queryProductKind(Integer id) {
		QueryWrapper<ProductKind> wrapper = new QueryWrapper<>();
		wrapper.eq(id != null, "p_id", id);
		List<ProductKind> kinds = this.productKindMapperLiuchengyang.selectList(wrapper);
		return new DataGridView(kinds);
	}

}
