package top.yangbuyi.product.liuchengyang.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
  * @ClassName: DropDownBox
  * @Author: campsis-tk
  * @Date: 2020/7/16 9:02
  * @Description:
  **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DropDownBox implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @TableField(value = "key_number")
    private String keyNumber;


    @TableField(value = "text_describe")
    private String textDescribe;


    @TableField(value = "drop_down_box_name")
    private String dropDownBoxName;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_KEY_NUMBER = "key_number";

    public static final String COL_TEXT_DESCRIBE = "text_describe";

    public static final String COL_DROP_DOWN_BOX_NAME = "drop_down_box_name";
}