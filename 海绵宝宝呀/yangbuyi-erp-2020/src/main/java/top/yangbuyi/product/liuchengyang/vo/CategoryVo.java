package top.yangbuyi.product.liuchengyang.vo;

import lombok.Data;

/**
 * @ClassName: BaseVo
 * @Author: campsis-tk
 * @Date: 2020/7/15 21:55
 * @Description: 类增强类 父类
 **/
@Data
public class CategoryVo {
	/**
	 * 一级分类ID
	 */
	private String firstKindId;

	/**
	 * 一级分类名称
	 */
	private String firstKindName;

	/**
	 * 二级分类ID
	 */
	private String secondKindId;

	/**
	 * 二级分类名称
	 */
	private String secondKindName;

	/**
	 * 三级分类ID
	 */
	private String thirdKindId;

	/**
	 * 三级分类名称
	 */
	private String thirdKindName;
}

