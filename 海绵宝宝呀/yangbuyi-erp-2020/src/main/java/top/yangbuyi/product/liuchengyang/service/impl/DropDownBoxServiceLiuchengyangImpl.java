package top.yangbuyi.product.liuchengyang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.product.liuchengyang.mapper.DropDownBoxMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.domain.DropDownBox;
import top.yangbuyi.product.liuchengyang.service.DropDownBoxService_liuchengyang;
import top.yangbuyi.system.common.Constant;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: DropDownBoxServiceLiuchengyangImpl
 * @Author: campsis-tk
 * @Date: 2020/7/16 9:02
 * @Description:
 **/
@Service
public class DropDownBoxServiceLiuchengyangImpl extends ServiceImpl<DropDownBoxMapper_liuchengyang, DropDownBox> implements DropDownBoxService_liuchengyang {

	@Autowired
	private DropDownBoxMapper_liuchengyang dropDownBoxMapper;

	@Override
	public DataGridView queryAllCheckTag() {
		QueryWrapper<DropDownBox> wrapper = new QueryWrapper<>();
		wrapper.eq("drop_down_box_name", Constant.CHECKTAG);
		List<DropDownBox> dropDownBoxes = this.dropDownBoxMapper.selectList(wrapper);
		return new DataGridView(dropDownBoxes);
	}

	@Override
	public DataGridView queryAllType() {
		QueryWrapper<DropDownBox> wrapper = new QueryWrapper<>();
		wrapper.eq("drop_down_box_name", Constant.TYPE);
		List<DropDownBox> dropDownBoxes = this.dropDownBoxMapper.selectList(wrapper);
		return new DataGridView(dropDownBoxes);
	}

	@Override
	public DataGridView queryAllLevel() {
		QueryWrapper<DropDownBox> wrapper = new QueryWrapper<>();
		wrapper.eq("drop_down_box_name", Constant.CLASS);
		List<DropDownBox> dropDownBoxes = this.dropDownBoxMapper.selectList(wrapper);
		return new DataGridView(dropDownBoxes);
	}

	@Override
	public DropDownBox queryTextDescribeByCheckTag(String checkTag) {
		QueryWrapper<DropDownBox> wrapper = new QueryWrapper<>();
		wrapper.eq(StringUtils.isNotBlank(checkTag), "key_number", checkTag);
		DropDownBox dropDownBox = this.dropDownBoxMapper.selectOne(wrapper);
		return dropDownBox;
	}

	@Override
	public DropDownBox queryByType(String type) {
		QueryWrapper<DropDownBox> wrapper = new QueryWrapper<>();
		wrapper.eq(StringUtils.isNotBlank(type), "key_number", type);
		DropDownBox dropDownBox = this.dropDownBoxMapper.selectOne(wrapper);
		return dropDownBox;
	}

	@Override
	public DropDownBox queryByLevel(String productClass) {
		System.err.println(productClass);
		QueryWrapper<DropDownBox> wrapper = new QueryWrapper<>();
		wrapper.eq(StringUtils.isNotBlank(productClass), "key_number", productClass);
		DropDownBox dropDownBox = this.dropDownBoxMapper.selectOne(wrapper);
		return dropDownBox;
	}

	@Override
	public DropDownBox getById(Serializable id) {
		return super.getById(id);
	}
}
