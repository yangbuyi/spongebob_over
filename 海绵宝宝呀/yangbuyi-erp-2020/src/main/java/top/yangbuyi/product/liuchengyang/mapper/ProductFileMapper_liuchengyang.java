package top.yangbuyi.product.liuchengyang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.product.liuchengyang.domain.ProductFile;

/**
  * @ClassName: ProductFileMapper_liuchengyang
  * @Author: campsis-tk
  * @Date: 2020/7/15 21:53
  * @Description:
  **/
public interface ProductFileMapper_liuchengyang extends BaseMapper<ProductFile> {
	ProductFile selectProductByProductId(Integer id);

	ProductFile selectByProductId(String productId);
}