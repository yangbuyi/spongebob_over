package top.yangbuyi.product.liuchengyang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import top.yangbuyi.product.liuchengyang.mapper.ProductMaterialDetailsMapper_liuchengyang;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterialDetails;
import top.yangbuyi.product.liuchengyang.service.ProductMaterialDetailsService_liuchengyang;
import top.yangbuyi.system.common.Constant;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

/**
 * @ClassName: ProductMaterialDetailsServiceLiuchengyangImpl
 * @Author: campsis-tk
 * @Date: 2020/7/17 19:30
 * @Description:
 **/
@Service
public class ProductMaterialDetailsServiceLiuchengyangImpl extends ServiceImpl<ProductMaterialDetailsMapper_liuchengyang, ProductMaterialDetails> implements ProductMaterialDetailsService_liuchengyang {

	@Autowired
	private ProductMaterialDetailsMapper_liuchengyang productMaterialDetailsMapperLiuchengyang;

	@Override
	public DataGridView getProductMaterialDetailsBy(Integer parentId) {
		QueryWrapper<ProductMaterialDetails> wrapper = new QueryWrapper<>();
		wrapper.eq(parentId != null, "parent_id", parentId);
		List<ProductMaterialDetails> productMaterialDetails = this.productMaterialDetailsMapperLiuchengyang.selectList(wrapper);
		return new DataGridView(productMaterialDetails);
	}

	@Override
	public void addProductMaterialDetails(List<ProductFile> productFile, Integer id) {
		for (int i = 0; i < productFile.size(); i++) {
			if (productFile.get(i).getType().equals(Constant.TYPE_Y001_2)) {
				ProductMaterialDetails productMaterialDetails = new ProductMaterialDetails();
				productMaterialDetails.setParentId(id);
				productMaterialDetails.setResidualAmount(productFile.get(i).getAmount());//将物料可用数量添加进物料组成表
				productMaterialDetails.setProductId(productFile.get(i).getProductId());//将物料ID添加进物料组成表
				productMaterialDetails.setProductName(productFile.get(i).getProductName());//将物料名称添加进物料组成表
				productMaterialDetails.setType(productFile.get(i).getType());//将物料类型添加进物料组成表
				productMaterialDetails.setProductDescribe(productFile.get(i).getProductDescribe());//将物料简介添加进物料组成表
				productMaterialDetails.setAmountUnit(productFile.get(i).getAmountUnit());//将物料单位添加进物料组成表
				productMaterialDetails.setCostPrice(productFile.get(i).getCostPrice());//将物料单价添加进物料组成表
				productMaterialDetails.setSubtotal(productFile.get(i).getSubtotal());//将物料小计添加进物料组成表
				productMaterialDetails.setProductDescribe(productFile.get(i).getDescribe());//
				productMaterialDetails.setDetailsNumber(i + 1);
				productMaterialDetails.setAmount(productFile.get(i).getAmount());//将物料数量添加进物料组成表
				this.productMaterialDetailsMapperLiuchengyang.insert(productMaterialDetails);
			}
		}
	}

	@Override
	public void deleteByProductMaterialId(Integer id) {
		this.productMaterialDetailsMapperLiuchengyang.deleteByProductMaterialId(id);
	}

	@Override
	public void updProductMaterialDetails(List<ProductFile> productFile) {
		for (int i = 0; i < productFile.size(); i++) {
			if (productFile.get(i).getType()!=null) {
				ProductMaterialDetails productMaterialDetails = new ProductMaterialDetails();
				productMaterialDetails.setId(productFile.get(i).getId());
				productMaterialDetails.setProductId(productFile.get(i).getProductId());//将物料ID添加进物料组成表
				productMaterialDetails.setProductName(productFile.get(i).getProductName());//将物料名称添加进物料组成表
				productMaterialDetails.setType(productFile.get(i).getType());//将物料类型添加进物料组成表
				productMaterialDetails.setProductDescribe(productFile.get(i).getProductDescribe());//将物料简介添加进物料组成表
				productMaterialDetails.setAmountUnit(productFile.get(i).getAmountUnit());//将物料单位添加进物料组成表
				productMaterialDetails.setCostPrice(productFile.get(i).getCostPrice());//将物料单价添加进物料组成表
				productMaterialDetails.setSubtotal(productFile.get(i).getAlltotal());//将物料小计添加进物料组成表
				productMaterialDetails.setProductDescribe(productFile.get(i).getProductDescribe());//
				productMaterialDetails.setDetailsNumber(i + 1);
				productMaterialDetails.setAmount(productFile.get(i).getAmount());//将物料数量添加进物料组成表
				productMaterialDetails.setResidualAmount(null);//将物料可用数量添加进物料组成表
				this.productMaterialDetailsMapperLiuchengyang.updateById(productMaterialDetails);
			}
		}
	}
}
