package top.yangbuyi.product.liuchengyang.vo;

import lombok.Data;

/**
 * @ClassName: BaseVo
 * @Author: campsis-tk
 * @Date: 2020/7/15 21:55
 * @Description: 类增强类 父类
 **/
@Data
public class BaseVo {
	private Integer page;
	private Integer limit;
	private Integer available;
}

