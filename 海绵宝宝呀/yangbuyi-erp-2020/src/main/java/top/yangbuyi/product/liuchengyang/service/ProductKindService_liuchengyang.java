package top.yangbuyi.product.liuchengyang.service;

import top.yangbuyi.product.liuchengyang.domain.ProductKind;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.common.DataGridView;

/**
 * @ClassName: ProductKindService_liuchengyang
 * @Author: campsis-tk
 * @Date: 2020/7/16 14:32
 * @Description:
 **/
public interface ProductKindService_liuchengyang extends IService<ProductKind> {

	/**
	 * @Description: 查询出一级分类
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DataGridView queryFirstProductKind();

	/**
	 * @Description: 查询出其他分类
	 * @param id
	 * @return: DataGridView
	 * @Author: campsis-tk
	 * @Date: 2020/7/16
	 */
	DataGridView queryProductKind(Integer id);

}
