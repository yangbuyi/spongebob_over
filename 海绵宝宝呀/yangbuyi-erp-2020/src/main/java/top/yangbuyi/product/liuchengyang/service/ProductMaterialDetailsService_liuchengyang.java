package top.yangbuyi.product.liuchengyang.service;

import top.yangbuyi.product.liuchengyang.domain.ProductFile;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterialDetails;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

/**
 * @ClassName: ProductMaterialDetailsService_liuchengyang
 * @Author: campsis-tk
 * @Date: 2020/7/17 19:30
 * @Description:
 **/
public interface ProductMaterialDetailsService_liuchengyang extends IService<ProductMaterialDetails> {

	/**
	 * @Description: 根据产品id
	 * @param parentId
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	DataGridView getProductMaterialDetailsBy(Integer parentId);

	/**
	 * @Description: 添加物料
	 * @param productFile
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	void addProductMaterialDetails(List<ProductFile> productFile, Integer id);

	/**
	 * @Description: 根据商品id删除相关联的物料
	 * @param id
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	void deleteByProductMaterialId(Integer id);

	/**
	 * @Description: 修改物料
	 * @param productFile
	 * @return: java.lang.Object
	 * @Author: campsis-tk
	 * @Date: 2020/7/19
	 */
	void updProductMaterialDetails(List<ProductFile> productFile);
}
