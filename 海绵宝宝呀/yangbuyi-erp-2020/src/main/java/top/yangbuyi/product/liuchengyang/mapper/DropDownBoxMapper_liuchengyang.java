package top.yangbuyi.product.liuchengyang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.product.liuchengyang.domain.DropDownBox;

/**
  * @ClassName: DropDownBoxMapper_liuchengyang
  * @Author: campsis-tk
  * @Date: 2020/7/16 9:02
  * @Description:
  **/
public interface DropDownBoxMapper_liuchengyang extends BaseMapper<DropDownBox> {
}