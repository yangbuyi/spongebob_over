package top.yangbuyi.product.liuchengyang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.product.liuchengyang.domain.ProductMaterialDetails;

/**
  * @ClassName: ProductMaterialDetailsMapper_liuchengyang
  * @Author: campsis-tk
  * @Date: 2020/7/17 19:30
  * @Description:
  **/
public interface ProductMaterialDetailsMapper_liuchengyang extends BaseMapper<ProductMaterialDetails> {

	void deleteByProductMaterialId(Integer id);
}