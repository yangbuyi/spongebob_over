package top.yangbuyi.product.liuchengyang.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
  * @ClassName: ProductKind
  * @Author: campsis-tk
  * @Date: 2020/7/16 14:32
  * @Description:
  **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductKind implements Serializable {

	/**
	 * 序号
	 * */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

	/**
	 * 父级序号
	 * */
    @TableField(value = "p_id")
    private Integer pId;

	/**
	 * 分类编号
	 * */
    @TableField(value = "kind_id")
    private String kindId;

	/**
	 * 分类名称
	 * */
    @TableField(value = "kind_name")
    private String kindName;

	/**
	 * 级别
	 * */
    @TableField(value = "kind_level")
    private Integer kindLevel;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_P_ID = "p_id";

    public static final String COL_KIND_ID = "kind_id";

    public static final String COL_KIND_NAME = "kind_name";

    public static final String COL_KIND_LEVEL = "kind_level";
}