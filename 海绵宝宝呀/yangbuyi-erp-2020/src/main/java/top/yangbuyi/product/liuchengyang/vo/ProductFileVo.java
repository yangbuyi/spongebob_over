package top.yangbuyi.product.liuchengyang.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName: ProductFileVo
 * @Author: campsis-tk
 * @Date: 2020/7/15 21:55
 * @Description: 产品档案登记增强类
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductFileVo extends BaseVo {

	/**
	 * 产品名称
	 */
	private String productName;

	/**
	 * 审核状态
	 */
	private String checkTag;


	/**
	 * 结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	/**
	 * 开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
}
