package top.yangbuyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.system.domain.Menu;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/18
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface MenuMapper extends BaseMapper<Menu> {
	  Integer queryMenuMaxOrderNum();
	  
	  Integer queryMenuChildrenCountById(Integer id);
}