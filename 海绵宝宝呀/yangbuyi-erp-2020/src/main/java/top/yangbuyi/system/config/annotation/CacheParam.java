package top.yangbuyi.system.config.annotation;

import java.lang.annotation.*;

/**
 * @program: resubmit
 * @description: 锁的参数 key 值 注解
 * @author: Dai Yuanchuan
 * @create: 2019-08-31 03:56
 **/
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CacheParam {

    /**
     * 字段名称
     *
     * @return String
     */
    String name() default "";
}