package top.yangbuyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.system.domain.User;

/**
* ClassName: UserMapper
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/4/14 19:50
* @author TeouBle
* @author yangbuyi
* @version
* @since JDK 1.8
*/
public interface UserMapper extends BaseMapper<User> {
	  Integer queryUserMaxOrderNum();
   
}