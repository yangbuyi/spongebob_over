package top.yangbuyi.system.webapi;

import org.lionsoul.ip2region.DataBlock;
import org.lionsoul.ip2region.DbConfig;
import org.lionsoul.ip2region.DbSearcher;
import org.lionsoul.ip2region.Util;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLDecoder;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  iputils
 * create:  2020-07-10 09:20
 *
 * @author: yangbuyi
 * @since： JDK1.8
 * @iputils:
 **/

/**
 * 具体请看 SQL当中的 data文件  和jar包
 * 通过下面语句进行 编译 地理位置
 * java -jar dbMaker-1.2.2.jar -src ./data/ip.merge.txt -region ./data/global_region.csv
 */

public class iputils {
      
      
      public static String getCityInfo(String ip) throws IOException {
            //db
            
            File file1 = ResourceUtils.getFile("classpath:static/data/ip2region.db");
//             ClassPathResource classPathResource = new ClassPathResource("static/data/ip2region.db");
            String dbPath = file1.getPath();

//            ClassPathResource classPathResource = new ClassPathResource("static/data/ip2region.db");
//            InputStream dbPath = classPathResource.getInputStream();
            //   String dbPath = new File("SQL/data/ip2region.db").getPath();
            dbPath = URLDecoder.decode(dbPath, "UTF-8");
            File file = new File(dbPath);
            if (file.exists() == false) {
                  System.out.println("Error: Invalid ip2region.db file");
            }
            //查询算法
            int algorithm = DbSearcher.BTREE_ALGORITHM; //B-tree
            //DbSearcher.BINARY_ALGORITHM //Binary
            //DbSearcher.MEMORY_ALGORITYM //Memory
            try {
                  DbConfig config = new DbConfig();
                  DbSearcher searcher = new DbSearcher(config, dbPath);
                  //define the method
                  Method method = null;
                  switch (algorithm) {
                        case DbSearcher.BTREE_ALGORITHM:
                              method = searcher.getClass().getMethod("btreeSearch", String.class);
                              break;
                        case DbSearcher.BINARY_ALGORITHM:
                              method = searcher.getClass().getMethod("binarySearch", String.class);
                              break;
                        case DbSearcher.MEMORY_ALGORITYM:
                              method = searcher.getClass().getMethod("memorySearch", String.class);
                              break;
                  }
                  DataBlock dataBlock = null;
                  if (Util.isIpAddress(ip) == false) {
                        System.out.println("Error: Invalid ip address");
                  }
                  dataBlock = (DataBlock) method.invoke(searcher, ip);
                  String decode = URLDecoder.decode(dataBlock.getRegion(), "UTF-8");
                  return decode;
            } catch (Exception e) {
                  e.printStackTrace();
            }
            return null;
      }
      
      public static void main(String[] args) throws Exception {
            System.err.println(iputils.getCityInfo("106.18.167.102"));
      }
      
}
