package top.yangbuyi.system.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.yangbuyi.system.common.ActiveUser;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.upload.UploadService;
import top.yangbuyi.system.domain.User;
import top.yangbuyi.system.service.UserService;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: FileController
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/16 20:18
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 文件上传 管理
 */

@RestController
@RequestMapping("api/file")
public class FileController {
      @Autowired
      private UploadService uploadService;
      
      @Autowired
      private UserService userService;
      
      /**
       * 上传文件  用户  储存到redis的
       */
      @RequestMapping("uploadFile")
      public Object uploadFile(MultipartFile mf) {
            // 调用上传到FastDFS
            String path = this.uploadService.uploadImage(mf);
            Map<String, String> map = new HashMap<>();
            map.put("src", path);
            //更新数据库
            ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
            User user = activeUser.getUser();
            user.setImgpath(path);
            System.out.println(user);
            // 更新用户
            userService.updateUser(user);
            return new DataGridView(map);
      }
      
      
      /**
       * 上传文件  任意文件
       */
      @RequestMapping("uploadGoodsFile")
      @ResponseBody
      public Object uploadGoodsFile(MultipartFile mf) {
            String path = this.uploadService.uploadImage(mf);
            
            Map<String, String> map = new HashMap<>();
            map.put("src", path);
            return new DataGridView(map);
      }
      
      
      @RequestMapping("uploadFileWang")
      @ResponseBody
      public Map<String, String> uploadFileWang(MultipartFile mf) {
            String path = this.uploadService.uploadImage(mf);
            Map<String, String> map = new HashMap<>();
            map.put("src", path);
            return map;
      }
      
}