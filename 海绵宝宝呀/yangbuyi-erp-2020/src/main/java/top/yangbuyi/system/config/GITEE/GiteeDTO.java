package top.yangbuyi.system.config.GITEE;

import lombok.Data;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  GiteeDTO
 * create:  2020-07-02 10:55
 *
 * @author: yangbuyi
 * @since： JDK1.8
 
 
 基于GIT返回的参数拿到 对应的数据
 **/

@Data
public class GiteeDTO {

      private Integer id;
      private String login;
      private String name;
      private String avatar_url;
      private String url;
      private String html_url;
      private String followers_url;
      
      /*
      *
      * "id": 5151444,
     "login": "yangbuyi",
     "name": "杨不易呀",
     "avatar_url": "https://portrait.gitee.com/uploads/avatars/user/1717/5151444_yangbuyi_1591280581.png",
     "url": "https://gitee.com/api/v5/users/yangbuyi",
     "html_url": "https://gitee.com/yangbuyi",
     "followers_url": "https://gitee.com/api/v5/users/yangbuyi/followers",
     "following_url": "https://gitee.com/api/v5/users/yangbuyi/following_url{/other_user}",
     "gists_url": "https://gitee.com/api/v5/users/yangbuyi/gists{/gist_id}",
     "starred_url": "https://gitee.com/api/v5/users/yangbuyi/starred{/owner}{/repo}",
     "subscriptions_url": "https://gitee.com/api/v5/users/yangbuyi/subscriptions",
     "organizations_url": "https://gitee.com/api/v5/users/yangbuyi/orgs",
     "repos_url": "https://gitee.com/api/v5/users/yangbuyi/repos",
     "events_url": "https://gitee.com/api/v5/users/yangbuyi/events{/privacy}",
     "received_events_url": "https://gitee.com/api/v5/users/yangbuyi/received_events",
     "type": "User",
     "site_admin": false,
     "blog": "www.yangbuyi.top/",
     "weibo": null,
     "bio": "自我介绍呀",
     "public_repos": 3,
     "public_gists": 0,
     "followers": 4,
     "following": 3,
     "stared": 3,
     "watched": 4,
     "created_at": "2019-07-15T22:56:27+08:00",
     "updated_at": "2020-07-02T10:44:14+08:00",
     "email": null
}
      * */



}
