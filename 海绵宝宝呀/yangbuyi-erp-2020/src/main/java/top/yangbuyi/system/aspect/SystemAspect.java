package top.yangbuyi.system.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import top.yangbuyi.system.common.ClientIPUtils;
import top.yangbuyi.system.common.WebUtils;
import top.yangbuyi.system.domain.Systemlog;
import top.yangbuyi.system.domain.User;
import top.yangbuyi.system.mapper.SystemlogMapper;
import top.yangbuyi.system.webapi.BaiduMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;


/**
 * ClassName: SystemAspect
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 18:05
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * @deprecated 切面 注入操作日志信息
 */
@Aspect
@Component
@EnableAspectJAutoProxy
@Slf4j
public class SystemAspect {
      
      @Autowired
      private SystemlogMapper systemlogMapper;
      
      @Autowired
      private RedisTemplate redisTemplate;
      
      /**
       * 日志信息输入
       */
      /* JoinPoint 连接点获取方法  日志执行之前的方法  */
      public static final String POINTCUT_ADD = "execution(* top.yangbuyi.system.service.*.*(..))";
      
      
      /**
       * 定义切入点
       */
      @Pointcut("execution(public * top.yangbuyi.system.controller.*.*(..))")
      public void webPointCut() {
      }
      
      @Pointcut("execution(public * top.yangbuyi.business.controller.*.*(..))")
      public void webPointCut2() {
      }
      
      
      /**
       * @param joinpoint
       * @throws Throwable
       */
//      @After("webPointCut() || webPointCut2()")
      @After("webPointCut() || webPointCut2()")
      public void writeLog(JoinPoint joinpoint) throws Throwable {
            log.info("开始插入操作日志");
            // 设置时间
            Systemlog systemlog = new Systemlog();
            systemlog.setOptime(new Date());
            // 设置ip地址
            String addressIp = null;
            HttpServletRequest httpServletRequest = WebUtils.getHttpServletRequest();
            String clientIp = ClientIPUtils.getClientIp(httpServletRequest);
            if (clientIp != null) {
                  // 获取自定义拦截器请求
                  systemlog.setIp(clientIp);
                  addressIp = clientIp;
            }
            // 设置当前执行的方法
            // 获取目标执行方法的全路径
            String name = joinpoint.getTarget().getClass().getName();
            // 获取方法名称
            String signature = joinpoint.getSignature().getName();
            String func = name + ":" + signature;
            // 设置当前用户执行了哪些方法
            systemlog.setFunction(func);
            // 获取方法参数
            systemlog.setParams(Arrays.toString(joinpoint.getArgs()));
            // 设置当前用户
            User thisName = (User) WebUtils.getThisName();
            if (null != thisName) {
                  systemlog.setThisname(thisName.getName());
            } else {
                  systemlog.setThisname("未知用户");
            }

//        // 设置ip地址
            BaiduMap baiduMap = new BaiduMap();
            baiduMap.setAddress(addressIp);
            // 获取操作地址 本地
//            String cityInfo = iputils.getCityInfo(clientIp);
//        // response  堵塞
//        try {
//              baiduMap = IP.getWebapiAddress(addressIp, "nMBfefpMVsdP6BUVn350G1ByUmOu07GK");
//              log.info("获取百度接口地址成功:{}" + baiduMap);
//        } catch (IOException | NoSuchFieldException e) {
//            e.printStackTrace();
//            log.info("获取百度接口地址失败");
//        }
//        assert baiduMap != null;
//            systemlog.setAddress(baiduMap.getAddress());
            systemlog.setAddress("百度接口异常||杨不易");
            // 注入到数据库
            systemlogMapper.insert(systemlog);
            
            // 进行请求接口异常操作
//            Integer ipCount = (Integer) redisTemplate.opsForValue().get(clientIp);
//            if (ipCount == null) {
//                  // +1 设置过期时间五秒
//                  redisTemplate.opsForValue().set(clientIp, 1, 30, TimeUnit.SECONDS);
//            } else if (ipCount == 5) {
//                  // 五秒接口次数请求到大 100 次
//                  log.info("接口请求频繁，请五秒后在操作");
//              /*    HttpServletResponse response = WebUtils.getHttpServletResponse();
//                  response.setCharacterEncoding("UTF-8");
//                  response.setContentType("application/json");
//                  Map<String,Object> resultData = new HashMap<>();
//                  resultData.put("code", -1);
//                  resultData.put("msg", "接口请求频繁，请五秒后在操作");
//                  PrintWriter out= response.getWriter();
//                  //使用fastjson的对象转化方法
//                  out.write(JSONObject.toJSON(resultData).toString());*/
////                  return new ResultObj(-1, "");
//            } else {
//                  // 五秒接口次数小于一百
//                  // +1
//                  redisTemplate.opsForValue().increment(clientIp, 1);
//            }
            log.info("结束切面插入操作日志");
      }
}