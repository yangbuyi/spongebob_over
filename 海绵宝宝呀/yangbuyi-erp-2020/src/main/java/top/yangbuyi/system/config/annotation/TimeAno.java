package top.yangbuyi.system.config.annotation;

import java.lang.annotation.*;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  TimeAno
 * create:  2020-07-10 23:31
 *
 * @author: yangbuyi
 * @since： JDK1.8
 * @TimeAno:
 **/

/**、
 * 这是注解源码 default 默认 value = 3个小时
 */

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public  @interface TimeAno  {
      String value() default "10800000";
}