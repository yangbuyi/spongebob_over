package top.yangbuyi.system.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import top.yangbuyi.system.config.interceptor.FangshuaInterceptor;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  WebConfigFangShuaInterceotor
 * create:  2020-07-11 17:06
 *
 * @author: yangbuyi
 * @since： JDK1.8
 * @WebConfigFangShuaInterceotor:  这里采用了注解方式（拦截器），
 * 结合redis来存储请求次数，达到上限就不让用户操作。
 * 当然，redis有时间限制，到了时间用户可以再次请求接口的。
 **/

@Configuration
public class WebConfigFangShuaInterceotor extends WebMvcConfigurerAdapter {
      
      @Autowired
      private FangshuaInterceptor interceptor;
      
      @Override
      public void addInterceptors(InterceptorRegistry registry) {
            registry.addInterceptor(interceptor);
      }
}
