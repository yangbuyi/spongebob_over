package top.yangbuyi.system.webapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Lazy;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: BaiduMap
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 17:01
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 组装百度地图 接收JSON
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaiduMap {
      
      @Lazy
      private String address;
      @Lazy
      private Map<String, String> content = new HashMap<>();
      @Lazy
      private Integer status;

/*

    public static class mapContent {
        private String address;
        private Map<String, String> address_detail;
        private Map<String, String> point;
    }
*/

}