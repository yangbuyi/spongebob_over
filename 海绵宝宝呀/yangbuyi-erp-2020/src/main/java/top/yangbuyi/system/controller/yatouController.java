package top.yangbuyi.system.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.system.common.ResultObj;

/**
 * @description: 杨不易网站:www.yangbuyi.top
 * @program: yangbuyi-erp-2020
 * @ClassName: yatouController
 * @create: 2020-07-12 21:03
 * @author: yangbuyi
 * @since： JDK1.8
 * @yatouController:
 **/

@RestController
@Slf4j
@RequestMapping("yatouController")
public class yatouController {
      
      @RequestMapping("yatou")
      public ResultObj yatou(String username) {
            
            
            return new ResultObj(200, "刘志和"+ username);
      }
}
