package top.yangbuyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.system.domain.Dept;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/4/17
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
*/
public interface DeptMapper extends BaseMapper<Dept> {
    Integer queryDeptMaxOrderNum();
	  
	  Integer queryDeptChildrenCountById(Integer id);
}