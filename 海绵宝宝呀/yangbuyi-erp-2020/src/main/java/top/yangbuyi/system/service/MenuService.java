package top.yangbuyi.system.service;

import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.domain.Menu;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.vo.MenuVo;

import java.util.List;

/**
 * ClassName: MenuService
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/14 18:39
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
public interface MenuService extends IService<Menu> {
	  /**
	   * 全查询菜单
	   *
	   * @return
	   */
	  List<Menu> queryAllMenuForList();
	  
	  /**
	   * 根据用户ID查询菜单
	   *
	   * @param id
	   * @return
	   */
	  List<Menu> queryMenuForListByUserId(Integer id);
	  
	  DataGridView queryAllMenu(MenuVo menuVo);
	  
	  Integer queryMenuMaxOrderNum();
	  
	  Menu saveMenu(Menu menu);
	  
	  Menu updateMenu(Menu menu);
	  
	  Integer queryMenuChildrenCountById(Integer id);
	  
	  List<String> queryPermissionCodesByUserId(Integer id);
	  
}

