package top.yangbuyi.system.service;

import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.domain.Notice;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.vo.NoticeVo;

/**
 * ClassName: NoticeService
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/16 15:51
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
public interface NoticeService extends IService<Notice> {

    /**
     * 获取公共 带高级查询
     *
     * @param noticeVo
     * @return
     */
    DataGridView queryAllNotice(NoticeVo noticeVo);

}
