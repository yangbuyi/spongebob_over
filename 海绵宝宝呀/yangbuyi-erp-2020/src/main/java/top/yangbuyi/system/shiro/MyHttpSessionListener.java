package top.yangbuyi.system.shiro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  MyHttpSessionListener
 * create:  2020-07-08 15:16
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/

/**
 * 使用 HttpSessionListener 统计在线用户数的监听器
 * @author shengwu ni
 * @date 2018/07/05
 */
@Component
public class MyHttpSessionListener implements HttpSessionListener {
      
      private static final Logger logger = LoggerFactory.getLogger(MyHttpSessionListener.class);
      
      /**
       * 记录在线的用户数量
       */
      public Integer count = 0;
      
      @Override
      public synchronized void sessionCreated(HttpSessionEvent httpSessionEvent) {
            logger.info("新用户上线了");
            count++;
            httpSessionEvent.getSession().getServletContext().setAttribute("count", count);
      }
      
      @Override
      public synchronized void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
            logger.info("用户下线了");
            count--;
            httpSessionEvent.getSession().getServletContext().setAttribute("count", count);
      }
}
