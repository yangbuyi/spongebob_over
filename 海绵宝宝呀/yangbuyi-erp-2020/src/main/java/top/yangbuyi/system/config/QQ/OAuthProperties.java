package top.yangbuyi.system.config.QQ;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  OAuthProperties
 * create:  2020-06-24 17:06
 *
 * @author: yangbuyi
 * @since： JDK1.8
 * <p>
 * 获取Code码
 **/

@Component
//对应application.yml中，oauth下参数
@ConfigurationProperties(prefix = "oauth")
public class OAuthProperties {
      
      //获取applicaiton.yml下qq下所有的参数
      private QQProperties qq = new QQProperties();
      
      public QQProperties getQQ() {
            return qq;
      }
      
      public void setQQ(QQProperties qq) {
            this.qq = qq;
      }
}

