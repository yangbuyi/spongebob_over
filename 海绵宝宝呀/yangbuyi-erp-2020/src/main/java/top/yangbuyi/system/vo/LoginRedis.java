package top.yangbuyi.system.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  LoginRedis
 * create:  2020-07-06 14:36
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRedis {
      
      private String loginname;
      private String username;
      private String password;
      private String keyCode;
      private String captcha;
      
}
