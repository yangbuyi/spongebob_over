package top.yangbuyi.system.vo;

import lombok.Data;

/**
 * ClassName: BaseVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 多个VO用到的属性
 */
@Data
public class BaseVo {
    private Integer page;
    private Integer limit;
    private Integer available;
}
