package top.yangbuyi.system.config.QQ.vo;

import lombok.Data;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  QQOpenidDTO
 * create:  2020-06-24 17:19
 *
 * @author: yangbuyi
 * @since： JDK1.8
 *
 * 用来获取 access_token、openid
 **/
@Data

public class QQOpenidDTO {
      
      private String openid;
      
      private String client_id;
      
}


