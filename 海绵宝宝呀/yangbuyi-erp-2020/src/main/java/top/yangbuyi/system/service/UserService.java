package top.yangbuyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.domain.User;
import top.yangbuyi.system.vo.UserVo;

/**
 * ClassName: UserService
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/14 19:50
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
public interface UserService extends IService<User> {
      
      /**
       * 根据用户名查询用户登陆信息
       */
      public User queryUserByLoginName(String loingname);
      
      
      /**
       * 加载用户列表
       *
       * @param roleVo
       * @return
       */
      DataGridView queryAllUser(UserVo roleVo);
      
      /**
       * 保存用户
       *
       * @param user
       * @return
       */
      User saveUser(User user);
      
      /**
       * 更新    上传图片 更新一下下
       *
       * @param user
       */
      User updateUser(User user);
      
      /**
       * 查询用户最大排序码
       *
       * @return
       */
      Integer queryUserMaxOrderNum();
      
      /**
       * 保存用户 与角色关系/
       *
       * @param uid
       * @param rids
       */
      void saveUserRole(Integer uid, Integer[] rids);
      
}
