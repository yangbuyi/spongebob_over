package top.yangbuyi.system.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * ClassName: LoginfoVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class UserVo extends  BaseVo{

	private String name;
	private String remark;
	private String address;
	  /**
	   * 部门ID
	   */
	private Integer deptid;

}