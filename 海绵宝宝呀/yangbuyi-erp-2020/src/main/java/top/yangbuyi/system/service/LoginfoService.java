package top.yangbuyi.system.service;

import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.domain.Loginfo;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.vo.LoginfoVo;

/**
 * ClassName: LoginfoService
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:10
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
public interface LoginfoService extends IService<Loginfo> {

    /**
     * 查询全部日志
     *
     * @param loginfoVo 条件
     * @return
     */
    DataGridView queryAllLoginfo(LoginfoVo loginfoVo);

}
