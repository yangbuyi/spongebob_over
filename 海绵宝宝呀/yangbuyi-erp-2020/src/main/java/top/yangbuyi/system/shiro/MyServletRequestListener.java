package top.yangbuyi.system.shiro;

import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  MyServletRequestListener
 * create:  2020-07-08 15:18
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/

/**
 * 使用 ServletRequestListener 获取访问信息
 *
 * @author shengwu ni
 * @date 2018/07/05
 */
@Component
public class MyServletRequestListener implements ServletRequestListener {
      
      private static final Logger logger = LoggerFactory.getLogger(MyServletRequestListener.class);
      
      
      @Override
      public void requestInitialized(ServletRequestEvent servletRequestEvent) {
            logger.info("杨不易：{MyServletRequestListener}");
      }
      
      @Override
      public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
      
            String header = WebUtils.toHttp(servletRequestEvent.getServletRequest()).getHeader("TOKEN");
            logger.info("请求来的TOKEN为 ：{}", header);
            
      }
      
}
