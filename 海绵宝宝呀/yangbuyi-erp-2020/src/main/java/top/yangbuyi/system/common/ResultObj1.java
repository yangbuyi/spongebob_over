package top.yangbuyi.system.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * ClassName: RestObj
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/14 20:04
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 返回值工具类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultObj1 implements Serializable {

    private Integer code;
    private String msg;
    private Integer codeback;

}