package top.yangbuyi.system.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import top.yangbuyi.system.domain.Loginfo;

import java.util.Date;

/**
 * ClassName: LoginfoVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 日志增强类
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LoginfoVo extends  BaseVo{


    /**
     * 登陆名称
     */
    private String loginname;
    /**
     * 登陆ip
     */
    private String loginip;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

}