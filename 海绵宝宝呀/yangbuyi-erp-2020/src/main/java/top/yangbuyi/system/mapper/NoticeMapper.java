package top.yangbuyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.system.domain.Notice;

/**
* ClassName: NoticeMapper
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/4/16 15:51
* @author TeouBle
* @author yangbuyi
* @version
* @since JDK 1.8
*/
public interface NoticeMapper extends BaseMapper<Notice> {
}