package top.yangbuyi.system.config.GITHUB;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  GitHubConstant
 * create:  2020-07-02 14:08
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/

/**
 * hub的参数常量
 */
public class GitHubConstant {
      
      // 这里填写在GitHub上注册应用时候获得 CLIENT ID
      public static final String  CLIENT_ID="b6376cd705014e40be18";
      //这里填写在GitHub上注册应用时候获得 CLIENT_SECRET
      public static final String CLIENT_SECRET="1f14bbf165c0cc1cc15360555a20eaea75691e36";
      // 回调路径
      public static final String CALLBACK = "https://www.yangbuyi.top/api/github/oauth2";
      
      //获取code的url
      public static final String CODE_URL = "https://github.com/login/oauth/authorize?client_id="+CLIENT_ID+"&state=STATE&redirect_uri="+CALLBACK+"";
      //获取token的url
      public static final String TOKEN_URL = "https://github.com/login/oauth/access_token?client_id="+CLIENT_ID+"&client_secret="+CLIENT_SECRET+"&code=CODE&redirect_uri="+CALLBACK+"";
      //获取用户信息的url
      public static final String USER_INFO_URL = "https://api.github.com/user?access_token=TOKEN";
}
