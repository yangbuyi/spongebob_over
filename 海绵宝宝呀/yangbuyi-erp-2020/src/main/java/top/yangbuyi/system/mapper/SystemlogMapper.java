package top.yangbuyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.system.domain.Systemlog;

/**
 * ClassName: SystemlogMapper
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/16 17:55
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
public interface SystemlogMapper extends BaseMapper<Systemlog> {
}