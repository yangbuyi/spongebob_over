package top.yangbuyi.system.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * ClassName: LoginfoVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 部门增强类
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleVo extends BaseVo {
	  
	  
	  private Integer userId;
	  
	  private String name;
	  private String remark;
	  
}