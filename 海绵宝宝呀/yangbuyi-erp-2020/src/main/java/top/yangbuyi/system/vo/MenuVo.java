package top.yangbuyi.system.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * ClassName: LoginfoVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 部门增强类
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class MenuVo extends  BaseVo{
    


}