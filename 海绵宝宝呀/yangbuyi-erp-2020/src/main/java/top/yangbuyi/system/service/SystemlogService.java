package top.yangbuyi.system.service;

import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.domain.Systemlog;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.system.vo.SystemlogVo;

/**
 * ClassName: SystemlogService
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 18:09
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
public interface SystemlogService extends IService<Systemlog> {


    /**
     * 查询操作日志
     * @param systemlogVo
     */
    DataGridView queryAllSystemLog(SystemlogVo systemlogVo);
}


