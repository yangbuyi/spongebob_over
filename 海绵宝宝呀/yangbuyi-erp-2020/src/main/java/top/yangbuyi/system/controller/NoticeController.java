package top.yangbuyi.system.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.system.common.ActiveUser;
import top.yangbuyi.system.common.ResultObj;
import top.yangbuyi.system.domain.Notice;
import top.yangbuyi.system.service.NoticeService;
import top.yangbuyi.system.vo.NoticeVo;

import java.util.Arrays;
import java.util.Date;

/**
 * ClassName: NoticeController
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/16 15:52
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
@RestController
@RequestMapping("api/notice")
public class NoticeController {
      
      @Autowired
      private NoticeService noticeService;
      
      /**
       * 公告数据
       *
       * @param noticeVo
       * @return
       */
      @RequestMapping("loadAllNotice")
      public Object loadAllNotice(NoticeVo noticeVo) {
            return this.noticeService.queryAllNotice(noticeVo);
      }
      
      
      /**
       * 添加公告
       *
       * @param notice
       * @return
       */
      @RequestMapping("addNotice")
      public Object addNotice(Notice notice) {
            try {
                  notice.setCreatetime(new Date());
                  ActiveUser principal = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
                  notice.setOpername(principal.getUser().getName());
                  this.noticeService.save(notice);
                  return ResultObj.ADD_SUCCESS;
            } catch (Exception e) {
                  e.printStackTrace();
                  return ResultObj.ADD_ERROR;
            }
      }
      
      /**
       * 修改公告
       *
       * @param notice
       * @return
       */
      @RequestMapping("updateNotice")
      public Object updateNotice(Notice notice) {
            try {
                  this.noticeService.updateById(notice);
                  return ResultObj.UPDATE_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.UPDATE_ERROR;
            }
      }
      
      /**
       * 删除公告
       *
       * @param id
       * @return
       */
      @RequestMapping("deleteNotice")
      public Object deleteNotice(Integer id) {
            try {
                  this.noticeService.removeById(id);
                  return ResultObj.DELETE_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.DELETE_ERROR;
            }
      }
      
      /**
       * 批量删除公告
       *
       * @param ids
       * @return
       */
      @RequestMapping("batchDeleteNotice")
      public Object batchDeleteNotice(Integer[] ids) {
            try {
                  this.noticeService.removeByIds(Arrays.asList(ids));
                  return ResultObj.DELETE_SUCCESS;
            } catch (Exception e) {
                  return ResultObj.DELETE_ERROR;
            }
      }
      
}