package top.yangbuyi.system.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  RequestLimit
 * create:  2020-07-02 15:25
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AccessLimit {
      /**
       * 秒
       * @return
       */
      int seconds();
      
      /**
       * 最大请求次数
       * @return
       */
      int maxCount();
      
      /**
       * 您必须先登入
       * @return
       */
      boolean needLogin() default true;
}

/*

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
*/
/**
 * 一分钟请求超过50次 限制等待300秒  默认值 自己定义
 *//*

public @interface RequestLimit {

      int time() default 60;
      int count() default 50;
      int waits() default 3000;

}
*/
