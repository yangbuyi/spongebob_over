package top.yangbuyi.system.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.yangbuyi.system.domain.Menu;
import top.yangbuyi.system.domain.User;

import java.io.Serializable;
import java.util.List;

/**
 * ClassName: ActiveUser
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/14 20:10
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActiveUser implements Serializable {
    /**
     * 用户类
     */
    private User user;
    /**
     * 角色
     */
    private List<String> roles;
    /**
     * 权限
     */
    private List<String> permissions;
}