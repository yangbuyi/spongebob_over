package top.yangbuyi.system.common;

import java.io.Serializable;

/**
 * ClassName: Constant
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/14 23:20
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 静态属性直接拿
 */

public class Constant implements Serializable {
      
      
      /**
       * 用户类型
       */
      public static final Integer USER_TYPE_SUPER = 0;
      public static final Integer USER_TYPE_NORMAL = 1;
      
      
      /**
       * 可用类型
       */
      public static final Integer AVAILABLE_TRUE = 1;
      public static final Integer AVAILABLE_FALSE = 0;
      
      /**
       * 权限类型
       */
      public static final String MENU_TYPE_TOP = "topmenu";
      public static final String MENU_TYPE_LEFT = "leftmenu";
      public static final String MENU_TYPE_PERMISSION = "permission";
      
      /**
       * 是否展开
       */
      public static final Integer SPREAD_TRUE = 1;
      public static final Integer SPREAD_FALSE = 0;
      
      /**
       * 密码
       */
      public static final String DEFAULT_PWD = "123456";
      
      /**
       * 默认头像地址 http://yangbuyi.top
       * http://yangbuyi.top/group1/M00/00/00/rBHkkV6f6SSAKajEAAhxrMIZycU316.jpg
       * 小盆友的
       * /group1/M00/00/00/rBHkkV6dtmuALC5bAAjYBWxrayQ371.gif
       */
      public static final String DEFAULT_TITLE_IMAGE = "/group1/M00/00/00/rBHkkV6f6SSAKajEAAhxrMIZycU316.jpg";
      /**
       * 第三方登陆默认的 角色
       */
      public static final Integer[] QQ_ROLE_DEFAULT = {18};
      // 默认的 角色
      public static Integer[] DEFAULT_ROLE_NAME = new Integer[]{17};
      
      /**
       * 审核类型
       */
      public static final String CHECKTAG = "checkTag";
      
      /**
       * 审核状态
       */
      public static final String APPROVE_WAITE = "S001-1";//未审核
      public static final String APPROVE_YES = "S001-2";//审核通过
      public static final String APPROVE_NO = "S001-3";//审核未通过
      
      /**
       * 设计状态
       */
      public static final String DESGIN_NO = "W001-1";    //未设计
      public static final String DESGIN_YES= "W001-2";    //已设计
      
      /**
       * 商品
       */
      public static final String TYPE_Y001_1 = "Y001-1";
      
      /**
       * 物料
       */
      public static final String TYPE_Y001_2 = "Y001-2";
      
      /**
       * 用途类型
       */
      public static final String TYPE = "type";
      
      /**
       * 档次
       */
      public static final String CLASS = "productClass";
      
      /**
       * 一二三级分类
       */
      public static final Integer PID = 0;
      
      /**
       * 第一次删除
       */
      public static final String DELETE_C001_1 = "C001-1";
      
      /**
       * 第二次删除
       */
      public static final String DELETE_C001_2 = "C001-2";
      
      
      /**
       *
       */
      public static final String CHANGE_TAG_1 = "B002-1";
      
      public static final String CHANGE_TAG_2 = "B002-2";
      
      
}
