package top.yangbuyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.system.domain.Loginfo;

/**
* ClassName: LoginfoMapper
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/4/15 19:10
* @author TeouBle
* @author yangbuyi
* @version
* @since JDK 1.8
*/
public interface LoginfoMapper extends BaseMapper<Loginfo> {
}