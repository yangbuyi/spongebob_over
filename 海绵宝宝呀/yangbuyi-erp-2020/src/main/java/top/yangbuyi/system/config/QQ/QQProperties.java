package top.yangbuyi.system.config.QQ;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  QQProperties
 * create:  2020-06-24 17:04
 *
 * @author: yangbuyi
 * @since： JDK1.8
 *
 * 集成第三方登陆 QQ  参数
 **/
@Data
@Component
public class QQProperties {
      /**
       * 你的appid
       */
      private String client_id;
      /**
       *  #你的appkey
       */
      private String client_secret;
      /**
       * 你接收响应code码地址
       */
      private String redirect_uri;
      /**
       * 腾讯获取code码地址
       */
      private String code_callback_uri;
      /**
       * 腾讯获取access_token地址
       */
      private String access_token_callback_uri;
      /**
       * 腾讯获取openid地址
       */
      private String openid_callback_uri;
      /**
       * 腾讯获取用户信息地址
       */
      private String user_info_callback_uri;
      
      
      /**
       * 要回调到哪个网站
       */
      private String redirect_url_index_yby;
      private String redirect_url_login_yby;
    
}

