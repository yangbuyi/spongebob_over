package top.yangbuyi.system.common;

import com.wf.captcha.*;

import java.io.IOException;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  Captcha
 * create:  2020-07-08 08:59
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/

 
public class Captcha {
      
      
      public static void main(String[] args) throws IOException {
            // png类型
//            SpecCaptcha captcha = new SpecCaptcha(130, 48);
//            captcha.text();  // 获取验证码的字符
//            captcha.textChar();  // 获取验证码的字符数组

//            // gif类型
//            GifCaptcha captcha = new GifCaptcha(130, 48);
//
//            // 中文类型
//            ChineseCaptcha captcha = new ChineseCaptcha(130, 48);
//
//            // 中文gif类型
//            ChineseGifCaptcha captcha = new ChineseGifCaptcha(130, 48);
//
            // 算术类型
            
            ArithmeticCaptcha captcha = new ArithmeticCaptcha(130, 48);
            captcha.setLen(3);  // 几位数运算，默认是两位
            captcha.getArithmeticString();  // 获取运算的公式：3+2=?
            captcha.text();  // 获取运算的结果：5
            
            captcha.out(WebUtils.getHttpServletResponse().getOutputStream());
      }
}
