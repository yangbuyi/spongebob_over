package top.yangbuyi.business.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import top.yangbuyi.system.vo.BaseVo;

import java.util.Date;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  InportVo
 * create:  2020-04-26 17:51
 *
 * @author: yangbuyi
 * @since： JDK1.8
 **/

@Data
@EqualsAndHashCode(callSuper = false)
public class InportVo extends BaseVo {
      /**
       * 供应商ID
       */
      private Integer providerid;
      /**
       * 商品ID
       */
      private Integer goodsid;
      /**
       * 结束时间
       */
      @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:dd")
      private Date endTime;
      /**
       * 开始时间
       */
      @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:dd")
      private Date startTime;
      
}
