package top.yangbuyi.business.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

/**
 * ClassName: LoginfoVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 公告增强类   模糊查询
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProviderVo extends BaseVo {
	  
	  /**
	   * 供应商名称
	   */
	  private String customername;
	  /**
	   * 供应商电话
	   */
	  private String phone;
	  /**
	   * 供应商联系人
	   */
	  private String connectionperson;
	  
	  
}