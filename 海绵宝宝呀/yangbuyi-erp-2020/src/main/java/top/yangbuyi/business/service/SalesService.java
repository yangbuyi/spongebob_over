package top.yangbuyi.business.service;

import top.yangbuyi.business.domain.Sales;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.business.vo.SalesVo;
import top.yangbuyi.system.common.DataGridView;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/27
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface SalesService extends IService<Sales> {
      
      /**
       * 查询全部商品销售信息
       *
       * @param salesVo
       * @return
       */
      DataGridView queryAllSales(SalesVo salesVo);
      
      Sales saveSales(Sales sales);
      
      Sales updateSales(Sales sales);
}
