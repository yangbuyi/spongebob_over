package top.yangbuyi.business.service;

import top.yangbuyi.business.domain.Provider;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.business.vo.ProviderVo;
import top.yangbuyi.system.common.DataGridView;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/25
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface ProviderService extends IService<Provider> {
	  
	  /**
	   * 获取所有供应商信息
	   *
	   * @param providerVo
	   * @return
	   */
	  DataGridView queryAllProvider(ProviderVo providerVo);
	  
	  /**
	   * 添加供应商
	   *
	   * @param provider
	   * @return
	   */
	  Provider saveProvider(Provider provider);
	  
	  /**
	   * 更新供应商信息
	   *
	   * @param provider
	   * @return
	   */
	  Provider updateProvider(Provider provider);
	
	/**
	 * 查询所有供应商 不分页
	 * @return
	 */
	DataGridView getAllAvailableProvider();
}
