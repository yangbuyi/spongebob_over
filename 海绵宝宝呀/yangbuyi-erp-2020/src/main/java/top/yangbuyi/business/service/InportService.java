package top.yangbuyi.business.service;

import top.yangbuyi.business.domain.Inport;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.business.vo.InportVo;
import top.yangbuyi.system.common.DataGridView;

import java.io.Serializable;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/26
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface InportService extends IService<Inport> {
      
      /**
       * 保存进货信息
       *
       * @param inportVo
       * @return
       */
      DataGridView queryAllInport(InportVo inportVo);
      
      /**
       * 保存进货信息
       *
       * @param inport
       * @return
       */
      Inport saveInport(Inport inport);
      
      /**
       * 更新进货信息
       *
       * @param inport
       * @return
       */
      Inport updateInport(Inport inport);
      
}
