package top.yangbuyi.business.service;

import top.yangbuyi.business.domain.Customer;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.business.vo.CustomerVo;
import top.yangbuyi.system.common.DataGridView;

import javax.xml.crypto.Data;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/25
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface CustomerService extends IService<Customer> {
	  
	  /**
	   * 查询全部联系人
	   *
	   * @param customerVo
	   * @return
	   */
	  DataGridView queryAllCustomer(CustomerVo customerVo);
	  
	  /**
	   * 保存  客户
	   *
	   * @param customer
	   * @return
	   */
	  Customer saveCustomer(Customer customer);
	  
	  /**
	   * 更新客户
	   *
	   * @param customer
	   * @return
	   */
	  Customer updateCustomer(Customer customer);
	
	/**
	 * 查询所有可用 的客户
	 * @return
	 */
	DataGridView getAllAvailableCustomer();
}
