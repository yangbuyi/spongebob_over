package top.yangbuyi.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.business.domain.Salesback;
import top.yangbuyi.business.service.SalesbackService;
import top.yangbuyi.business.service.SalesbackService;
import top.yangbuyi.business.vo.SalesbackVo;
import top.yangbuyi.system.common.ResultObj;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  SalesbackController
 * create:  2020-04-27 14:37
 *
 * @author: yangbuyi
 * @since： JDK1.8
 *
 * 退货
 **/

@RestController
@RequestMapping("api/salesback")
public class SalesbackController {
      
      
      @Autowired
      private SalesbackService salesbackService;
      
      
      /**
       * 查询商品退货
       */
      @RequestMapping("loadAllSalesback")
      public Object loadAllSalesback(SalesbackVo salesbackVo){
            return this.salesbackService.queryAllSalesback(salesbackVo);
      }
      
      /**
       * 添加退货信息
       */
      @RequestMapping("addSalesback")
      public ResultObj addSalesback(Salesback salesback){
            try {
                  this.salesbackService.saveSalesback(salesback);
                  return ResultObj.ADD_SUCCESS;
            }catch (Exception e)
            {
                  e.printStackTrace();
                  return ResultObj.ADD_ERROR;
            }
            
      }
      
}
