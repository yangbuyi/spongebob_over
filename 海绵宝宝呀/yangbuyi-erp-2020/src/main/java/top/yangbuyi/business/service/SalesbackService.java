package top.yangbuyi.business.service;

import top.yangbuyi.business.domain.Salesback;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.business.vo.SalesbackVo;
import top.yangbuyi.system.common.DataGridView;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/27
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface SalesbackService extends IService<Salesback> {
      
      
      DataGridView queryAllSalesback(SalesbackVo salesbackVo);
      
      Salesback saveSalesback(Salesback salesback);
}
