package top.yangbuyi.business.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import top.yangbuyi.system.vo.BaseVo;

import java.util.Date;

/**
 * ClassName: LoginfoVo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/15 19:17
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 * 公告增强类   模糊查询
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CustomerVo extends BaseVo {
	  
	  /**
	   * 客户名称
	   */
	  private String customername;
	  /**
	   * 联系人电话
	   */
	  private String phone;
	  /**
	   * 联系人
	   */
	  private String connectionperson;
	  
	  
}