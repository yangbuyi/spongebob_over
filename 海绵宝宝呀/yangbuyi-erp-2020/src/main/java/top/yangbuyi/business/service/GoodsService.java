package top.yangbuyi.business.service;

import top.yangbuyi.business.domain.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.business.vo.GoodsVo;
import top.yangbuyi.system.common.DataGridView;

/**
 * ClassName: Loginfo
 * Description: 杨不易网站 :www.yangbuyi.top
 * date: 2020/4/26
 *
 * @author TeouBle
 * @author yangbuyi
 * @since JDK 1.8
 **/

public interface GoodsService extends IService<Goods> {
      
      /**
       * 查询所有商品信息
       *
       * @param goodsVo
       * @return
       */
      DataGridView queryAllGoods(GoodsVo goodsVo);
      
      
      /**
       * 保存商品
       *
       * @param goods
       * @return 缓存用
       */
      Goods saveGoods(Goods goods);
      
      /**
       * 更新商品
       *
       * @param goods
       * @return 缓存用
       */
      Goods updateGoods(Goods goods);
      
      /**
       * 查询所有商品数据 不分页
       *
       * @return
       */
      DataGridView getAllAvailableGoods();
      
      /**
       * 根据供货商查询 商品
       *
       * @param providerid
       * @return
       */
      DataGridView getGoodsByProviderId(Integer providerid);
      
      /**
       * 根据商品ID  获取商品信息
       * @param goodsid
       * @return
       */
      DataGridView getGoodsByGoodId(Integer goodsid);
}
