package top.yangbuyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.business.domain.Inport;

/**
* ClassName: Loginfo
* Description: 杨不易网站 :www.yangbuyi.top
* date: 2020/4/26
* @author TeouBle
* @author yangbuyi
* @since JDK 1.8
**/

public interface InportMapper extends BaseMapper<Inport> {
}