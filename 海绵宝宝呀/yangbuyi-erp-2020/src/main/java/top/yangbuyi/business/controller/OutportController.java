package top.yangbuyi.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.business.domain.Outport;
import top.yangbuyi.business.service.OutportService;
import top.yangbuyi.business.vo.OutportVo;
import top.yangbuyi.system.common.ResultObj;

/**
 * description:  杨不易网站 :www.yangbuyi.top
 * program:  yangbuyi-erp-2020
 * ClassName:  OutportController
 * create:  2020-04-27 14:37
 *
 * @author: yangbuyi
 * @since： JDK1.8
 *
 * 退货
 **/

@RestController
@RequestMapping("api/outport")
public class OutportController {
      
      
      @Autowired
      private OutportService outportService;
      
      
      /**
       * 查询
       */
      @RequestMapping("loadAllOutport")
      public Object loadAllOutport(OutportVo outportVo){
            return this.outportService.queryAllOutport(outportVo);
      }
      
      /**
       * 添加退货信息
       */
      @RequestMapping("addOutport")
      public ResultObj addOutport(Outport outport){
            try {
                  this.outportService.saveOutport(outport);
                  return ResultObj.ADD_SUCCESS;
            }catch (Exception e)
            {
                  e.printStackTrace();
                  return ResultObj.ADD_ERROR;
            }
            
      }

}
