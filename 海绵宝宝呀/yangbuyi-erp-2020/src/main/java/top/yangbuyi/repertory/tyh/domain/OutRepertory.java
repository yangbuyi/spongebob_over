package top.yangbuyi.repertory.tyh.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@TableName(value = "out_repertory")
public class OutRepertory implements Serializable {
    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 出库单号
     */
    @TableField(value = "pay_id")
    private String payId;

    /**
     * 出库人
     */
    @TableField(value = "storer")
    private String storer;

    /**
     * 出库理由
     */
    @TableField(value = "reason")
    private String reason;

    /**
     * 出库详情理由
     */
    @TableField(value = "reasonexact")
    private String reasonexact;


    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 登记人
     */
    @TableField(value = "register")
    private String register;

    /**
     * 登记时间
     */
    @TableField(value = "register_time")
    private String registerTime;

    /**
     * 复核人
     */
    @TableField(value = "checker")
    private String checker;

    /**
     * 复核时间
     */
    @TableField(value = "check_time")
    private Date checkTime;

      private BigDecimal amountSum;

      private BigDecimal costPriceSum;

      public BigDecimal getAmountSum() {
            return amountSum;
      }

      public void setAmountSum(BigDecimal amountSum) {
            this.amountSum = amountSum;
      }

      public BigDecimal getCostPriceSum() {
            return costPriceSum;
      }

      public void setCostPriceSum(BigDecimal costPriceSum) {
            this.costPriceSum = costPriceSum;
      }

      /**
     * 审核状态S001-0 等待审核  S001-1 审核通过  S001-2 审核不通过
     */
    @TableField(value = "check_tag")
    private String checkTag;

    /**
     * 调度人
     */
    @TableField(value = "attemper")
    private String attemper;

    /**
     * 调度时间
     */
    @TableField(value = "attemper_time")
    private Date attemperTime;

    private List<OutRepertoryDetails> outRepertoryDetails;

      public List<OutRepertoryDetails> getOutRepertoryDetails() {
            return outRepertoryDetails;
      }

      public void setOutRepertoryDetails(List<OutRepertoryDetails> outRepertoryDetails) {
            this.outRepertoryDetails = outRepertoryDetails;
      }

      /**
     * 库存标志 K002-1: 已登记  K002-2: 已调度
     */
    @TableField(value = "store_tag")
    private String storeTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_AMOUNT_SUM = "amount_sum";

    public static final String COL_PAY_ID = "pay_id";

    public static final String COL_STORER = "storer";

    public static final String COL_REASON = "reason";

    public static final String COL_REASONEXACT = "reasonexact";

    public static final String COL_COST_PRICE_SUM = "cost_price_sum";

    public static final String COL_PAID_AMOUNT_SUM = "paid_amount_sum";

    public static final String COL_REMARK = "remark";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_ATTEMPER = "attemper";

    public static final String COL_ATTEMPER_TIME = "attemper_time";

    public static final String COL_STORE_TAG = "store_tag";

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * 获取出库单号
     *
     * @return pay_id - 出库单号
     */
    public String getPayId() {
        return payId;
    }

    /**
     * 设置出库单号
     *
     * @param payId 出库单号
     */
    public void setPayId(String payId) {
        this.payId = payId;
    }

    /**
     * 获取出库人
     *
     * @return storer - 出库人
     */
    public String getStorer() {
        return storer;
    }

    /**
     * 设置出库人
     *
     * @param storer 出库人
     */
    public void setStorer(String storer) {
        this.storer = storer;
    }

    /**
     * 获取出库理由
     *
     * @return reason - 出库理由
     */
    public String getReason() {
        return reason;
    }

    /**
     * 设置出库理由
     *
     * @param reason 出库理由
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 获取出库详情理由
     *
     * @return reasonexact - 出库详情理由
     */
    public String getReasonexact() {
        return reasonexact;
    }

    /**
     * 设置出库详情理由
     *
     * @param reasonexact 出库详情理由
     */
    public void setReasonexact(String reasonexact) {
        this.reasonexact = reasonexact;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取登记人
     *
     * @return register - 登记人
     */
    public String getRegister() {
        return register;
    }

    /**
     * 设置登记人
     *
     * @param register 登记人
     */
    public void setRegister(String register) {
        this.register = register;
    }

    /**
     * 获取登记时间
     *
     * @return register_time - 登记时间
     */
    public String getRegisterTime() {
        return registerTime;
    }

    /**
     * 设置登记时间
     *
     * @param registerTime 登记时间
     */
    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * 获取复核人
     *
     * @return checker - 复核人
     */
    public String getChecker() {
        return checker;
    }

    /**
     * 设置复核人
     *
     * @param checker 复核人
     */
    public void setChecker(String checker) {
        this.checker = checker;
    }

    /**
     * 获取复核时间
     *
     * @return check_time - 复核时间
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * 设置复核时间
     *
     * @param checkTime 复核时间
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * 获取审核状态S001-0 等待审核  S001-1 审核通过  S001-2 审核不通过
     *
     * @return check_tag - 审核状态S001-0 等待审核  S001-1 审核通过  S001-2 审核不通过
     */
    public String getCheckTag() {
        return checkTag;
    }

    /**
     * 设置审核状态S001-0 等待审核  S001-1 审核通过  S001-2 审核不通过
     *
     * @param checkTag 审核状态S001-0 等待审核  S001-1 审核通过  S001-2 审核不通过
     */
    public void setCheckTag(String checkTag) {
        this.checkTag = checkTag;
    }

    /**
     * 获取调度人
     *
     * @return attemper - 调度人
     */
    public String getAttemper() {
        return attemper;
    }

    /**
     * 设置调度人
     *
     * @param attemper 调度人
     */
    public void setAttemper(String attemper) {
        this.attemper = attemper;
    }

    /**
     * 获取调度时间
     *
     * @return attemper_time - 调度时间
     */
    public Date getAttemperTime() {
        return attemperTime;
    }

    /**
     * 设置调度时间
     *
     * @param attemperTime 调度时间
     */
    public void setAttemperTime(Date attemperTime) {
        this.attemperTime = attemperTime;
    }

    /**
     * 获取库存标志 K002-1: 已登记  K002-2: 已调度
     *
     * @return store_tag - 库存标志 K002-1: 已登记  K002-2: 已调度
     */
    public String getStoreTag() {
        return storeTag;
    }

    /**
     * 设置库存标志 K002-1: 已登记  K002-2: 已调度
     *
     * @param storeTag 库存标志 K002-1: 已登记  K002-2: 已调度
     */
    public void setStoreTag(String storeTag) {
        this.storeTag = storeTag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", payId=").append(payId);
        sb.append(", storer=").append(storer);
        sb.append(", reason=").append(reason);
        sb.append(", reasonexact=").append(reasonexact);
        sb.append(", remark=").append(remark);
        sb.append(", register=").append(register);
        sb.append(", registerTime=").append(registerTime);
        sb.append(", checker=").append(checker);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", checkTag=").append(checkTag);
        sb.append(", attemper=").append(attemper);
          sb.append(", amountSum=").append(amountSum);
          sb.append(", costPriceSum=").append(costPriceSum);
        sb.append(", attemperTime=").append(attemperTime);
        sb.append(", storeTag=").append(storeTag);
        sb.append("]");
        return sb.toString();
    }
}