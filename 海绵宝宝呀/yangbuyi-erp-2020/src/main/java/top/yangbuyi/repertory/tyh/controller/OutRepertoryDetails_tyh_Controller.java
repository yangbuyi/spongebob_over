package top.yangbuyi.repertory.tyh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.tyh.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.tyh.service.OutRepertory_tyh_DetailsService;
import top.yangbuyi.repertory.tyh.service.OutRepertory_tyh_Service;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@RestController
@RequestMapping("api/tanyuhao/OutRepertoryDetails")
public class OutRepertoryDetails_tyh_Controller {

      @Autowired
      private OutRepertory_tyh_DetailsService outRepertoryTyhDetailsService;
      @Autowired
      private OutRepertory_tyh_Service outRepertoryTyhService;

      /**
       * 获取出库产品物料详情
       * @param parentId
       * @return
       */
      @RequestMapping(value = "/getDetailsListByPid",method = RequestMethod.GET)
      public DataGridView getDetailsListByPid(@RequestParam("id") Integer parentId){
            List<OutRepertoryDetails> outRepertoryDetails= outRepertoryTyhDetailsService.getDetailsListByPid(parentId);
            return new DataGridView(outRepertoryDetails);
      }

      /**
       * 入库调度
       * @return
       */
      @RequestMapping(value = "/Warehouschedul",method = RequestMethod.POST)
      public DataGridView Warehouschedul(Integer id){
            outRepertoryTyhDetailsService.WarehouschedulDetails(id);
            outRepertoryTyhService.Warehouschedul(id);
            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("出库调度完成");
            return dataGridView;
      }

}
