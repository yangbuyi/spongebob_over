package top.yangbuyi.repertory.tyh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;
import top.yangbuyi.repertory.tyh.service.PutRepertory_tyh_Service;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@RestController
@RequestMapping("api/tanyuhao/putRepertory")
public class PutRepertory_tyh_Controller {

      @Autowired
      private PutRepertory_tyh_Service putRepertoryTyhService;

      /**
       * 获取入库登记以调度待登记的产品
       * @return
       */
      @RequestMapping(value = "/getPutRepertory",method = RequestMethod.GET)
      public DataGridView getPutRepertory(PutRepertory putRepertory){
            List<PutRepertory> list= putRepertoryTyhService.getDispatch(putRepertory);
            int count = list.size();
            long b = (int)count;
            return new DataGridView(b,list);
      }

      /**
       * 获取已经入库的物料
       */
      @RequestMapping(value = "/getDispatchByStoreTag",method = RequestMethod.GET)
      public DataGridView getDispatchByStoreTag(PutRepertory putRepertory){
            List<PutRepertory> list= putRepertoryTyhService.getDispatchByStoreTag(putRepertory);
            int count = list.size();
            long b = (int)count;
            return new DataGridView(b,list);
      }

      /**
       * 获取入库登记以调度待登记的产品
       * @return
       */
      @RequestMapping(value = "/getToReview",method = RequestMethod.GET)
      public DataGridView getToReview(PutRepertory putRepertory){
            List<PutRepertory> list= putRepertoryTyhService.getToReview(putRepertory);
            int count = list.size();
            long b = (int)count;
            return new DataGridView(b,list);
      }

      /**
       * 获取待入库调度的产品
       */
      @RequestMapping(value = "/getWarehouschedul",method = RequestMethod.GET)
      public DataGridView getWarehouschedul(PutRepertory putRepertory){
            List<PutRepertory> list= putRepertoryTyhService.getWarehouschedul(putRepertory);
            int count = list.size();
            long b = (int)count;
            return new DataGridView(b,list);
      }

}
