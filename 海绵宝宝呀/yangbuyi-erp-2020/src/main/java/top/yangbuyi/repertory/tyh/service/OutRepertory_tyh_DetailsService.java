package top.yangbuyi.repertory.tyh.service;

import top.yangbuyi.repertory.tyh.domain.OutRepertoryDetails;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface OutRepertory_tyh_DetailsService extends IService<OutRepertoryDetails>{

      public List<OutRepertoryDetails> getDetailsListByPid(Integer parentId);

      /**
       *出库调度
       */
      public void WarehouschedulDetails(Integer id);

}
