package top.yangbuyi.repertory.tyh.service;

import top.yangbuyi.repertory.tyh.domain.ProductFile;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ProductFile_tyh_Service extends IService<ProductFile>{

      /**
       * 获取安全库存配置单
       * */
      public List<ProductFile> getProductFile(ProductFile productFile);

      /**
       *制定安全库存配置单（改变字段值 表示需要过审）
       */
      public void updMakeConfig(int id);

}
