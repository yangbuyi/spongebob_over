package top.yangbuyi.repertory.zxe.service;

import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Vo;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Votou;

import java.util.ArrayList;
import java.util.List;

@Service
public interface PutRepertoryDetails_zxe_Service {
	public Boolean insertPutRepertoryDetails(List<PutRepertoryDetailsRe_zxe_Vo> list);
	ArrayList PutRepertoryDetailsid(Integer id);
	Integer deletePutRepertoryDetails(List<PutRepertoryDetailsRe_zxe_Votou> PutRepertoryDetailsRe_zxe_Votou);
}
