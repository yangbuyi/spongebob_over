package top.yangbuyi.repertory.yinshengjie.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.yinshengjie.mapper.OutRepertoryDetailsMapper_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetailsExample;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.yinshengjie.service.OutRepertoryDetailsService_yinshengjie;

@Service
public class OutRepertoryDetailsServiceYinshengjieImpl extends ServiceImpl<OutRepertoryDetailsMapper_yinshengjie, OutRepertoryDetails> implements OutRepertoryDetailsService_yinshengjie {

    @Override
    public long countByExample(OutRepertoryDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(OutRepertoryDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<OutRepertoryDetails> selectByExample(OutRepertoryDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(OutRepertoryDetails record,OutRepertoryDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(OutRepertoryDetails record,OutRepertoryDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }

    @Override
    public void addOutRepertoryDetails(OutRepertoryDetails o2) {
        baseMapper.addOutRepertoryDetails(o2);
    }

    @Override
    public void updateKcAmount(String productId, Integer amount) {
        baseMapper.updateKcAmount(productId,amount);
    }

    @Override
    public OutRepertoryDetails ShenheByParentId(int id) {
        return baseMapper.ShenheByParentId(id);
    }

    @Override
    public void addKCAmount(String productId, Integer amount) {
        baseMapper.addKCAmount(productId,amount);
    }

    @Override
    public void updateKcAmount2(Integer parentId, Integer amount,double subtotal) {
        baseMapper.updateKcAmount2(parentId,amount,subtotal);
    }

    @Override
    public OutRepertoryDetails queryId(String productId) {
        return baseMapper.queryId(productId);
    }

	@Override
	public int edit(OutRepertoryDetails details) {
		return baseMapper.edit(details);
	}

	/**
	 * 修改复核出库状态
	 *
	 * @param details
	 * @return
	 */
	@Override
	public int repetitionEdit(OutRepertoryDetails details) {
		return baseMapper.repetitionEdit(details);
	}
}
