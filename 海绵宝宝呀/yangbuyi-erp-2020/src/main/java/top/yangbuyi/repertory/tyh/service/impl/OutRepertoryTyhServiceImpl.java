package top.yangbuyi.repertory.tyh.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.tyh.mapper.OutRepertory_tyh_Mapper;
import top.yangbuyi.repertory.tyh.domain.OutRepertory;
import top.yangbuyi.repertory.tyh.service.OutRepertory_tyh_Service;
@Service
public class OutRepertoryTyhServiceImpl extends ServiceImpl<OutRepertory_tyh_Mapper, OutRepertory> implements OutRepertory_tyh_Service {

      @Autowired
      private OutRepertory_tyh_Mapper outRepertoryTyhMapper;

      /**
       * 获取出库单
       */
      @Override
      public List<OutRepertory> getOutRepertory(){
            return outRepertoryTyhMapper.getOutRepertory();
      }

      @Override
      public void Warehouschedul(Integer id) {
            outRepertoryTyhMapper.Warehouschedul(id);
      }

}
