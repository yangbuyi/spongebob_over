package top.yangbuyi.repertory.yishengjie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.yangbuyi.repertory.yishengjie.domain.RepertoryCell;

import java.util.List;

@Repository("repertorycellmapper")
public interface RepertoryCell_ysj_Mapper extends BaseMapper<RepertoryCell> {

}