package top.yangbuyi.repertory.tyh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.tyh.mapper.ProductFile_tyh_Mapper;
import top.yangbuyi.repertory.tyh.domain.ProductFile;
import top.yangbuyi.repertory.tyh.service.ProductFile_tyh_Service;
@Service
public class ProductFileTyhServiceImpl extends ServiceImpl<ProductFile_tyh_Mapper, ProductFile> implements ProductFile_tyh_Service {

      @Autowired
      private ProductFile_tyh_Mapper productFileTyhMapper;

      /**
       * 获取安全库存配置单
       * */
      @Override
      public List<ProductFile> getProductFile(ProductFile productFile) {
            QueryWrapper<ProductFile> queryWrapper=new QueryWrapper<>();
            if (productFile.getProductName()!=null&&productFile.getProductName()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("check_tag","S001-2")
                        .eq("design_cell_tag","K001-1")
                        .eq("product_name",productFile.getProductName());
            }else if(productFile.getRegisterTime()!=null&&productFile.getRegisterTime()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("check_tag","S001-2")
                        .eq("design_cell_tag","K001-1")
                        .likeRight("register_time",productFile.getRegisterTime());
            }else if((productFile.getProductName()!=null&&productFile.getProductName()!="")&&(productFile.getRegisterTime()!=null&&productFile.getRegisterTime()!="")){
                  queryWrapper.orderByDesc("id")
                        .eq("check_tag","S001-2")
                        .eq("design_cell_tag","K001-1")
                        .eq("product_name",productFile.getProductName())
                        .likeRight("register_time",productFile.getRegisterTime());
            }else{
                  queryWrapper.orderByDesc("id")
                        .eq("check_tag","S001-2")
                        .eq("design_cell_tag","K001-1");
            }
            return productFileTyhMapper.selectList(queryWrapper);
      }


      /**
       * 制定安全库存配置单（改变字段值 表示需要过审）
       */
      @Override
      public void updMakeConfig(int id) {
            productFileTyhMapper.updMakeConfig(id);
      }
}
