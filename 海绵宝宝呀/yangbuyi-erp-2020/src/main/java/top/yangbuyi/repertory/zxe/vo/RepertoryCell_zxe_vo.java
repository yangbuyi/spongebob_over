package top.yangbuyi.repertory.zxe.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class RepertoryCell_zxe_vo {

	@TableField(value = "product_id")
	private String productId;


	@TableField(value = "product_name")
	private String productName;

	@TableField(value = "product_describe")
	private String productDescribe;

	@TableField(value = "type")
	private String type;

	@TableField(value = "max_capacity_amount")
	private BigDecimal maxCapacityAmount;

	@TableField(value = "amount")
	private BigDecimal amount;

	@TableField(value = "amount_unit")
	private String amountUnit;

	@TableField(value = "cost_price")
	private BigDecimal costPrice;

	@TableField(value = "personal_unit")
	private String personalUnit;
}
