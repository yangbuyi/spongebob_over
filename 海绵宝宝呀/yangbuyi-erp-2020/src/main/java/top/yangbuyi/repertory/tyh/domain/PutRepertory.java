package top.yangbuyi.repertory.tyh.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "put_repertory")
public class PutRepertory implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "gather_id")
    private String gatherId;

    @TableField(value = "storer")
    private String storer;

    @TableField(value = "reason")
    private String reason;

    @TableField(value = "reasonexact")
    private String reasonexact;

    @TableField(value = "amount_sum")
    private BigDecimal amountSum;

    @TableField(value = "cost_price_sum")
    private BigDecimal costPriceSum;

    @TableField(value = "gathered_amount_sum")
    private BigDecimal gatheredAmountSum;

    @TableField(value = "remark")
    private String remark;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private String registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "attemper")
    private String attemper;

    @TableField(value = "attemper_time")
    private Date attemperTime;

    @TableField(value = "store_tag")
    private String storeTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_GATHER_ID = "gather_id";

    public static final String COL_STORER = "storer";

    public static final String COL_REASON = "reason";

    public static final String COL_REASONEXACT = "reasonexact";

    public static final String COL_AMOUNT_SUM = "amount_sum";

    public static final String COL_COST_PRICE_SUM = "cost_price_sum";

    public static final String COL_GATHERED_AMOUNT_SUM = "gathered_amount_sum";

    public static final String COL_REMARK = "remark";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_ATTEMPER = "attemper";

    public static final String COL_ATTEMPER_TIME = "attemper_time";

    public static final String COL_STORE_TAG = "store_tag";

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return gather_id
     */
    public String getGatherId() {
        return gatherId;
    }

    /**
     * @param gatherId
     */
    public void setGatherId(String gatherId) {
        this.gatherId = gatherId;
    }

    /**
     * @return storer
     */
    public String getStorer() {
        return storer;
    }

    /**
     * @param storer
     */
    public void setStorer(String storer) {
        this.storer = storer;
    }

    /**
     * @return reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return reasonexact
     */
    public String getReasonexact() {
        return reasonexact;
    }

    /**
     * @param reasonexact
     */
    public void setReasonexact(String reasonexact) {
        this.reasonexact = reasonexact;
    }

    /**
     * @return amount_sum
     */
    public BigDecimal getAmountSum() {
        return amountSum;
    }

    /**
     * @param amountSum
     */
    public void setAmountSum(BigDecimal amountSum) {
        this.amountSum = amountSum;
    }

    /**
     * @return cost_price_sum
     */
    public BigDecimal getCostPriceSum() {
        return costPriceSum;
    }

    /**
     * @param costPriceSum
     */
    public void setCostPriceSum(BigDecimal costPriceSum) {
        this.costPriceSum = costPriceSum;
    }

    /**
     * @return gathered_amount_sum
     */
    public BigDecimal getGatheredAmountSum() {
        return gatheredAmountSum;
    }

    /**
     * @param gatheredAmountSum
     */
    public void setGatheredAmountSum(BigDecimal gatheredAmountSum) {
        this.gatheredAmountSum = gatheredAmountSum;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return register
     */
    public String getRegister() {
        return register;
    }

    /**
     * @param register
     */
    public void setRegister(String register) {
        this.register = register;
    }

    /**
     * @return register_time
     */
    public String getRegisterTime() {
        return registerTime;
    }

    /**
     * @param registerTime
     */
    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * @return checker
     */
    public String getChecker() {
        return checker;
    }

    /**
     * @param checker
     */
    public void setChecker(String checker) {
        this.checker = checker;
    }

    /**
     * @return check_time
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * @param checkTime
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * @return check_tag
     */
    public String getCheckTag() {
        return checkTag;
    }

    /**
     * @param checkTag
     */
    public void setCheckTag(String checkTag) {
        this.checkTag = checkTag;
    }

    /**
     * @return attemper
     */
    public String getAttemper() {
        return attemper;
    }

    /**
     * @param attemper
     */
    public void setAttemper(String attemper) {
        this.attemper = attemper;
    }

    /**
     * @return attemper_time
     */
    public Date getAttemperTime() {
        return attemperTime;
    }

    /**
     * @param attemperTime
     */
    public void setAttemperTime(Date attemperTime) {
        this.attemperTime = attemperTime;
    }

    /**
     * @return store_tag
     */
    public String getStoreTag() {
        return storeTag;
    }

    /**
     * @param storeTag
     */
    public void setStoreTag(String storeTag) {
        this.storeTag = storeTag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gatherId=").append(gatherId);
        sb.append(", storer=").append(storer);
        sb.append(", reason=").append(reason);
        sb.append(", reasonexact=").append(reasonexact);
        sb.append(", amountSum=").append(amountSum);
        sb.append(", costPriceSum=").append(costPriceSum);
        sb.append(", gatheredAmountSum=").append(gatheredAmountSum);
        sb.append(", remark=").append(remark);
        sb.append(", register=").append(register);
        sb.append(", registerTime=").append(registerTime);
        sb.append(", checker=").append(checker);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", checkTag=").append(checkTag);
        sb.append(", attemper=").append(attemper);
        sb.append(", attemperTime=").append(attemperTime);
        sb.append(", storeTag=").append(storeTag);
        sb.append("]");
        return sb.toString();
    }
}