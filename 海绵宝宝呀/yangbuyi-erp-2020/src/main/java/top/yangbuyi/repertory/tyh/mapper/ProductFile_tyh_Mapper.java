package top.yangbuyi.repertory.tyh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.yangbuyi.repertory.tyh.domain.ProductFile;

@Repository
public interface ProductFile_tyh_Mapper extends BaseMapper<ProductFile> {

      /**
       *制定安全库存配置单（改变字段值 表示需要过审）
       */
      public void updMakeConfig(int id);

}