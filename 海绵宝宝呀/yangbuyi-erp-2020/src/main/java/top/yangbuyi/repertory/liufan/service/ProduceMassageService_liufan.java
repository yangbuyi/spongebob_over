package top.yangbuyi.repertory.liufan.service;

import java.util.List;
import top.yangbuyi.repertory.liufan.domain.ProduceMassage;
import top.yangbuyi.repertory.liufan.domain.ProduceMassageExample;
import com.baomidou.mybatisplus.extension.service.IService;
public interface ProduceMassageService_liufan extends IService<ProduceMassage>{


    long countByExample(ProduceMassageExample example);

    int deleteByExample(ProduceMassageExample example);

    List<ProduceMassage> selectByExample(ProduceMassageExample example);

    int updateByExampleSelective(ProduceMassage record, ProduceMassageExample example);

    int updateByExample(ProduceMassage record, ProduceMassageExample example);

}
