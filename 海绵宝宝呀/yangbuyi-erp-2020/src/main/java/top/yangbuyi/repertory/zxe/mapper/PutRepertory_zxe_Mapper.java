package top.yangbuyi.repertory.zxe.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.zxe.domain.PutRepertory;

import java.util.ArrayList;

@Mapper
public interface PutRepertory_zxe_Mapper extends BaseMapper<PutRepertory> {
	int PutRepertoryadd(@Param("PutRepertory") PutRepertory PutRepertory);
	PutRepertory PutRepertoryselname(@Param("id") String id);
	ArrayList<PutRepertory> PutRepertorychecker(@Param("name") String name);
	int PutRepertoryupdate(@Param("check") String check,@Param("id") Integer id,@Param("gather") String gather,@Param("name")String name);
	ArrayList<PutRepertory> PutRepertorychecker2(@Param("check")String check,@Param("name") String name);
	int PutRepertoryupdateid(@Param("id")Integer id,@Param("reason")String reason,@Param("amountsum")Integer amountsum,@Param("costpricesum")Integer costpricesum);
}