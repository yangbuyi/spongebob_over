package top.yangbuyi.repertory.tyh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.repertory.tyh.domain.OutRepertory;
import top.yangbuyi.repertory.tyh.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.tyh.domain.PutRepertoryDetails;

import java.util.List;

public interface OutRepertoryDetails_tyh_Mapper extends BaseMapper<OutRepertoryDetails> {

      /**
       * 获取出库产品物料详情
       */
      public List<OutRepertoryDetails> getDetailsListByPid(Integer parentId);

      /**
       *出库调度
       */
      public void WarehouschedulDetails(Integer id);

}