package top.yangbuyi.repertory.zxe.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ToString
@TableName(value = "put_repertory")
public class PutRepertory {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "gather_id")
    private String gatherId;

	public void setGatherId(String gatherId) {
		this.gatherId = gatherId;
	}

	@TableField(value = "storer")
    private String storer;

    @TableField(value = "reason")
    private String reason;

    @TableField(value = "reasonexact")
    private String reasonexact;

    @TableField(value = "amount_sum")
    private BigDecimal amountSum;

    @TableField(value = "cost_price_sum")
    private BigDecimal costPriceSum;

    @TableField(value = "gathered_amount_sum")
    private BigDecimal gatheredAmountSum;

    @TableField(value = "remark")
    private String remark;

    @TableField(value = "register")
    private String register;

	public void setRegister(String register) {
		this.register = register;
	}

	@TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "attemper")
    private String attemper;

    @TableField(value = "attemper_time")
    private Date attemperTime;

    @TableField(value = "store_tag")
    private String storeTag;
}