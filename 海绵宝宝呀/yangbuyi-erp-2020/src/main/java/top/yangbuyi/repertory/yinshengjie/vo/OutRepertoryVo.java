package top.yangbuyi.repertory.yinshengjie.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

@Data
@EqualsAndHashCode(callSuper = false)
public class OutRepertoryVo extends BaseVo {
    private String storer; //申请出库人
    private String reason; //出库理由
    private String remark;//备注
    private String productId;//产品编号
    private String productName;//产品明朝
    private String productDescribe;//产品描述
    private int amount;//数量
    private String amountUnit;//单位
    private double costPrice;//单价

//    private List<OutRepertoryDetails>  OutDetails;
}
