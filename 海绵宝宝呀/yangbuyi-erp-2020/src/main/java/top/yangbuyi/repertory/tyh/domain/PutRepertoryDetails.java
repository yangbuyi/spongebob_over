package top.yangbuyi.repertory.tyh.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;

@TableName(value = "put_repertory_details")
public class PutRepertoryDetails implements Serializable {
    @TableId(value = "pid", type = IdType.AUTO)
    private Integer pid;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "amount")
    private int amount;

    @TableField(value = "amount_unit")
    private String amountUnit;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    @TableField(value = "gathered_amount")
    private int gatheredAmount;

    @TableField(value = "gather_tag")
    private String gatherTag;

    private ProductFile productFile;
    private RepertoryCell repertoryCell;

    private static final long serialVersionUID = 1L;

    public static final String COL_PID = "pid";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_GATHERED_AMOUNT = "gathered_amount";

    public static final String COL_GATHER_TAG = "gather_tag";

    /**
     * @return pid
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * @param pid
     */
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     * @return parent_id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * @param parentId
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return product_name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return product_describe
     */
    public String getProductDescribe() {
        return productDescribe;
    }

    /**
     * @param productDescribe
     */
    public void setProductDescribe(String productDescribe) {
        this.productDescribe = productDescribe;
    }

    /**
     * @return amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return amount_unit
     */
    public String getAmountUnit() {
        return amountUnit;
    }

    /**
     * @param amountUnit
     */
    public void setAmountUnit(String amountUnit) {
        this.amountUnit = amountUnit;
    }

    /**
     * @return cost_price
     */
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    /**
     * @param costPrice
     */
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return subtotal
     */
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    /**
     * @param subtotal
     */
    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @return gathered_amount
     */
    public int getGatheredAmount() {
        return gatheredAmount;
    }

    /**
     * @param gatheredAmount
     */
    public void setGatheredAmount(int gatheredAmount) {
        this.gatheredAmount = gatheredAmount;
    }

    /**
     * @return gather_tag
     */
    public String getGatherTag() {
        return gatherTag;
    }

    /**
     * @param gatherTag
     */
    public void setGatherTag(String gatherTag) {
        this.gatherTag = gatherTag;
    }

      public ProductFile getProductFile() {
            return productFile;
      }

      public void setProductFile(ProductFile productFile) {
            this.productFile = productFile;
      }

      public RepertoryCell getRepertoryCell() {
            return repertoryCell;
      }

      public void setRepertoryCell(RepertoryCell repertoryCell) {
            this.repertoryCell = repertoryCell;
      }

      @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pid=").append(pid);
        sb.append(", parentId=").append(parentId);
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", productDescribe=").append(productDescribe);
        sb.append(", amount=").append(amount);
        sb.append(", amountUnit=").append(amountUnit);
        sb.append(", costPrice=").append(costPrice);
        sb.append(", subtotal=").append(subtotal);
        sb.append(", gatheredAmount=").append(gatheredAmount);
        sb.append(", gatherTag=").append(gatherTag);
        sb.append(", repertoryCell=").append(repertoryCell);
        sb.append("]");
        return sb.toString();
    }
}