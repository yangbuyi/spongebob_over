package top.yangbuyi.repertory.yinshengjie.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetailsExample;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OutRepertoryDetailsService_yinshengjie extends IService<OutRepertoryDetails>{


    long countByExample(OutRepertoryDetailsExample example);

    int deleteByExample(OutRepertoryDetailsExample example);

    List<OutRepertoryDetails> selectByExample(OutRepertoryDetailsExample example);

    int updateByExampleSelective(OutRepertoryDetails record, OutRepertoryDetailsExample example);

    int updateByExample(OutRepertoryDetails record, OutRepertoryDetailsExample example);

    void addOutRepertoryDetails(OutRepertoryDetails o2);

    void updateKcAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    OutRepertoryDetails ShenheByParentId(int id);

    void addKCAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    void updateKcAmount2(@Param("parentId") Integer parentId, @Param("amount") Integer amount, @Param("subtotal") double subtotal);

    OutRepertoryDetails queryId(String productId);

	/**
	 * 修改出库状态
	 * @param details
	 * @return
	 */
	int edit(OutRepertoryDetails details);
	/**
	 * 修改复核出库状态
	 * @param details
	 * @return
	 */
	int repetitionEdit(OutRepertoryDetails details);
}
