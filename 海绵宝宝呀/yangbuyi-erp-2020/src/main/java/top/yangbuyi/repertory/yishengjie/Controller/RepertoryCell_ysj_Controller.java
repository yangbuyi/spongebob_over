package top.yangbuyi.repertory.yishengjie.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.yangbuyi.repertory.yishengjie.domain.RepertoryCell;
import top.yangbuyi.repertory.yishengjie.service.RepertoryCell_ysj_Service;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@Controller
@RequestMapping("api/yingshengjie/repertoryCell")
public class RepertoryCell_ysj_Controller {

      @Autowired
      private RepertoryCell_ysj_Service repertoryCellYsjService;

      /**
       * 查询
       * @param repertoryCell
       * @return
       */
      @ResponseBody
      @RequestMapping("findRepertory")
      public DataGridView findRepertory(RepertoryCell repertoryCell){
            DataGridView dataGridView = new DataGridView();
            List<RepertoryCell> repertoryCells = repertoryCellYsjService.pageQuery(repertoryCell);
            dataGridView.setData(repertoryCells);
            dataGridView.setCount((long) repertoryCells.size());
            return dataGridView;
      }

}
