package top.yangbuyi.repertory.liufan.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

@Data
@EqualsAndHashCode(callSuper = false)
public class repertoryCellVo extends BaseVo {
      private String rcStoreName;//库房名称
      private String rcProductId;//出库产品编号
      private String rcProductName;//出库产品名称
      private Integer rcAmount;//库存数量
      private double prdCostPrice;//出库商品单价
      private String rcAmountUnit;//单位
      private String rcConfing;//产品描述
}
