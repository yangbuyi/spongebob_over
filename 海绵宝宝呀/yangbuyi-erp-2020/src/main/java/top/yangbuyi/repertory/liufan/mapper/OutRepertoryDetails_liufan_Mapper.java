package top.yangbuyi.repertory.liufan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetailsExample;

import java.util.List;

public interface OutRepertoryDetails_liufan_Mapper extends BaseMapper<OutRepertoryDetails> {
    long countByExample(OutRepertoryDetailsExample example);

    int deleteByExample(OutRepertoryDetailsExample example);

    List<OutRepertoryDetails> selectByExample(OutRepertoryDetailsExample example);

    int updateByExampleSelective(@Param("record") OutRepertoryDetails record, @Param("example") OutRepertoryDetailsExample example);

    int updateByExample(@Param("record") OutRepertoryDetails record, @Param("example") OutRepertoryDetailsExample example);

    void addOutRepertoryDetails(OutRepertoryDetails o2);

    void updateKcAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    List<OutRepertoryDetails> ShenheByParentId(int id);

    void addKCAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    void updateKcAmount2(@Param("parentId") Integer parentId, @Param("amount") Integer amount, @Param("subtotal") double subtotal);

    OutRepertoryDetails queryId(String productId);

    void updatePMTag(@Param("pmProduct") String pmProduct);
}
