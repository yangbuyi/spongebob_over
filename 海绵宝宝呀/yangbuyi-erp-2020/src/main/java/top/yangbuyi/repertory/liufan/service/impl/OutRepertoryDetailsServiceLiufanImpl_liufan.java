package top.yangbuyi.repertory.liufan.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.liufan.service.OutRepertoryDetailsService_liufan;
import top.yangbuyi.repertory.liufan.mapper.OutRepertoryDetails_liufan_Mapper;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetailsExample;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetails;

@Service
public class OutRepertoryDetailsServiceLiufanImpl_liufan extends ServiceImpl<OutRepertoryDetails_liufan_Mapper, OutRepertoryDetails> implements OutRepertoryDetailsService_liufan {

    @Override
    public long countByExample(OutRepertoryDetailsExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(OutRepertoryDetailsExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<OutRepertoryDetails> selectByExample(OutRepertoryDetailsExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(OutRepertoryDetails record,OutRepertoryDetailsExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(OutRepertoryDetails record,OutRepertoryDetailsExample example) {
        return baseMapper.updateByExample(record,example);
    }

    @Override
    public void addOutRepertoryDetails(OutRepertoryDetails o2) {
        baseMapper.addOutRepertoryDetails(o2);
    }

    @Override
    public void updateKcAmount(String productId, Integer amount) {
        baseMapper.updateKcAmount(productId,amount);
    }

    @Override
    public List<OutRepertoryDetails> ShenheByParentId(int id) {
        return baseMapper.ShenheByParentId(id);
    }

    @Override
    public void addKCAmount(String productId, Integer amount) {
        baseMapper.addKCAmount(productId,amount);
    }

    @Override
    public void updateKcAmount2(Integer parentId, Integer amount,double subtotal) {
        baseMapper.updateKcAmount2(parentId,amount,subtotal);
    }

    @Override
    public OutRepertoryDetails queryId(String productId) {
        return baseMapper.queryId(productId);
    }

    @Override
    public void updatePMTag(String pmProduct) {
        baseMapper.updatePMTag(pmProduct);
    }

}
