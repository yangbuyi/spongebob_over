package top.yangbuyi.repertory.zxe.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@TableName(value = "put_repertory_details")
public class PutRepertoryDetails {
    @TableId(value = "pid", type = IdType.AUTO)
    private Integer pid;

    @TableField(value = "parent_id")
    private Integer parentId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "amount_unit")
    private String amountUnit;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    @TableField(value = "gathered_amount")
    private BigDecimal gatheredAmount;

    @TableField(value = "gather_tag")
    private String gatherTag;
}