package top.yangbuyi.repertory.zxe.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.yangbuyi.repertory.zxe.domain.RepertoryCell;

import java.util.ArrayList;

@Mapper
public interface RepertoryCell_zxe_Mapper extends BaseMapper<RepertoryCell> {
	ArrayList getRepertroyCellList();
}