package top.yangbuyi.repertory.tyh.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.tyh.domain.ProductFile;
import top.yangbuyi.repertory.tyh.service.ProductFile_tyh_Service;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@RestController
@RequestMapping("api/tanyuhao/ProductFile")
public class ProductFile_tyh_Controller {

      @Autowired
      private ProductFile_tyh_Service productFileTyhService;

      /**
       * 获取安全库存配置单
       * */
      @RequestMapping(value = "/productFileList",method = RequestMethod.GET)
      public DataGridView getProductFile(ProductFile productFile){
            System.out.println("产品名："+productFile.getProductName()+" "+"时间："+productFile.getRegisterTime());
            List<ProductFile> productFiles= productFileTyhService.getProductFile(productFile);
            DataGridView dataGridView=new DataGridView();
            int count = productFiles.size();
            long b = (int)count;
            dataGridView.setCount(b);
            dataGridView.setData(productFiles);
            return dataGridView;
      }

}
