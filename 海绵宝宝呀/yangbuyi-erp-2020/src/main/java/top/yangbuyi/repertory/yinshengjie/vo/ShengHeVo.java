package top.yangbuyi.repertory.yinshengjie.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;


@Data
@EqualsAndHashCode(callSuper = false)
public class ShengHeVo extends BaseVo {
    private Integer o1Id;
    private String o1PayId;
    private String o1Reason;
    private String o1RegisterTime;
    private double o2Amount;
    private double o2Subtotal;
	private String  o1CheckTag;

}
