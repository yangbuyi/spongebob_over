package top.yangbuyi.repertory.yinshengjie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetailsExample;

import java.util.List;

@Mapper
public interface OutRepertoryDetailsMapper_yinshengjie extends BaseMapper<OutRepertoryDetails> {
    long countByExample(OutRepertoryDetailsExample example);

    int deleteByExample(OutRepertoryDetailsExample example);

    List<OutRepertoryDetails> selectByExample(OutRepertoryDetailsExample example);

    int updateByExampleSelective(@Param("record") OutRepertoryDetails record, @Param("example") OutRepertoryDetailsExample example);

    int updateByExample(@Param("record") OutRepertoryDetails record, @Param("example") OutRepertoryDetailsExample example);

    void addOutRepertoryDetails(OutRepertoryDetails o2);

    void updateKcAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    OutRepertoryDetails ShenheByParentId(int id);

    void addKCAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    void updateKcAmount2(@Param("parentId") Integer parentId, @Param("amount") Integer amount, @Param("subtotal") double subtotal);

    OutRepertoryDetails queryId(String productId);

	/**
	 * 修改出库状态 K002-1 K002-2
	 * @param details
	 * @return
	 */
    int edit(@Param("details") OutRepertoryDetails details);
	/**
	 * 修改复核出库状态 K002-1 K002-2
	 * @param details
	 * @return
	 */
	int repetitionEdit(@Param("details") OutRepertoryDetails details);

}
