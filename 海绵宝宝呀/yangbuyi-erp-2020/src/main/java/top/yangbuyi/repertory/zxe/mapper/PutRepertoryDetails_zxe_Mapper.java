package top.yangbuyi.repertory.zxe.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.zxe.domain.PutRepertoryDetails;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Vo;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Votou;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface PutRepertoryDetails_zxe_Mapper extends BaseMapper<PutRepertoryDetails> {
	Integer insertPutRepertoryDetails(@Param("PutRepertoryDetailsReVo") List<PutRepertoryDetailsRe_zxe_Vo> list);
	Integer insertPutRepertoryDetailsupd(@Param("PutRepertoryDetailsReVo") List<PutRepertoryDetailsRe_zxe_Votou> list);
	ArrayList<PutRepertoryDetails> PutRepertoryDetailsid(Integer id);
	Integer PutRepertoryDetailsidupdate(@Param("gather") String gather, @Param("id") Integer id);
	Integer deletePutRepertoryDetails(@Param("id")Integer id);
}