package top.yangbuyi.repertory.zxe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.zxe.domain.PutRepertory;
import top.yangbuyi.repertory.zxe.service.PutRepertory_zxe_Service;
import top.yangbuyi.system.common.DataGridView;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/zhangxinge/PutRepertory")
public class PutRepertory_zxe_Controller {
      
      @Autowired
      private PutRepertory_zxe_Service PutRepertory_zxe_Service;
      
      @RequestMapping("/PutRepertoryadd")
      private Object PutRepertoryadd(@RequestBody List<PutRepertory> PutRepertory) {
            return PutRepertory_zxe_Service.PutRepertoryadd(PutRepertory.get(0));
      }
      
      @RequestMapping("/PutRepertorychecker")
      private Object PutRepertorychecker(String name) {
            ArrayList arrayList = PutRepertory_zxe_Service.PutRepertorychecker(name);
            return new DataGridView(Long.valueOf(arrayList.size()), arrayList);
      }
      
      @RequestMapping("/PutRepertoryname")
      private Object PutRepertoryname(String name) {
            ArrayList arrayList = PutRepertory_zxe_Service.PutRepertorychecker(name);
            
            return new DataGridView(Long.valueOf(arrayList.size()), arrayList);
      }
      
      @RequestMapping("/PutRepertoryupdate")
      private Object PutRepertoryupdate(String check, Integer[] id, String gather) {
            System.out.println(check + id + gather);
            return new DataGridView(PutRepertory_zxe_Service.PutRepertoryupdate(check, id, gather));
      }
      
      @RequestMapping("/PutRepertorychecker2")
      private Object PutRepertorychecker2(String check, String name) {
            ArrayList<PutRepertory> putRepertories = PutRepertory_zxe_Service.PutRepertorychecker2(check, name);
            return new DataGridView(Long.valueOf(putRepertories.size()), putRepertories);
      }
}
