package top.yangbuyi.repertory.tyh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;

@Repository
public interface PutRepertory_tyh_Mapper extends BaseMapper<PutRepertory> {

      public void Warehouschedul(Integer id);

}