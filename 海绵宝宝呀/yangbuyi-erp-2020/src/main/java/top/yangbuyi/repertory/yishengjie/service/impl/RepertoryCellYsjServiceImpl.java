package top.yangbuyi.repertory.yishengjie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.yishengjie.domain.RepertoryCell;
import top.yangbuyi.repertory.yishengjie.mapper.RepertoryCell_ysj_Mapper;
import top.yangbuyi.repertory.yishengjie.service.RepertoryCell_ysj_Service;
@Service("repertorycell")
public class RepertoryCellYsjServiceImpl extends ServiceImpl<RepertoryCell_ysj_Mapper, RepertoryCell> implements RepertoryCell_ysj_Service {

      @Autowired
      private RepertoryCell_ysj_Mapper repertoryCellYsjMapper;

      @Override
      public List<RepertoryCell> pageQuery(RepertoryCell repertoryCell) {
            QueryWrapper<RepertoryCell> queryWrapper=new QueryWrapper<>();
            if(repertoryCell.getStoreId()!="" && repertoryCell.getStoreId() !=null){
                  queryWrapper.like("store_Id",repertoryCell.getStoreId());
            }else if (repertoryCell.getFirstKindName()!="" && repertoryCell.getFirstKindName() !=null){
                  queryWrapper.like("first_kind_name",repertoryCell.getFirstKindName());
            }else if((repertoryCell.getStoreId()!="" && repertoryCell.getStoreId() !=null)&&(repertoryCell.getFirstKindName()!="" && repertoryCell.getFirstKindName() !=null)){
                  queryWrapper.like("first_kind_name",repertoryCell.getFirstKindName())
                        .like("store_Id",repertoryCell.getStoreId());
            }
            return repertoryCellYsjMapper.selectList(queryWrapper);
      }
}
