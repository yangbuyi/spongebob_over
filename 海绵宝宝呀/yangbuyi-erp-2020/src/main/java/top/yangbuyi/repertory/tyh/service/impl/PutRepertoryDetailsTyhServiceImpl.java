package top.yangbuyi.repertory.tyh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;
import top.yangbuyi.repertory.tyh.domain.PutRepertoryDetails;
import top.yangbuyi.repertory.tyh.mapper.PutRepertoryDetails_tyh_Mapper;
import top.yangbuyi.repertory.tyh.mapper.PutRepertory_tyh_Mapper;
import top.yangbuyi.repertory.tyh.service.PutRepertoryDetails_tyh_Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class PutRepertoryDetailsTyhServiceImpl extends ServiceImpl<PutRepertoryDetails_tyh_Mapper, PutRepertoryDetails> implements PutRepertoryDetails_tyh_Service {

      @Autowired
      private PutRepertoryDetails_tyh_Mapper putRepertoryDetailsTyhMapper;
      @Autowired
      private PutRepertory_tyh_Mapper putRepertoryTyhMapper;

      /**
       * 获取入库产品物料详情
       */
      @Override
      public List<PutRepertoryDetails> getDetailsListByPid(Integer parentId){
            return putRepertoryDetailsTyhMapper.getDetailsListByPid(parentId);
      }

      /**
       * 入库登记
       */
      @Override
      public void Warehousing(Integer parentId, BigDecimal subtotal, int gatheredAmount){
            putRepertoryDetailsTyhMapper.Warehousing(parentId,subtotal,gatheredAmount);
      }

      /**
       * 入库详细登记
       */
      @Override
      public void updstock(Integer parentId, int gatheredAmount) {
            putRepertoryDetailsTyhMapper.updstock(parentId,gatheredAmount);
      }

      /**
       *获取应入库数量与成本
       */
      @Override
      public List<PutRepertoryDetails> getAmountAndSubtotal(Integer parent_id){
            QueryWrapper<PutRepertoryDetails> queryWrapper=new QueryWrapper<PutRepertoryDetails>().select("pid","parent_id","product_id","product_name","product_describe","amount","amount_unit","cost_price","subtotal","gathered_amount","gather_tag");
            queryWrapper.eq("parent_id",parent_id);
            return putRepertoryDetailsTyhMapper.selectList(queryWrapper);
      }

      /**
       *入库登记待复核
       */
      @Override
      public void updPutRepertory(Integer parentId, String loginAccount, String date) {
            putRepertoryDetailsTyhMapper.updPutRepertory(parentId,loginAccount,date);
      }

      /**
       * 入库详细登记待复核
       */
      @Override
      public void updPutRepertoryDetails(Integer parentId) {
            putRepertoryDetailsTyhMapper.updPutRepertoryDetails(parentId);
      }

      /**
       * 获取入库时间
       */
      @Override
      public PutRepertory getAttemperTime(Integer pid) {
            QueryWrapper<PutRepertory> queryWrapper=new QueryWrapper<>();
            queryWrapper.eq("id",pid);
            return putRepertoryTyhMapper.selectOne(queryWrapper);
      }

      /**
       * 入库调度
       */
      @Override
      public void WarehouschedulDetails(Integer id) {
            putRepertoryDetailsTyhMapper.WarehouschedulDetails(id);
      }
}
