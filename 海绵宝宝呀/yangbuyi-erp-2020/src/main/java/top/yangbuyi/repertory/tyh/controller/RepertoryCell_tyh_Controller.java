package top.yangbuyi.repertory.tyh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.tyh.domain.RepertoryCell;
import top.yangbuyi.repertory.tyh.service.ProductFile_tyh_Service;
import top.yangbuyi.repertory.tyh.service.RepertoryCell_tyh_Service;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@RestController
@RequestMapping("api/tanyuhao/RepertoryCell")
public class RepertoryCell_tyh_Controller {

      @Autowired
      private RepertoryCell_tyh_Service repertoryCellTyhService;
      @Autowired
      private ProductFile_tyh_Service productFileTyhService;

      /**
       * 制定安全库存配置单
       */
      @RequestMapping(value = "/makeconfig",method = RequestMethod.POST)
      public DataGridView makeconfig(RepertoryCell repertoryCell){

            String [] kind=repertoryCell.getFirstKindId().split("-");
            repertoryCell.setFirstKindId(kind[0]);
            repertoryCell.setSecondKindId(kind[1]);
            repertoryCell.setThirdKindId(kind[2]);

            String [] type=repertoryCell.getFirstKindName().split("-");
            repertoryCell.setFirstKindName(type[0]);
            repertoryCell.setSecondKindName(type[1]);
            repertoryCell.setThirdKindName(type[2]);

            repertoryCell.setCheckTag("S001-1");
            repertoryCell.setAmount(0.0);

            productFileTyhService.updMakeConfig(repertoryCell.getId());
            repertoryCellTyhService.addCell(repertoryCell);

            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("制定库存配置成功！");
            return dataGridView;
      }

      /**
       *查询需要审核配置单
       */
      @RequestMapping(value = "/getRepertoryCell",method = RequestMethod.GET)
      public DataGridView getRepertoryCell(RepertoryCell repertoryCell){
           List<RepertoryCell> list = repertoryCellTyhService.getRepertoryCell(repertoryCell);
           int count = list.size();
           long b = (int)count;
           return new DataGridView(b,list);
      }

      /**
       * 复核配置单
       */
      @RequestMapping(value = "/updRepertoryCell",method = RequestMethod.POST)
      public DataGridView updRepertoryCell(RepertoryCell repertoryCell){
            repertoryCellTyhService.updRepertoryCell(repertoryCell);
            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("库存配置审核完毕！");
            return dataGridView;
      }

      /**
       * 查询所有配置单
       */
      @RequestMapping(value = "/getRepertoryCellAll",method = RequestMethod.GET)
      public DataGridView getRepertoryCellAll(RepertoryCell repertoryCell){
            List<RepertoryCell> list = repertoryCellTyhService.getRepertoryCellAll(repertoryCell);
            int count = list.size();
            long b = (int)count;
            return new DataGridView(b,list);
      }

      /**
       * 变更配置单
       */
      @RequestMapping(value = "/changeRepertoryCell",method = RequestMethod.POST)
      public DataGridView changeRepertoryCell(RepertoryCell repertoryCell){
            repertoryCellTyhService.changeRepertoryCell(repertoryCell);
            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("库存配置变更成功！");
            return dataGridView;
      }

}
