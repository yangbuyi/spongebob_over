package top.yangbuyi.repertory.tyh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.yangbuyi.repertory.tyh.domain.RepertoryCell;

import java.util.List;

@Repository("repertoryCellMapper")
public interface RepertoryCell_tyh_Mapper extends BaseMapper<RepertoryCell> {

      /**
       * 复核配置单
       */
      public void updRepertoryCell(RepertoryCell repertoryCell);

      /**
       * 变更配置单
       */
      void changeRepertoryCell(RepertoryCell repertoryCell);

      /**
       * 仓库数量增加
       */
      public void addAmount(RepertoryCell repertoryCell);

}