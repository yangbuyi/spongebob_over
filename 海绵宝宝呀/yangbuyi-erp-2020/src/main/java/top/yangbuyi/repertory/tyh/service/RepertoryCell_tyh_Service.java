package top.yangbuyi.repertory.tyh.service;

import top.yangbuyi.repertory.tyh.domain.RepertoryCell;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface RepertoryCell_tyh_Service extends IService<RepertoryCell> {

      /**
       * 获取需要审核的安全库存配置单
       * */
      public List<RepertoryCell> getRepertoryCell(RepertoryCell repertoryCell);

      /**
       * 获取所有配置单
       * */
      public List<RepertoryCell> getRepertoryCellAll(RepertoryCell repertoryCell);

      /**
       * 制定安全库存配置单
       */
      public void addCell(RepertoryCell repertoryCell);

      /**
       * 复核配置单
       */
      public void updRepertoryCell(RepertoryCell repertoryCell);

      /**
       * 变更配置单
       */
      public void changeRepertoryCell(RepertoryCell repertoryCell);

      /**
       * 仓库数量增加
       */
      public void addAmount(RepertoryCell repertoryCell);

}

