package top.yangbuyi.repertory.liufan.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.liufan.service.ProduceMassageService_liufan;
import top.yangbuyi.repertory.liufan.mapper.ProduceMassage_liufan_Mapper;
import top.yangbuyi.repertory.liufan.domain.ProduceMassage;
import top.yangbuyi.repertory.liufan.domain.ProduceMassageExample;

@Service
public class ProduceMassageServiceLiufanImpl_liufan extends ServiceImpl<ProduceMassage_liufan_Mapper, ProduceMassage> implements ProduceMassageService_liufan {

    @Override
    public long countByExample(ProduceMassageExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(ProduceMassageExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<ProduceMassage> selectByExample(ProduceMassageExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(ProduceMassage record,ProduceMassageExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(ProduceMassage record,ProduceMassageExample example) {
        return baseMapper.updateByExample(record,example);
    }
}
