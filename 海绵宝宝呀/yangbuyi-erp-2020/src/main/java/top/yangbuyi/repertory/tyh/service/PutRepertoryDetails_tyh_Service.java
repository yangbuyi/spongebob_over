package top.yangbuyi.repertory.tyh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;
import top.yangbuyi.repertory.tyh.domain.PutRepertoryDetails;

import java.math.BigDecimal;
import java.util.List;

public interface PutRepertoryDetails_tyh_Service extends IService<PutRepertoryDetails>{

      /**
       * 获取入库产品物料详情
       */
      public List<PutRepertoryDetails> getDetailsListByPid(Integer parentId);

      /**
       * 入库详细登记
       */
      public void Warehousing(Integer parentId, BigDecimal subtotal, int gatheredAmount);

      /**
       * 入库登记
       */
      public void updstock(Integer parentId, int gatheredAmount);

      /**
       *获取应入库数量与成本
       */
      public List<PutRepertoryDetails> getAmountAndSubtotal(Integer parent_id);

      /**
       *入库登记待复核
       */
      public void updPutRepertory(Integer parentId, String loginAccount, String date);

      /**
       * 入库详细登记待复核
       */
      public void updPutRepertoryDetails(Integer parentId);

      /**
       * 获取入库时间
       */
      public PutRepertory getAttemperTime(Integer pid);

      /**
       *入库调度
       */
      public void WarehouschedulDetails(Integer id);

}
