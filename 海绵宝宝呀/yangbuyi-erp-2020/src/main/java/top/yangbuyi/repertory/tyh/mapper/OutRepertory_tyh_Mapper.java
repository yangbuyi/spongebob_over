package top.yangbuyi.repertory.tyh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.repertory.tyh.domain.OutRepertory;

import java.util.List;

public interface OutRepertory_tyh_Mapper extends BaseMapper<OutRepertory> {

      /**
       * 获取出库单
       */
      public List<OutRepertory> getOutRepertory();


      public void Warehouschedul(Integer id);

}