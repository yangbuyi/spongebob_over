package top.yangbuyi.repertory.liufan.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

@Data
@EqualsAndHashCode(callSuper = false)
public class ShengHeVo2 extends BaseVo {
    private String o1Storer; //申请出库人
    private String o1Reason; //出库理由
    private String o2ProductID;//产品编号
    private String o2ProductName;//产品名称
    private String o2ProductDescribe;//产品描述
    private int o2Amount;//产品数量
    private String o2AmountUnit;//单位
    private double o2CostPrice;//单价
    private double o2Subtotal;//总价
}
