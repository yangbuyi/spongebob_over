package top.yangbuyi.repertory.liufan.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetailsExample;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetails;
import com.baomidou.mybatisplus.extension.service.IService;
public interface OutRepertoryDetailsService_liufan extends IService<OutRepertoryDetails>{


    long countByExample(OutRepertoryDetailsExample example);

    int deleteByExample(OutRepertoryDetailsExample example);

    List<OutRepertoryDetails> selectByExample(OutRepertoryDetailsExample example);

    int updateByExampleSelective(OutRepertoryDetails record, OutRepertoryDetailsExample example);

    int updateByExample(OutRepertoryDetails record, OutRepertoryDetailsExample example);

    void addOutRepertoryDetails(OutRepertoryDetails o2);

    void updateKcAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    List<OutRepertoryDetails> ShenheByParentId(int id);

    void addKCAmount(@Param("productId") String productId, @Param("amount") Integer amount);

    void updateKcAmount2(@Param("parentId") Integer parentId, @Param("amount") Integer amount, @Param("subtotal") double subtotal);

    OutRepertoryDetails queryId(String productId);

    void updatePMTag(@Param("pmProduct") String pmProduct);
}
