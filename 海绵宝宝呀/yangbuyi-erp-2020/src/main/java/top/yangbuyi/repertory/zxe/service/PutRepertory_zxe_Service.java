package top.yangbuyi.repertory.zxe.service;

import top.yangbuyi.repertory.zxe.domain.PutRepertory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;

public interface PutRepertory_zxe_Service extends IService<PutRepertory>{
	PutRepertory PutRepertoryadd( PutRepertory PutRepertory);
	ArrayList PutRepertorychecker(String name);
 	Boolean PutRepertoryupdate(String check,Integer[] id,String gather);
	ArrayList<PutRepertory> PutRepertorychecker2(String check,String name);
}
