package top.yangbuyi.repertory.zxe.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.zxe.mapper.PutRepertoryDetails_zxe_Mapper;
import top.yangbuyi.repertory.zxe.mapper.PutRepertory_zxe_Mapper;
import top.yangbuyi.repertory.zxe.service.PutRepertoryDetails_zxe_Service;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Vo;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Votou;

import java.util.ArrayList;
import java.util.List;

@Service
public class PutRepertoryDetails_zxe_ServiceImpl implements PutRepertoryDetails_zxe_Service {
	@Autowired
	private PutRepertoryDetails_zxe_Mapper PutRepertoryDetails_zxe_Mapper;

	@Autowired
	private PutRepertory_zxe_Mapper PutRepertory_zxe_Mapper;

	@Override
	public Boolean insertPutRepertoryDetails(List<PutRepertoryDetailsRe_zxe_Vo> list) {
		return PutRepertoryDetails_zxe_Mapper.insertPutRepertoryDetails(list)>0?true:false;
	}

	@Override
	public ArrayList PutRepertoryDetailsid(Integer id) {
		return PutRepertoryDetails_zxe_Mapper.PutRepertoryDetailsid(id);
	}

	@Override
	public Integer deletePutRepertoryDetails(List<PutRepertoryDetailsRe_zxe_Votou> PutRepertoryDetailsRe_zxe_Votou) {
		Integer count=0;
		Integer pringcon=0;
			System.err.println(PutRepertoryDetailsRe_zxe_Votou.toString());
		for (PutRepertoryDetailsRe_zxe_Votou putRepertoryDetailsReZxeVotou2 : PutRepertoryDetailsRe_zxe_Votou
			 ) {
			count+=(Integer.valueOf(String.valueOf(putRepertoryDetailsReZxeVotou2.getAmount())));
			pringcon+=(Integer.valueOf(String.valueOf(putRepertoryDetailsReZxeVotou2.getCostPrice())));
		}
		System.err.println(count+","+pringcon);
			PutRepertoryDetails_zxe_Mapper.deletePutRepertoryDetails(PutRepertoryDetailsRe_zxe_Votou.get(0).getParentId());
			PutRepertory_zxe_Mapper.PutRepertoryupdateid(PutRepertoryDetailsRe_zxe_Votou.get(0).getParentId(), PutRepertoryDetailsRe_zxe_Votou.get(0).getReason(),count,pringcon);
		return PutRepertoryDetails_zxe_Mapper.insertPutRepertoryDetailsupd(PutRepertoryDetailsRe_zxe_Votou);
	}
}
