package top.yangbuyi.repertory.tyh.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "product_file")
public class ProductFile implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "factory_name")
    private String factoryName;

    @TableField(value = "first_kind_id")
    private String firstKindId;

    @TableField(value = "first_kind_name")
    private String firstKindName;

    @TableField(value = "second_kind_id")
    private String secondKindId;

    @TableField(value = "second_kind_name")
    private String secondKindName;

    @TableField(value = "third_kind_id")
    private String thirdKindId;

    @TableField(value = "third_kind_name")
    private String thirdKindName;

    @TableField(value = "product_nick")
    private String productNick;

    @TableField(value = "type")
    private String type;

    @TableField(value = "product_class")
    private String productClass;

    @TableField(value = "personal_unit")
    private String personalUnit;

    @TableField(value = "personal_value")
    private String personalValue;

    @TableField(value = "provider_group")
    private String providerGroup;

    @TableField(value = "warranty")
    private String warranty;

    @TableField(value = "twin_name")
    private String twinName;

    @TableField(value = "twin_id")
    private String twinId;

    @TableField(value = "lifecycle")
    private String lifecycle;

    @TableField(value = "list_price")
    private BigDecimal listPrice;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "real_cost_price")
    private BigDecimal realCostPrice;

    @TableField(value = "amount_unit")
    private String amountUnit;

    @TableField(value = "product_describe")
    private String productDescribe;

    @TableField(value = "responsible_person")
    private String responsiblePerson;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private String registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "changer")
    private String changer;

    @TableField(value = "change_time")
    private Date changeTime;

    @TableField(value = "change_tag")
    private String changeTag;

    @TableField(value = "price_change_tag")
    private String priceChangeTag;

    @TableField(value = "file_change_amount")
    private Integer fileChangeAmount;

    @TableField(value = "delete_tag")
    private String deleteTag;

    @TableField(value = "design_module_tag")
    private String designModuleTag;

    @TableField(value = "design_procedure_tag")
    private String designProcedureTag;

    @TableField(value = "design_cell_tag")
    private String designCellTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_FACTORY_NAME = "factory_name";

    public static final String COL_FIRST_KIND_ID = "first_kind_id";

    public static final String COL_FIRST_KIND_NAME = "first_kind_name";

    public static final String COL_SECOND_KIND_ID = "second_kind_id";

    public static final String COL_SECOND_KIND_NAME = "second_kind_name";

    public static final String COL_THIRD_KIND_ID = "third_kind_id";

    public static final String COL_THIRD_KIND_NAME = "third_kind_name";

    public static final String COL_PRODUCT_NICK = "product_nick";

    public static final String COL_TYPE = "type";

    public static final String COL_PRODUCT_CLASS = "product_class";

    public static final String COL_PERSONAL_UNIT = "personal_unit";

    public static final String COL_PERSONAL_VALUE = "personal_value";

    public static final String COL_PROVIDER_GROUP = "provider_group";

    public static final String COL_WARRANTY = "warranty";

    public static final String COL_TWIN_NAME = "twin_name";

    public static final String COL_TWIN_ID = "twin_id";

    public static final String COL_LIFECYCLE = "lifecycle";

    public static final String COL_LIST_PRICE = "list_price";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_REAL_COST_PRICE = "real_cost_price";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_RESPONSIBLE_PERSON = "responsible_person";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_CHANGER = "changer";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHANGE_TAG = "change_tag";

    public static final String COL_PRICE_CHANGE_TAG = "price_change_tag";

    public static final String COL_FILE_CHANGE_AMOUNT = "file_change_amount";

    public static final String COL_DELETE_TAG = "delete_tag";

    public static final String COL_DESIGN_MODULE_TAG = "design_module_tag";

    public static final String COL_DESIGN_PROCEDURE_TAG = "design_procedure_tag";

    public static final String COL_DESIGN_CELL_TAG = "design_cell_tag";

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return product_name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return factory_name
     */
    public String getFactoryName() {
        return factoryName;
    }

    /**
     * @param factoryName
     */
    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    /**
     * @return first_kind_id
     */
    public String getFirstKindId() {
        return firstKindId;
    }

    /**
     * @param firstKindId
     */
    public void setFirstKindId(String firstKindId) {
        this.firstKindId = firstKindId;
    }

    /**
     * @return first_kind_name
     */
    public String getFirstKindName() {
        return firstKindName;
    }

    /**
     * @param firstKindName
     */
    public void setFirstKindName(String firstKindName) {
        this.firstKindName = firstKindName;
    }

    /**
     * @return second_kind_id
     */
    public String getSecondKindId() {
        return secondKindId;
    }

    /**
     * @param secondKindId
     */
    public void setSecondKindId(String secondKindId) {
        this.secondKindId = secondKindId;
    }

    /**
     * @return second_kind_name
     */
    public String getSecondKindName() {
        return secondKindName;
    }

    /**
     * @param secondKindName
     */
    public void setSecondKindName(String secondKindName) {
        this.secondKindName = secondKindName;
    }

    /**
     * @return third_kind_id
     */
    public String getThirdKindId() {
        return thirdKindId;
    }

    /**
     * @param thirdKindId
     */
    public void setThirdKindId(String thirdKindId) {
        this.thirdKindId = thirdKindId;
    }

    /**
     * @return third_kind_name
     */
    public String getThirdKindName() {
        return thirdKindName;
    }

    /**
     * @param thirdKindName
     */
    public void setThirdKindName(String thirdKindName) {
        this.thirdKindName = thirdKindName;
    }

    /**
     * @return product_nick
     */
    public String getProductNick() {
        return productNick;
    }

    /**
     * @param productNick
     */
    public void setProductNick(String productNick) {
        this.productNick = productNick;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return product_class
     */
    public String getProductClass() {
        return productClass;
    }

    /**
     * @param productClass
     */
    public void setProductClass(String productClass) {
        this.productClass = productClass;
    }

    /**
     * @return personal_unit
     */
    public String getPersonalUnit() {
        return personalUnit;
    }

    /**
     * @param personalUnit
     */
    public void setPersonalUnit(String personalUnit) {
        this.personalUnit = personalUnit;
    }

    /**
     * @return personal_value
     */
    public String getPersonalValue() {
        return personalValue;
    }

    /**
     * @param personalValue
     */
    public void setPersonalValue(String personalValue) {
        this.personalValue = personalValue;
    }

    /**
     * @return provider_group
     */
    public String getProviderGroup() {
        return providerGroup;
    }

    /**
     * @param providerGroup
     */
    public void setProviderGroup(String providerGroup) {
        this.providerGroup = providerGroup;
    }

    /**
     * @return warranty
     */
    public String getWarranty() {
        return warranty;
    }

    /**
     * @param warranty
     */
    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    /**
     * @return twin_name
     */
    public String getTwinName() {
        return twinName;
    }

    /**
     * @param twinName
     */
    public void setTwinName(String twinName) {
        this.twinName = twinName;
    }

    /**
     * @return twin_id
     */
    public String getTwinId() {
        return twinId;
    }

    /**
     * @param twinId
     */
    public void setTwinId(String twinId) {
        this.twinId = twinId;
    }

    /**
     * @return lifecycle
     */
    public String getLifecycle() {
        return lifecycle;
    }

    /**
     * @param lifecycle
     */
    public void setLifecycle(String lifecycle) {
        this.lifecycle = lifecycle;
    }

    /**
     * @return list_price
     */
    public BigDecimal getListPrice() {
        return listPrice;
    }

    /**
     * @param listPrice
     */
    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    /**
     * @return cost_price
     */
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    /**
     * @param costPrice
     */
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return real_cost_price
     */
    public BigDecimal getRealCostPrice() {
        return realCostPrice;
    }

    /**
     * @param realCostPrice
     */
    public void setRealCostPrice(BigDecimal realCostPrice) {
        this.realCostPrice = realCostPrice;
    }

    /**
     * @return amount_unit
     */
    public String getAmountUnit() {
        return amountUnit;
    }

    /**
     * @param amountUnit
     */
    public void setAmountUnit(String amountUnit) {
        this.amountUnit = amountUnit;
    }

    /**
     * @return product_describe
     */
    public String getProductDescribe() {
        return productDescribe;
    }

    /**
     * @param productDescribe
     */
    public void setProductDescribe(String productDescribe) {
        this.productDescribe = productDescribe;
    }

    /**
     * @return responsible_person
     */
    public String getResponsiblePerson() {
        return responsiblePerson;
    }

    /**
     * @param responsiblePerson
     */
    public void setResponsiblePerson(String responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    /**
     * @return register
     */
    public String getRegister() {
        return register;
    }

    /**
     * @param register
     */
    public void setRegister(String register) {
        this.register = register;
    }

    /**
     * @return register_time
     */
    public String getRegisterTime() {
        return registerTime;
    }

    /**
     * @param registerTime
     */
    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * @return checker
     */
    public String getChecker() {
        return checker;
    }

    /**
     * @param checker
     */
    public void setChecker(String checker) {
        this.checker = checker;
    }

    /**
     * @return check_time
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * @param checkTime
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * @return check_tag
     */
    public String getCheckTag() {
        return checkTag;
    }

    /**
     * @param checkTag
     */
    public void setCheckTag(String checkTag) {
        this.checkTag = checkTag;
    }

    /**
     * @return changer
     */
    public String getChanger() {
        return changer;
    }

    /**
     * @param changer
     */
    public void setChanger(String changer) {
        this.changer = changer;
    }

    /**
     * @return change_time
     */
    public Date getChangeTime() {
        return changeTime;
    }

    /**
     * @param changeTime
     */
    public void setChangeTime(Date changeTime) {
        this.changeTime = changeTime;
    }

    /**
     * @return change_tag
     */
    public String getChangeTag() {
        return changeTag;
    }

    /**
     * @param changeTag
     */
    public void setChangeTag(String changeTag) {
        this.changeTag = changeTag;
    }

    /**
     * @return price_change_tag
     */
    public String getPriceChangeTag() {
        return priceChangeTag;
    }

    /**
     * @param priceChangeTag
     */
    public void setPriceChangeTag(String priceChangeTag) {
        this.priceChangeTag = priceChangeTag;
    }

    /**
     * @return file_change_amount
     */
    public Integer getFileChangeAmount() {
        return fileChangeAmount;
    }

    /**
     * @param fileChangeAmount
     */
    public void setFileChangeAmount(Integer fileChangeAmount) {
        this.fileChangeAmount = fileChangeAmount;
    }

    /**
     * @return delete_tag
     */
    public String getDeleteTag() {
        return deleteTag;
    }

    /**
     * @param deleteTag
     */
    public void setDeleteTag(String deleteTag) {
        this.deleteTag = deleteTag;
    }

    /**
     * @return design_module_tag
     */
    public String getDesignModuleTag() {
        return designModuleTag;
    }

    /**
     * @param designModuleTag
     */
    public void setDesignModuleTag(String designModuleTag) {
        this.designModuleTag = designModuleTag;
    }

    /**
     * @return design_procedure_tag
     */
    public String getDesignProcedureTag() {
        return designProcedureTag;
    }

    /**
     * @param designProcedureTag
     */
    public void setDesignProcedureTag(String designProcedureTag) {
        this.designProcedureTag = designProcedureTag;
    }

    /**
     * @return design_cell_tag
     */
    public String getDesignCellTag() {
        return designCellTag;
    }

    /**
     * @param designCellTag
     */
    public void setDesignCellTag(String designCellTag) {
        this.designCellTag = designCellTag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", factoryName=").append(factoryName);
        sb.append(", firstKindId=").append(firstKindId);
        sb.append(", firstKindName=").append(firstKindName);
        sb.append(", secondKindId=").append(secondKindId);
        sb.append(", secondKindName=").append(secondKindName);
        sb.append(", thirdKindId=").append(thirdKindId);
        sb.append(", thirdKindName=").append(thirdKindName);
        sb.append(", productNick=").append(productNick);
        sb.append(", type=").append(type);
        sb.append(", productClass=").append(productClass);
        sb.append(", personalUnit=").append(personalUnit);
        sb.append(", personalValue=").append(personalValue);
        sb.append(", providerGroup=").append(providerGroup);
        sb.append(", warranty=").append(warranty);
        sb.append(", twinName=").append(twinName);
        sb.append(", twinId=").append(twinId);
        sb.append(", lifecycle=").append(lifecycle);
        sb.append(", listPrice=").append(listPrice);
        sb.append(", costPrice=").append(costPrice);
        sb.append(", realCostPrice=").append(realCostPrice);
        sb.append(", amountUnit=").append(amountUnit);
        sb.append(", productDescribe=").append(productDescribe);
        sb.append(", responsiblePerson=").append(responsiblePerson);
        sb.append(", register=").append(register);
        sb.append(", registerTime=").append(registerTime);
        sb.append(", checker=").append(checker);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", checkTag=").append(checkTag);
        sb.append(", changer=").append(changer);
        sb.append(", changeTime=").append(changeTime);
        sb.append(", changeTag=").append(changeTag);
        sb.append(", priceChangeTag=").append(priceChangeTag);
        sb.append(", fileChangeAmount=").append(fileChangeAmount);
        sb.append(", deleteTag=").append(deleteTag);
        sb.append(", designModuleTag=").append(designModuleTag);
        sb.append(", designProcedureTag=").append(designProcedureTag);
        sb.append(", designCellTag=").append(designCellTag);
        sb.append("]");
        return sb.toString();
    }
}