package top.yangbuyi.repertory.tyh.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;
import top.yangbuyi.repertory.tyh.domain.PutRepertoryDetails;
import top.yangbuyi.repertory.tyh.domain.RepertoryCell;
import top.yangbuyi.repertory.tyh.service.PutRepertoryDetails_tyh_Service;
import top.yangbuyi.repertory.tyh.service.PutRepertory_tyh_Service;
import top.yangbuyi.repertory.tyh.service.RepertoryCell_tyh_Service;
import top.yangbuyi.system.common.ActiveUser;
import top.yangbuyi.system.common.DataGridView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/tanyuhao/PutRepertoryDetails")
public class PutRepertoryDetails_tyh_Controller {

      @Autowired
      private PutRepertoryDetails_tyh_Service putRepertoryDetailsTyhService;
      @Autowired
      private PutRepertory_tyh_Service putRepertoryTyhService;
      @Autowired
      private RepertoryCell_tyh_Service repertoryCellTyhService;

      /**
       * 获取入库产品物料详情
       * @param parentId
       * @return
       */
      @RequestMapping(value = "/getDetailsListByPid",method = RequestMethod.GET)
      public DataGridView getDetailsListByPid(@RequestParam("id") Integer parentId){
            List<PutRepertoryDetails> putRepertoryDetails= putRepertoryDetailsTyhService.getDetailsListByPid(parentId);
            System.out.println("----------------------:"+putRepertoryDetails);
            return new DataGridView(putRepertoryDetails);
      }

      @RequestMapping(value = "/Warehousing",method = RequestMethod.POST)
      public DataGridView Warehousing(@RequestParam("id") Integer parentId){
            ActiveUser loginAccount = (ActiveUser)SecurityUtils.getSubject().getPrincipal();
            SimpleDateFormat sdf = new SimpleDateFormat();// 格式化时间
            sdf.applyPattern("yyyy-MM-dd HH:mm:ss");// a为am/pm的标记
            Date date = new Date();// 获取当前时间
            List<PutRepertoryDetails> detailsList= putRepertoryDetailsTyhService.getAmountAndSubtotal(parentId);
            for (PutRepertoryDetails list:detailsList) {
                  putRepertoryDetailsTyhService.updPutRepertoryDetails(list.getPid());
            }
            putRepertoryDetailsTyhService.updPutRepertory(parentId,loginAccount.getUser().getLoginname(),sdf.format(date));
            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("登记完成(待审核)！");
            return dataGridView;
      }

      @RequestMapping(value = "/Toreview",method = RequestMethod.POST)
      public DataGridView Toreview(@RequestParam("id") Integer parentId){
            List<PutRepertoryDetails> detailsList= putRepertoryDetailsTyhService.getAmountAndSubtotal(parentId);
            int number =0;
            for (PutRepertoryDetails list:detailsList) {
                  number+=list.getAmount();
                  putRepertoryDetailsTyhService.Warehousing(list.getPid(),list.getSubtotal(),list.getAmount());
                  RepertoryCell repertoryCell=new RepertoryCell();
                  repertoryCell.setProductId(list.getProductId());
                  repertoryCell.setAmount(Double.parseDouble(String.valueOf(list.getAmount())));
                  repertoryCellTyhService.addAmount(repertoryCell);
            }
            putRepertoryDetailsTyhService.updstock(parentId,number);
            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("审核完毕,已入库");
            return dataGridView;
      }

      @RequestMapping(value = "/getAttemperTime",method = RequestMethod.GET)
      public DataGridView getAttemperTime(Integer pid){
            PutRepertory putRepertory= putRepertoryDetailsTyhService.getAttemperTime(pid);
            return new DataGridView(putRepertory);
      }

      /**
       * 入库调度
       * @return
       */
      @RequestMapping(value = "/Warehouschedul",method = RequestMethod.POST)
      public DataGridView Warehouschedul(Integer id){
            putRepertoryDetailsTyhService.WarehouschedulDetails(id);
            putRepertoryTyhService.Warehouschedul(id);
            DataGridView dataGridView=new DataGridView();
            dataGridView.setCode(200);
            dataGridView.setMsg("入库调度完成");
            return dataGridView;
      }

}
