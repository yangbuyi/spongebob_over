package top.yangbuyi.repertory.tyh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;

import java.util.List;

public interface PutRepertory_tyh_Service extends IService<PutRepertory>{

      /**
       * 获取入库登记待登记的产品
       * @return
       */
      public List<PutRepertory> getDispatch(PutRepertory putRepertory);

      /**
       * 获取已经入库的物料
       */
      public List<PutRepertory> getDispatchByStoreTag(PutRepertory putRepertory);

      /**
       * 获取入库登记复核产品
       */
      public List<PutRepertory> getToReview(PutRepertory putRepertory);

      /**
       * 获取待入库调度的产品
       */
      public List<PutRepertory> getWarehouschedul(PutRepertory putRepertory);

      public void Warehouschedul(Integer id);

}
