package top.yangbuyi.repertory.yinshengjie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertory;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.yinshengjie.service.OutRepertoryDetailsService_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.service.OutRepertoryService_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.vo.ShengHeVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;


/**
 * 出库登记
 */
@RestController
@RequestMapping("api/yingshengjie/OutBoundRegistration")
public class OutBoundRegistration_yinshengjie {

	@Autowired
	private OutRepertoryService_yinshengjie outRepertoryServiceYinshengjie;
	@Autowired
	private OutRepertoryDetailsService_yinshengjie detailsService;

	@RequestMapping("/findPayId")
	public DataGridView findPayid(OutRepertory outRepertory){
		DataGridView dataGridView = new DataGridView();
		List<ShengHeVo> shengHeVos = outRepertoryServiceYinshengjie.FindPayId(outRepertory);
		dataGridView.setCount((long) shengHeVos.size());
		dataGridView.setData(shengHeVos);
		return dataGridView;
	}

	/**
	 * 修改状态
	 * @param
	 * @return
	 */
	@RequestMapping("edit")
	public DataGridView editPayTag(OutRepertory outRepertory){
		DataGridView dataGridView = new DataGridView();
		System.out.println("-----------------------"+outRepertory);
		int edit = outRepertoryServiceYinshengjie.edit(outRepertory);
		OutRepertoryDetails outRepertoryDetails = new OutRepertoryDetails();

		outRepertoryDetails.setParentId(outRepertory.getId());
		detailsService.edit(outRepertoryDetails);
		dataGridView.setData(edit);
		dataGridView.setMsg("登记成功");
		return  dataGridView;
	}


}
