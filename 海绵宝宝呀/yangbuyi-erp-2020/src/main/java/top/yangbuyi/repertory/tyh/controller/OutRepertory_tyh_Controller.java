package top.yangbuyi.repertory.tyh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.tyh.domain.OutRepertory;
import top.yangbuyi.repertory.tyh.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.tyh.service.OutRepertory_tyh_DetailsService;
import top.yangbuyi.repertory.tyh.service.OutRepertory_tyh_Service;
import top.yangbuyi.system.common.DataGridView;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("api/tanyuhao/OutRepertory")
public class OutRepertory_tyh_Controller {

      @Autowired
      private OutRepertory_tyh_DetailsService outRepertoryTyhDetailsService;
      @Autowired
      private OutRepertory_tyh_Service outRepertoryTyhService;

      /**
       * 获取需要制定产品出库单列表
       */
      @RequestMapping(value = "getOutRepertory",method = RequestMethod.GET)
      public DataGridView getOutRepertory(){
            List<OutRepertory> outRepertoryList = outRepertoryTyhService.getOutRepertory();
            Double number=0.0;
            Double money=0.0;
            for (OutRepertory out:outRepertoryList ) {
                  for (OutRepertoryDetails deta:out.getOutRepertoryDetails()) {
                        number+=Double.parseDouble(String.valueOf(deta.getAmount()));
                        money+=Double.parseDouble(String.valueOf(deta.getCostPrice()))*Double.parseDouble(String.valueOf(deta.getAmount()));
                  }
                  out.setCostPriceSum(BigDecimal.valueOf(money));
                  out.setAmountSum(BigDecimal.valueOf(number));
                  number=0.0;
                  money=0.0;
            }
            return new DataGridView(outRepertoryList);
      }

}
