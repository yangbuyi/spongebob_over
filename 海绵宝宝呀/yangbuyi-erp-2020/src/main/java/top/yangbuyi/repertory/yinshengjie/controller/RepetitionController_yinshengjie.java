package top.yangbuyi.repertory.yinshengjie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertory;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.yinshengjie.service.OutRepertoryDetailsService_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.service.OutRepertoryService_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.vo.ShengHeVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;


/**
 * 出库登记复查
 */
@RestController
@RequestMapping("api/yingshengjie/RepetitionController")
public class RepetitionController_yinshengjie {

	@Autowired
	private OutRepertoryService_yinshengjie outRepertoryServiceYinshengjie;
	@Autowired
	private OutRepertoryDetailsService_yinshengjie detailsService;

	@RequestMapping("/repetition")
	public DataGridView repetition(OutRepertory outRepertory){
		DataGridView dataGridView = new DataGridView();
		List<ShengHeVo> repetition = outRepertoryServiceYinshengjie.repetition(outRepertory);
		dataGridView.setData(repetition);
		dataGridView.setCount((long) repetition.size());
		return  dataGridView;
	}

	/**
	 * 复核出库
	 * @param outRepertory
	 * @return
	 */
	@RequestMapping("/repetitionEdit")
	public DataGridView repetitionEdit(OutRepertory outRepertory){
		DataGridView dataGridView = new DataGridView();
		//修改出库表状态
		int i = outRepertoryServiceYinshengjie.repetitionEdit(outRepertory);
		OutRepertoryDetails outRepertoryDetails = new OutRepertoryDetails();
		outRepertoryDetails.setParentId(outRepertory.getId());
		//修改出库详细表状态
		detailsService.repetitionEdit(outRepertoryDetails);
		dataGridView.setData(i);
		dataGridView.setMsg("复核出库修改成功");
		return  dataGridView;
	}

	/**
	 * 查询已出库
	 * @param outRepertory
	 * @return
	 */
	@RequestMapping("/delivery")
	public DataGridView delivery(OutRepertory outRepertory){
		DataGridView dataGridView = new DataGridView();
		List<ShengHeVo> delivery = outRepertoryServiceYinshengjie.delivery(outRepertory);
		dataGridView.setData(delivery);
		dataGridView.setCount((long) delivery.size());
		return  dataGridView;
	}
}
