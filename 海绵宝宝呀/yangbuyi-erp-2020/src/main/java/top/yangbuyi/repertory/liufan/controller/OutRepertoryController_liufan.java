package top.yangbuyi.repertory.liufan.controller;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.liufan.domain.OutRepertory;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.liufan.service.OutRepertoryDetailsService_liufan;
import top.yangbuyi.repertory.liufan.service.OutRepertoryService_liufan;
import top.yangbuyi.repertory.liufan.service.ProduceMassageService_liufan;
import top.yangbuyi.repertory.liufan.vo.ShengHeVo;
import top.yangbuyi.repertory.liufan.vo.repertoryCellVo;
import top.yangbuyi.system.common.DataGridView;
import top.yangbuyi.system.common.ResultObj;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping({"/api/liufan/outrepertory"})
public class OutRepertoryController_liufan {
    @Autowired
    private OutRepertoryService_liufan outRepertoryServiceLiufan;
    @Autowired
    private OutRepertoryDetailsService_liufan outRepertoryDetailsServiceLiufan;
    @Autowired
    private ProduceMassageService_liufan produceMassageServiceLiufan;

    public OutRepertoryController_liufan() {
    }

    /**
     * 查询需要出库的产品
     * @return
     */
    @RequestMapping({"selectBygatherTag"})
    public Object selectBygatherTag() {
        DataGridView dataGridView = this.outRepertoryServiceLiufan.selectBygatherTag();
        System.out.println("dataGridView  -------------"+dataGridView);
        return dataGridView;
    }

    /**
     * 向出库表和出库详细表添加数据并修改库存数量
     * @param storer 申请人
     * @param reason 申请理由
     * @param remark 申请备注
     * @param OutDetails 产品信息
     * @return
     */
    //添加出库登记
    @ResponseBody
    @RequestMapping("addOutRepertoryShenqing")
//    public Object addOutRepertoryShenqing(@RequestBody List<OutRepertoryDetails>  OutDetails, @RequestBody OutRepertoryVo outRepertoryVo){
    public Object addOutRepertoryShenqing(
            String storer,
            String reason,
            String remark,
            @RequestBody List<repertoryCellVo>  OutDetails){
        System.out.println("2：-------------------------"+OutDetails);
        System.out.println(storer);
        System.out.println(reason);
        System.out.println(remark);
        OutRepertory o1=new OutRepertory();

//        出库编号 pay_id
        SimpleDateFormat sf=new SimpleDateFormat("yyyyMMdd");
        String date=sf.format(new Date());
        String weishu="";
        Random ran=new Random();
        for(int i=0;i<4;i++) {
            weishu+=ran.nextInt(10);
        }
        String payId="ck"+date+weishu;

        o1.setPayId(payId);

//        storer

        o1.setStorer(storer);
////        reason
        o1.setReason(reason);
////        remark
        o1.setRemark(remark);
////        register
        o1.setRegister(storer);
//        register_time
        Date date1=new Date();
        o1.setRegisterTime(date1);
//        check_tag 等待审核 0 通过审核1 不通过 2
        String checkTag="S001-0";
        o1.setCheckTag(checkTag);
//        attemper
        o1.setAttemper(storer);
//        attemper_time
        o1.setAttemperTime(date1);
//        store_tag
        String storeTge="K002-0";
        o1.setStoreTag(storeTge);

        //向出库表添加数据
        outRepertoryServiceLiufan.addOutRepertory(o1);


        OutRepertoryDetails o2=new OutRepertoryDetails();

        //根据出库编号 查询 出库表的id
        OutRepertory or= outRepertoryServiceLiufan.selectByPayId(payId);
        System.out.println("orid:  "+or.getId());



        for (repertoryCellVo rc: OutDetails) {
            //        parent_id  与出库表关联的id
            o2.setParentId(or.getId());
            //        product_id  产品编号
            o2.setProductId(rc.getRcProductId());
//        product_name 产品名称
            o2.setProductName(rc.getRcProductName());
//        product_describe 产品描述
            o2.setProductDescribe(rc.getRcConfing());
//        amount 产品数量
            o2.setAmount(rc.getRcAmount());
//        amount_unit 产品单位
            o2.setAmountUnit(rc.getRcAmountUnit());
//        cost_price  产品单价
            o2.setCostPrice(rc.getPrdCostPrice());
//        subtotal  产品总价
            o2.setSubtotal(rc.getRcAmount()*rc.getPrdCostPrice());
//        paid_amount 确定数量
            o2.setPaidAmount(rc.getRcAmount());
//        pay_tag  状态
            String payTge="K002-0";
            o2.setPayTag(payTge);

            //向出库详细表添加数据
            outRepertoryDetailsServiceLiufan.addOutRepertoryDetails(o2);
            //修改库存数量
           // outRepertoryDetailsServiceLiufan.updateKcAmount(rc.getRcProductId(),rc.getRcAmount());
            //修改产品表的状态
//            outRepertoryDetailsServiceLiufan.updatePMTag(od.getPmProduct());
//
        }

        return ResultObj.ADD_SUCCESS;
    }


    /**
     * 查询等待审核的出库单
     * @param sh
     * @return
     */
    @RequestMapping({"selectShengHe"})
    public Object selectShengHe(ShengHeVo sh) {
        DataGridView dataGridView = this.outRepertoryServiceLiufan.selectShengHe(sh);
        return dataGridView;
    }

    /**
     * 查询等待审核的出库单的详情 根据编号
     * @param o1Id 出库编号
     * @return
     */
    @RequestMapping({"selectShengHe2"})
    public Object selectShengHe2(Integer o1Id) {
        DataGridView dataGridView = this.outRepertoryServiceLiufan.selectShengHe2(o1Id);
        return dataGridView;
    }

    /**
     * 通过审核
     * @param ids
     * @return
     */
    @RequestMapping({"ShenHePass"})
    public Object ShenHePass(@Param("ids") Integer[] ids) {
        Integer[] var2 = ids;
        int var3 = ids.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            int id = var2[var4];
            System.out.println("id：  " + id);
            this.outRepertoryServiceLiufan.ShenHePass(id);
        }

        return ResultObj.UPDATE_SUCCESS;
    }


    /**
     * 未通过审核
     * @param ids
     * @return
     */
    @RequestMapping({"ShenHeNoPass"})
    public Object ShenHeNoPass(@Param("ids") Integer[] ids) {
        Integer[] var2 = ids;
        int var3 = ids.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            int id = var2[var4];
            System.out.println("id：  " + id);
            this.outRepertoryServiceLiufan.ShenHeNoPass(id);
            
            
            // 不通过审核
           /* List<OutRepertoryDetails> od = this.outRepertoryDetailsServiceLiufan.ShenheByParentId(id);
            System.out.println("od:   " + od);
            for (OutRepertoryDetails opd:od) {
                this.outRepertoryDetailsServiceLiufan.addKCAmount(opd.getProductId(), opd.getAmount());
            }*/

        }

        return ResultObj.UPDATE_SUCCESS;
    }

    /**
     * 查询审核完的数据
     * @param shvo
     * @return
     */
    @RequestMapping({"QueryShengHe"})
    public Object QueryShengHe(ShengHeVo shvo) {
        System.out.println("shvo:          "+shvo);
        DataGridView dataGridView= outRepertoryServiceLiufan.QueryShengHe(shvo);
        return dataGridView;
    }

    /**
     * 查询需要出库的产品
     * @return
     */
    @RequestMapping({"QueryProducemassage"})
    public Object QueryProducemassage(){
        DataGridView dataGridView= outRepertoryServiceLiufan.QueryProducemassage();
        return dataGridView;
    }
    
    @RequestMapping({"QueryRepertoryCell"})
    public Object QueryRepertoryCell(){
        DataGridView dataGridView=outRepertoryServiceLiufan.QueryRepertoryCell();
        return dataGridView;
    }
    
}
