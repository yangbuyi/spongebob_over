package top.yangbuyi.repertory.zxe.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ToString
@TableName(value = "repertory_cell")
public class RepertoryCell {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "store_id")
    private String storeId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "first_kind_id")
    private String firstKindId;

    @TableField(value = "first_kind_name")
    private String firstKindName;

    @TableField(value = "second_kind_id")
    private String secondKindId;

    @TableField(value = "second_kind_name")
    private String secondKindName;

    @TableField(value = "third_kind_id")
    private String thirdKindId;

    @TableField(value = "third_kind_name")
    private String thirdKindName;

    @TableField(value = "min_amount")
    private BigDecimal minAmount;

    @TableField(value = "max_amount")
    private BigDecimal maxAmount;

    @TableField(value = "max_capacity_amount")
    private BigDecimal maxCapacityAmount;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "config")
    private String config;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "store_name")
    private String storeName;

    @TableField(value = "storage_unit_shorted")
    private String storageUnitShorted;

    @TableField(value = "amount_unit")
    private String amountUnit;
}