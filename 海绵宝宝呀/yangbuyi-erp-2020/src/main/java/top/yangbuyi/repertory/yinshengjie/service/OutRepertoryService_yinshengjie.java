package top.yangbuyi.repertory.yinshengjie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertory;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryExample;
import top.yangbuyi.repertory.yinshengjie.vo.ShengHeVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

public interface OutRepertoryService_yinshengjie extends IService<OutRepertory>{

    long countByExample(OutRepertoryExample example);

    int deleteByExample(OutRepertoryExample example);

    List<OutRepertory> selectByExample(OutRepertoryExample example);

    int updateByExampleSelective(OutRepertory record, OutRepertoryExample example);

    int updateByExample(OutRepertory record, OutRepertoryExample example);

    DataGridView selectBygatherTag();

    void addOutRepertory(OutRepertory o1);

    OutRepertory selectByPayId(String payId);

    DataGridView selectShengHe();

    DataGridView selectShengHe2(@Param("o1Id") Integer o1Id);


    void ShenHePass(int id);

    void ShenHeNoPass(int id);

    DataGridView QueryShengHe();

	/**
	 * 我的方法 高级查询
	 * @param outRepertory
	 * @return
	 */
	List<ShengHeVo> FindPayId(OutRepertory outRepertory);

	/**
	 * 修改出库 复查
	 * @param outRepertory
	 * @return
	 */
	int edit(OutRepertory outRepertory);

	/**
	 * 复核出库 查询
	 * @param outRepertory
	 * @return
	 */
	List<ShengHeVo> repetition(OutRepertory outRepertory);

	/**
	 * 修改复核出库状态
	 * @param outRepertory
	 * @return
	 */
	int repetitionEdit(OutRepertory outRepertory);

	/**
	 * 查询已经出库
	 * @param outRepertory
	 * @return
	 */
	List<ShengHeVo> delivery(@Param("outRepertory") OutRepertory outRepertory);
}
