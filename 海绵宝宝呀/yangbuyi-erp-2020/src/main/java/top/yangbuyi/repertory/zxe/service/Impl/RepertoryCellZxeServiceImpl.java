package top.yangbuyi.repertory.zxe.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.zxe.mapper.RepertoryCell_zxe_Mapper;
import top.yangbuyi.repertory.zxe.service.RepertoryCell_zxe_Service;

import java.util.ArrayList;
@Service
public class RepertoryCellZxeServiceImpl implements RepertoryCell_zxe_Service {
	@Autowired
	private RepertoryCell_zxe_Mapper RepertoryCell_zxe_Mapper;

	@Override
	public ArrayList getRepertroyCellList() {
		return RepertoryCell_zxe_Mapper.getRepertroyCellList();
	}
}
