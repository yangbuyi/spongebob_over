package top.yangbuyi.repertory.liufan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.liufan.domain.OutRepertory;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryExample;
import top.yangbuyi.repertory.liufan.mapper.OutRepertory_liufan_Mapper;
import top.yangbuyi.repertory.liufan.service.OutRepertoryService_liufan;
import top.yangbuyi.repertory.liufan.vo.ShengHeVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@Service
public class OutRepertoryServiceLiufanImpl_liufan extends ServiceImpl<OutRepertory_liufan_Mapper, OutRepertory> implements OutRepertoryService_liufan {

    @Override
    public long countByExample(OutRepertoryExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(OutRepertoryExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<OutRepertory> selectByExample(OutRepertoryExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(OutRepertory record,OutRepertoryExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(OutRepertory record,OutRepertoryExample example) {
        return baseMapper.updateByExample(record,example);
    }

    @Override
    public DataGridView selectBygatherTag() {
        return new DataGridView(baseMapper.selectBygatherTag());
    }

    @Override
    public void addOutRepertory(OutRepertory o1) {
        baseMapper.addOutRepertory(o1);
    }

    @Override
    public OutRepertory selectByPayId(String payId) {
        return baseMapper.selectByPayId(payId);
    }

    @Override
    public DataGridView selectShengHe(ShengHeVo sh) {
        return new DataGridView(baseMapper.selectShengHe(sh));
    }

    @Override
    public DataGridView selectShengHe2(Integer o1Id) {
        return new DataGridView(baseMapper.selectShengHe2(o1Id));
    }

    @Override
    public void ShenHePass(int id) {
        baseMapper.ShenHePass(id);
    }

    @Override
    public void ShenHeNoPass(int id) {
        baseMapper.ShenHeNoPass(id);
    }

    @Override
    public DataGridView QueryShengHe(@Param("shvo") ShengHeVo shvo) {
        return new DataGridView(baseMapper.QueryShengHe(shvo));
    }

    @Override
    public DataGridView QueryProducemassage() {
        return new DataGridView(baseMapper.QueryProducemassage());
    }
    
    @Override
    public DataGridView QueryRepertoryCell() {
        return new DataGridView(baseMapper.QueryRepertoryCell());
    }
}
