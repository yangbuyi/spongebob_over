package top.yangbuyi.repertory.tyh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.repertory.tyh.domain.PutRepertoryDetails;

import java.math.BigDecimal;
import java.util.List;

public interface PutRepertoryDetails_tyh_Mapper extends BaseMapper<PutRepertoryDetails> {

      /**
       * 获取入库产品物料详情
       */
      public List<PutRepertoryDetails> getDetailsListByPid(Integer parentId);

      /**
       * 入库详细登记复核
       */
      public void Warehousing(Integer pid, BigDecimal subtotal, int gatheredAmount);

      /**
       * 入库登记复核
       */
      public void updstock(Integer parentId, int gatheredAmount);

      /**
       *入库登记待复核
       */
      public void updPutRepertory(Integer parentId, String loginAccount, String date);

      /**
       * 入库详细登记待复核
       */
      public void updPutRepertoryDetails(Integer parentId);

      /**
       *入库调度
       */
      public void WarehouschedulDetails(Integer id);

}