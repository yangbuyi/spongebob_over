package top.yangbuyi.repertory.tyh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.tyh.domain.PutRepertory;
import top.yangbuyi.repertory.tyh.mapper.PutRepertory_tyh_Mapper;
import top.yangbuyi.repertory.tyh.service.PutRepertory_tyh_Service;

import java.util.List;

@Service
public class PutRepertoryTyhServiceImpl extends ServiceImpl<PutRepertory_tyh_Mapper, PutRepertory> implements PutRepertory_tyh_Service {

      @Autowired
      private PutRepertory_tyh_Mapper putRepertoryTyhMapper;

      /**
       * 获取待入库调度的产品
       */
      @Override
      public List<PutRepertory> getWarehouschedul(PutRepertory putRepertory){
            QueryWrapper<PutRepertory> queryWrapper=new QueryWrapper<>();
            if (putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-2")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId());
            }else if(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-2")
                        .eq("check_tag","S001-1")
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else if((putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!="")&&(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!="")){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-2")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId())
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else{
                  queryWrapper .orderByDesc("id")
                        .eq("check_tag","S001-1")
                        .eq("store_tag","K002-2");
            }
            return putRepertoryTyhMapper.selectList(queryWrapper);
      }



      /**
       * 获取入库登记以调度待登记的产品
       * @return
       */
      @Override
      public List<PutRepertory> getDispatch(PutRepertory putRepertory){
           QueryWrapper<PutRepertory> queryWrapper=new QueryWrapper<>();

            if (putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-3")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId());
            }else if(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-3")
                        .eq("check_tag","S001-1")
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else if((putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!="")&&(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!="")){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-3")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId())
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else{
                  queryWrapper .orderByDesc("id")
                        .eq("check_tag","S001-1")
                        .eq("store_tag","K002-3");
            }
            return putRepertoryTyhMapper.selectList(queryWrapper);
      }

      /**
       * 获取已经入库的物料
       */
      @Override
      public List<PutRepertory> getDispatchByStoreTag(PutRepertory putRepertory) {
            QueryWrapper<PutRepertory> queryWrapper=new QueryWrapper<>();
            if (putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-5")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId());
            }else if(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-5")
                        .eq("check_tag","S001-1")
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else if((putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!="")&&(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!="")){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-5")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId())
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else{
                  queryWrapper .orderByDesc("id")
                        .eq("check_tag","S001-1")
                        .eq("store_tag","K002-5");
            }
            return putRepertoryTyhMapper.selectList(queryWrapper);
      }

      @Override
      public List<PutRepertory> getToReview(PutRepertory putRepertory) {
            QueryWrapper<PutRepertory> queryWrapper=new QueryWrapper<>();
            if (putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-4")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId());
            }else if(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!=""){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-4")
                        .eq("check_tag","S001-1")
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else if((putRepertory.getGatherId()!=null&&putRepertory.getGatherId()!="")&&(putRepertory.getRegisterTime()!=null&&putRepertory.getRegisterTime()!="")){
                  queryWrapper.orderByDesc("id")
                        .eq("store_tag","K002-4")
                        .eq("check_tag","S001-1")
                        .eq("gather_id",putRepertory.getGatherId())
                        .likeRight("register_time",putRepertory.getRegisterTime());
            }else{
                  queryWrapper .orderByDesc("id")
                        .eq("check_tag","S001-1")
                        .eq("store_tag","K002-4");
            }
            return putRepertoryTyhMapper.selectList(queryWrapper);
      }

      @Override
      public void Warehouschedul(Integer id) {
            putRepertoryTyhMapper.Warehouschedul(id);
      }
}
