package top.yangbuyi.repertory.yinshengjie.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "out_repertory_details")
public class OutRepertoryDetails implements Serializable {
    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 父级序号 出库编号
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 产品编号
     */
    @TableField(value = "product_id")
    private String productId;

    /**
     * 产品名称
     */
    @TableField(value = "product_name")
    private String productName;

    /**
     * 描述
     */
    @TableField(value = "product_describe")
    private String productDescribe;

    /**
     * 数量
     */
    @TableField(value = "amount")
    private Integer amount;

    /**
     * 单位
     */
    @TableField(value = "amount_unit")
    private String amountUnit;

    /**
     * 单价
     */
    @TableField(value = "cost_price")
    private double costPrice;

    /**
     * 小计
     */
    @TableField(value = "subtotal")
    private double subtotal;

    /**
     * 确认入库件数
     */
    @TableField(value = "paid_amount")
    private Integer paidAmount;

    /**
     * 入库标志
     * K002-1: 已登记
     * K002-2: 已调度
     */
    @TableField(value = "pay_tag")
    private String payTag;



    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_PAID_AMOUNT = "paid_amount";

    public static final String COL_PAY_TAG = "pay_tag";

}
