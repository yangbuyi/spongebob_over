package top.yangbuyi.repertory.liufan.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "out_repertory")
public class OutRepertory implements Serializable {
    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 出库单号
     */
    @TableField(value = "pay_id")
    private String payId;

    /**
     * 出库人
     */
    @TableField(value = "storer")
    private String storer;

    /**
     * 出库理由
     */
    @TableField(value = "reason")
    private String reason;

    /**
     * 出库详情理由
     */
    @TableField(value = "reasonexact")
    private String reasonexact;

    /**
     * 总件数
     */
    @TableField(value = "amount_sum")
    private BigDecimal amountSum;

    /**
     * 总金额
     */
    @TableField(value = "cost_price_sum")
    private BigDecimal costPriceSum;

    /**
     * 确认出库总件数
     */
    @TableField(value = "paid_amount_sum")
    private BigDecimal paidAmountSum;

    /**
     *备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 登记人
     */
    @TableField(value = "register")
    private String register;

    /**
     * 登记时间
     */
    @TableField(value = "register_time")
    private Date registerTime;

    /**
     * 复核人
     */
    @TableField(value = "checker")
    private String checker;

    /**
     * 复核时间
     */
    @TableField(value = "check_time")
    private Date checkTime;

    /**
     * 审核状态 S001-0 等待审核  S001-1 审核通过  S001-2 审核不通过
     */
    @TableField(value = "check_tag")
    private String checkTag;

    /**
     * 调度人
     */
    @TableField(value = "attemper")
    private String attemper;

    /**
     * 库存标志 K002-1: 已登记  K002-2: 已调度
     */
    @TableField(value = "attemper_time")
    private Date attemperTime;

    /**
     * 序号
     */
    @TableField(value = "store_tag")
    private String storeTag;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PAY_ID = "pay_id";

    public static final String COL_STORER = "storer";

    public static final String COL_REASON = "reason";

    public static final String COL_REASONEXACT = "reasonexact";

    public static final String COL_AMOUNT_SUM = "amount_sum";

    public static final String COL_COST_PRICE_SUM = "cost_price_sum";

    public static final String COL_PAID_AMOUNT_SUM = "paid_amount_sum";

    public static final String COL_REMARK = "remark";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_ATTEMPER = "attemper";

    public static final String COL_ATTEMPER_TIME = "attemper_time";

    public static final String COL_STORE_TAG = "store_tag";
}
