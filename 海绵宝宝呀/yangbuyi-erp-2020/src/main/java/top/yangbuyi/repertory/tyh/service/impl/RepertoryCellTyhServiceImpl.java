package top.yangbuyi.repertory.tyh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.tyh.domain.RepertoryCell;
import top.yangbuyi.repertory.tyh.mapper.RepertoryCell_tyh_Mapper;
import top.yangbuyi.repertory.tyh.service.RepertoryCell_tyh_Service;

@Service
public class RepertoryCellTyhServiceImpl extends ServiceImpl<RepertoryCell_tyh_Mapper, RepertoryCell> implements RepertoryCell_tyh_Service {

    @Autowired
    private RepertoryCell_tyh_Mapper repertoryCellTyhMapper;

    /**
     * 获取需要审核的安全库存配置单
     */
    @Override
    public List<RepertoryCell> getRepertoryCell(RepertoryCell repertoryCell) {
        QueryWrapper<RepertoryCell> queryWrapper = new QueryWrapper<>();
          if (repertoryCell.getProductName()!=null&&repertoryCell.getProductName()!=""){
                queryWrapper.orderByDesc("id")
                      .eq("check_tag","S001-1")
                      .eq("product_name",repertoryCell.getProductName());
          }else{
                queryWrapper.orderByDesc("id")
                      .eq("check_tag","S001-1");
          }
        return repertoryCellTyhMapper.selectList(queryWrapper);
    }

      /**
       * 获取所有安全库存配置单
       */
    @Override
    public List<RepertoryCell> getRepertoryCellAll(RepertoryCell repertoryCell) {
          QueryWrapper<RepertoryCell> queryWrapper = new QueryWrapper<>();
          if (repertoryCell.getProductName()!=null&&repertoryCell.getProductName()!=""){
                queryWrapper.orderByDesc("id")
                      .in("check_tag","S001-1","S001-2","S001-3")
                      .eq("product_name",repertoryCell.getProductName());
          }else{
                queryWrapper.orderByDesc("id")
                      .in("check_tag","S001-1","S001-2","S001-3");
          }
          return repertoryCellTyhMapper.selectList(queryWrapper);
    }

      /**
     * 制定安全库存配置单
     */
      @Override
    public void addCell(RepertoryCell repertoryCell) {
        repertoryCellTyhMapper.insert(repertoryCell);
    }

      /**
       * 复核配置单
       */
      @Override
      public void updRepertoryCell(RepertoryCell repertoryCell){
            repertoryCellTyhMapper.updRepertoryCell(repertoryCell);
      }

      /**
       * 变更配置单
       */
      @Override
      public void changeRepertoryCell(RepertoryCell repertoryCell){
            repertoryCellTyhMapper.changeRepertoryCell(repertoryCell);
      }

      /**
       * 仓库数量增加
       */
      @Override
      public void addAmount(RepertoryCell repertoryCell ){
            repertoryCellTyhMapper.addAmount(repertoryCell);
      }

}

