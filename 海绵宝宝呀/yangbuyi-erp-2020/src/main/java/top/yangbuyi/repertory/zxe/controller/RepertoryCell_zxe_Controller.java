package top.yangbuyi.repertory.zxe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.zxe.service.RepertoryCell_zxe_Service;
import top.yangbuyi.system.common.DataGridView;

@RestController
@RequestMapping("/api/zhangxinge/RepertoryCell")
public class RepertoryCell_zxe_Controller {
	@Autowired
	private RepertoryCell_zxe_Service RepertoryCell_zxe_Service;

	@RequestMapping("/getRepertroyCellList")
	public Object getRepertroyCellList(){
		return new DataGridView(RepertoryCell_zxe_Service.getRepertroyCellList());
	}

}
