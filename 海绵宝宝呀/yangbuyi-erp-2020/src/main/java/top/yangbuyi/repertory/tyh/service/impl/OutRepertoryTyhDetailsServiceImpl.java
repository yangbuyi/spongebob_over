package top.yangbuyi.repertory.tyh.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.yangbuyi.repertory.tyh.mapper.OutRepertoryDetails_tyh_Mapper;
import top.yangbuyi.repertory.tyh.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.tyh.service.OutRepertory_tyh_DetailsService;
@Service
public class OutRepertoryTyhDetailsServiceImpl extends ServiceImpl<OutRepertoryDetails_tyh_Mapper, OutRepertoryDetails> implements OutRepertory_tyh_DetailsService {

      @Autowired
      private OutRepertoryDetails_tyh_Mapper outRepertoryDetailsTyhMapper;

      /**
       * 获取入库产品物料详情
       */
      @Override
      public List<OutRepertoryDetails> getDetailsListByPid(Integer parentId){
            return outRepertoryDetailsTyhMapper.getDetailsListByPid(parentId);
      }

      /**
       * 入库调度
       */
      @Override
      public void WarehouschedulDetails(Integer id) {
            outRepertoryDetailsTyhMapper.WarehouschedulDetails(id);
      }

}
