package top.yangbuyi.repertory.yinshengjie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertory;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryExample;
import top.yangbuyi.repertory.yinshengjie.mapper.OutRepertoryMapper_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.service.OutRepertoryService_yinshengjie;
import top.yangbuyi.repertory.yinshengjie.vo.ShengHeVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@Service
public class OutRepertoryServiceImpl_yinshengjie extends ServiceImpl<OutRepertoryMapper_yinshengjie, OutRepertory> implements OutRepertoryService_yinshengjie {

    @Override
    public long countByExample(OutRepertoryExample example) {
        return baseMapper.countByExample(example);
    }
    @Override
    public int deleteByExample(OutRepertoryExample example) {
        return baseMapper.deleteByExample(example);
    }
    @Override
    public List<OutRepertory> selectByExample(OutRepertoryExample example) {
        return baseMapper.selectByExample(example);
    }
    @Override
    public int updateByExampleSelective(OutRepertory record,OutRepertoryExample example) {
        return baseMapper.updateByExampleSelective(record,example);
    }
    @Override
    public int updateByExample(OutRepertory record,OutRepertoryExample example) {
        return baseMapper.updateByExample(record,example);
    }

    @Override
    public DataGridView selectBygatherTag() {
        return new DataGridView( baseMapper.selectBygatherTag());
    }

    @Override
    public void addOutRepertory(OutRepertory o1) {
         baseMapper.addOutRepertory(o1);
    }

    @Override
    public OutRepertory selectByPayId(String payId) {
        return baseMapper.selectByPayId(payId);
    }

    @Override
    public DataGridView selectShengHe() {
        return new DataGridView(baseMapper.selectShengHe());
    }

    @Override
    public DataGridView selectShengHe2(Integer o1Id) {
        return new DataGridView(baseMapper.selectShengHe2(o1Id));
    }

    @Override
    public void ShenHePass(int id) {
        baseMapper.ShenHePass(id);
    }

    @Override
    public void ShenHeNoPass(int id) {
        baseMapper.ShenHeNoPass(id);
    }

    @Override
    public DataGridView QueryShengHe() {
        return new DataGridView(baseMapper.QueryShengHe());
    }

	@Override
	public List<ShengHeVo> FindPayId(OutRepertory outRepertory) {
		return baseMapper.FindPayId(outRepertory);
	}

	@Override
	public int edit(OutRepertory outRepertory) {
		return baseMapper.edit(outRepertory);
	}

	/**
	 * 出库
	 *
	 * @param outRepertory
	 * @return
	 */
	@Override
	public List<ShengHeVo> repetition(OutRepertory outRepertory) {
		return baseMapper.repetition(outRepertory);
	}

	/**
	 * 修改复核出库状态
	 *
	 * @param outRepertory
	 * @return
	 */
	@Override
	public int repetitionEdit(OutRepertory outRepertory) {
		return baseMapper.repetitionEdit(outRepertory);
	}

	/**
	 * 查询已经出库
	 *
	 * @param outRepertory
	 * @return
	 */
	@Override
	public List<ShengHeVo> delivery(OutRepertory outRepertory) {
		return baseMapper.delivery(outRepertory);
	}

}
