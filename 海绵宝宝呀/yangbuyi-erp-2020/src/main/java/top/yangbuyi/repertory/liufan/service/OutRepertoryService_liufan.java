package top.yangbuyi.repertory.liufan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.liufan.domain.OutRepertory;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryExample;
import top.yangbuyi.repertory.liufan.vo.ShengHeVo;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

public interface OutRepertoryService_liufan extends IService<OutRepertory>{


    long countByExample(OutRepertoryExample example);

    int deleteByExample(OutRepertoryExample example);

    List<OutRepertory> selectByExample(OutRepertoryExample example);

    int updateByExampleSelective(OutRepertory record, OutRepertoryExample example);

    int updateByExample(OutRepertory record, OutRepertoryExample example);

    DataGridView selectBygatherTag();

    void addOutRepertory(OutRepertory o1);

    OutRepertory selectByPayId(String payId);

    DataGridView selectShengHe(ShengHeVo sh);

    DataGridView selectShengHe2(@Param("o1Id") Integer o1Id);


    void ShenHePass(int id);

    void ShenHeNoPass(int id);

    DataGridView QueryShengHe(@Param("shvo") ShengHeVo shvo);

    DataGridView QueryProducemassage();
    
    DataGridView QueryRepertoryCell();
}
