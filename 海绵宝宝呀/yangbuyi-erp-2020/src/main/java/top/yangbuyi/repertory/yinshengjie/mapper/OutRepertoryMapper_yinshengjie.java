package top.yangbuyi.repertory.yinshengjie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertory;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryDetails;
import top.yangbuyi.repertory.yinshengjie.domain.OutRepertoryExample;
import top.yangbuyi.repertory.yinshengjie.vo.ShengHeVo;
import top.yangbuyi.repertory.yinshengjie.vo.ShengHeVo2;

import java.util.List;

@Mapper
public interface OutRepertoryMapper_yinshengjie extends BaseMapper<OutRepertory> {
    long countByExample(OutRepertoryExample example);

    int deleteByExample(OutRepertoryExample example);

    List<OutRepertory> selectByExample(OutRepertoryExample example);

    int updateByExampleSelective(@Param("record") OutRepertory record, @Param("example") OutRepertoryExample example);

    int updateByExample(@Param("record") OutRepertory record, @Param("example") OutRepertoryExample example);

    List<OutRepertoryDetails> selectBygatherTag();

    void addOutRepertory(OutRepertory o1);

    OutRepertory selectByPayId(String payId);

    List<ShengHeVo> selectShengHe();

    List<ShengHeVo2> selectShengHe2(@Param("o1Id") Integer o1Id);

    void ShenHePass(int id);

    void ShenHeNoPass(int id);

    List<ShengHeVo> QueryShengHe();


	/**
	 * 我的接口 高级查询
	 * @param outRepertory
	 * @return
	 */
	List<ShengHeVo> FindPayId(@Param("outRepertory") OutRepertory outRepertory);

	/**
	 * 修改出库 去复查
	 * @param outRepertory
	 * @return
	 */
    int edit(@Param("outRepertory") OutRepertory outRepertory);

	/**
	 * 复核出库查询
	 */
	List<ShengHeVo> repetition(@Param("outRepertory") OutRepertory outRepertory);

	/**
	 * 修改复核出库状态
	 * @param outRepertory
	 * @return
	 */
	int repetitionEdit(@Param("outRepertory") OutRepertory outRepertory);

	/**
	 * 查询已经出库
	 */
	List<ShengHeVo> delivery(@Param("outRepertory") OutRepertory outRepertory);
}
