package top.yangbuyi.repertory.zxe.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yangbuyi.repertory.zxe.domain.PutRepertory;
import top.yangbuyi.repertory.zxe.mapper.PutRepertoryDetails_zxe_Mapper;
import top.yangbuyi.repertory.zxe.mapper.PutRepertory_zxe_Mapper;
import top.yangbuyi.repertory.zxe.service.PutRepertory_zxe_Service;
import top.yangbuyi.system.common.ActiveUser;
import top.yangbuyi.system.domain.User;

import java.util.ArrayList;
import java.util.Random;

@Service
public class PutRepertoryZxeServiceImpl extends ServiceImpl<PutRepertory_zxe_Mapper, PutRepertory> implements PutRepertory_zxe_Service {
	@Autowired
	private PutRepertory_zxe_Mapper PutRepertory_zxe_Mapper;
	@Autowired
	private PutRepertoryDetails_zxe_Mapper PutRepertoryDetails_zxe_Mapper;

	@Override
	public PutRepertory PutRepertoryadd(PutRepertory PutRepertory) {
		ActiveUser ActiveUser=(ActiveUser) SecurityUtils.getSubject().getPrincipal();
		User User=ActiveUser.getUser();
		Random Random =new Random();
		String gatherid="rk"+(Random.nextInt(99999)+10000);
		PutRepertory.setGatherId(gatherid);
		PutRepertory.setRegister(User.getLoginname());
		PutRepertory_zxe_Mapper.PutRepertoryadd(PutRepertory);
		System.err.println(PutRepertory_zxe_Mapper.PutRepertoryselname(gatherid));
		return PutRepertory_zxe_Mapper.PutRepertoryselname(gatherid);
	}

	@Override
	public ArrayList PutRepertorychecker(String name) {
		return PutRepertory_zxe_Mapper.PutRepertorychecker(name);
	}

	@Override
	public Boolean PutRepertoryupdate(String check, Integer[] id,String gather) {
		boolean bol=false;
		System.err.println("S001-2".equals(check));
		ActiveUser ActiveUser=(ActiveUser)SecurityUtils.getSubject().getPrincipal();
		User User = ActiveUser.getUser();
		if("S001-2".equals(check)){
			System.err.println(1);
			for (int i = 0; i < id.length; i++) {
				bol= PutRepertory_zxe_Mapper.PutRepertoryupdate(check, id[i],null,User.getLoginname())>0?true:false;
			}
		}else{
			System.err.println(2);
			for (int i = 0; i < id.length; i++) {
				PutRepertoryDetails_zxe_Mapper.PutRepertoryDetailsidupdate("K002-2",id[i]);
				bol= PutRepertory_zxe_Mapper.PutRepertoryupdate(check, id[i],"K002-2",User.getLoginname())>0?true:false;
			}
		}
		return bol;
	}

	@Override
	public ArrayList<PutRepertory> PutRepertorychecker2(String chack,String name) {
		return PutRepertory_zxe_Mapper.PutRepertorychecker2(chack,name);
	}
}
