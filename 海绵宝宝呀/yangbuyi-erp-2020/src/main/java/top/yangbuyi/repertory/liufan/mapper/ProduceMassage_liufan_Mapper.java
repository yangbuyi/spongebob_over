package top.yangbuyi.repertory.liufan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.liufan.domain.ProduceMassage;
import top.yangbuyi.repertory.liufan.domain.ProduceMassageExample;

public interface ProduceMassage_liufan_Mapper extends BaseMapper<ProduceMassage> {
    long countByExample(ProduceMassageExample example);

    int deleteByExample(ProduceMassageExample example);

    List<ProduceMassage> selectByExample(ProduceMassageExample example);

    int updateByExampleSelective(@Param("record") ProduceMassage record, @Param("example") ProduceMassageExample example);

    int updateByExample(@Param("record") ProduceMassage record, @Param("example") ProduceMassageExample example);
}
