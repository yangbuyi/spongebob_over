package top.yangbuyi.repertory.liufan.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.yangbuyi.system.vo.BaseVo;

@Data
@EqualsAndHashCode(callSuper = false)
public class ProduceMassageVo extends BaseVo {
    private String ProductId;
    private String ProductName;
    private String productDescribe;
    private String amountUnit;
    private Integer amount;
    private double costPrice;
    private double subtotal;
    private String pmProduct;
    private Integer pmamount;
}
