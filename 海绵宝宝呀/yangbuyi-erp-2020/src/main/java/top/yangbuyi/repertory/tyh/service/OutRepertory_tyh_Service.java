package top.yangbuyi.repertory.tyh.service;

import top.yangbuyi.repertory.tyh.domain.OutRepertory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface OutRepertory_tyh_Service extends IService<OutRepertory>{

      /**
       * 获取出库单
       */
      public List<OutRepertory> getOutRepertory();

      public void Warehouschedul(Integer id);

}
