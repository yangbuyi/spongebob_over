package top.yangbuyi.repertory.tyh.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

@TableName(value = "out_repertory_details")
public class OutRepertoryDetails implements Serializable {
    /**
     * 序号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 出库单号
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 商品编号
     */
    @TableField(value = "product_id")
    private String productId;

    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;

    /**
     * 商品描述
     */
    @TableField(value = "product_describe")
    private String productDescribe;

    /**
     * 出库数量
     */
    @TableField(value = "amount")
    private BigDecimal amount2;

    /**
     * 单位
     */
    @TableField(value = "amount_unit")
    private String amountUnit;

    /**
     * 出库单价
     */
    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    /**
     * 小计
     */
    @TableField(value = "subtotal")
    private BigDecimal subtotal;

    /**
     * 确认出库件数
     */
    @TableField(value = "paid_amount")
    private BigDecimal paidAmount;

    /**
     * 出库标志  K002-1: 已登记  K002-2: 已调度
     */
    @TableField(value = "pay_tag")
    private String payTag;

    private RepertoryCell repertoryCell;

      public RepertoryCell getRepertoryCell() {
            return repertoryCell;
      }

      public void setRepertoryCell(RepertoryCell repertoryCell) {
            this.repertoryCell = repertoryCell;
      }

      private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PARENT_ID = "parent_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_PRODUCT_DESCRIBE = "product_describe";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_SUBTOTAL = "subtotal";

    public static final String COL_PAID_AMOUNT = "paid_amount";

    public static final String COL_PAY_TAG = "pay_tag";

    /**
     * 获取序号
     *
     * @return id - 序号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置序号
     *
     * @param id 序号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取出库单号
     *
     * @return parent_id - 出库单号
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置出库单号
     *
     * @param parentId 出库单号
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取商品编号
     *
     * @return product_id - 商品编号
     */
    public String getProductId() {
        return productId;
    }

    /**
     * 设置商品编号
     *
     * @param productId 商品编号
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 获取商品名称
     *
     * @return product_name - 商品名称
     */
    public String getProductName() {
        return productName;
    }

    /**
     * 设置商品名称
     *
     * @param productName 商品名称
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * 获取商品描述
     *
     * @return product_describe - 商品描述
     */
    public String getProductDescribe() {
        return productDescribe;
    }

    /**
     * 设置商品描述
     *
     * @param productDescribe 商品描述
     */
    public void setProductDescribe(String productDescribe) {
        this.productDescribe = productDescribe;
    }

    /**
     * 获取出库数量
     *
     * @return amount - 出库数量
     */
    public BigDecimal getAmount() {
        return amount2;
    }

    /**
     * 设置出库数量
     *
     * @param amount 出库数量
     */
    public void setAmount(BigDecimal amount) {
        this.amount2 = amount;
    }

    /**
     * 获取单位
     *
     * @return amount_unit - 单位
     */
    public String getAmountUnit() {
        return amountUnit;
    }

    /**
     * 设置单位
     *
     * @param amountUnit 单位
     */
    public void setAmountUnit(String amountUnit) {
        this.amountUnit = amountUnit;
    }

    /**
     * 获取出库单价
     *
     * @return cost_price - 出库单价
     */
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    /**
     * 设置出库单价
     *
     * @param costPrice 出库单价
     */
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * 获取小计
     *
     * @return subtotal - 小计
     */
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    /**
     * 设置小计
     *
     * @param subtotal 小计
     */
    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * 获取确认出库件数
     *
     * @return paid_amount - 确认出库件数
     */
    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    /**
     * 设置确认出库件数
     *
     * @param paidAmount 确认出库件数
     */
    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    /**
     * 获取出库标志  K002-1: 已登记  K002-2: 已调度
     *
     * @return pay_tag - 出库标志  K002-1: 已登记  K002-2: 已调度
     */
    public String getPayTag() {
        return payTag;
    }

    /**
     * 设置出库标志  K002-1: 已登记  K002-2: 已调度
     *
     * @param payTag 出库标志  K002-1: 已登记  K002-2: 已调度
     */
    public void setPayTag(String payTag) {
        this.payTag = payTag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", productDescribe=").append(productDescribe);
          sb.append(", repertoryCell=").append(repertoryCell);
        sb.append(", amount=").append(amount2);
        sb.append(", amountUnit=").append(amountUnit);
        sb.append(", costPrice=").append(costPrice);
        sb.append(", subtotal=").append(subtotal);
        sb.append(", paidAmount=").append(paidAmount);
        sb.append(", payTag=").append(payTag);
        sb.append("]");
        return sb.toString();
    }
}