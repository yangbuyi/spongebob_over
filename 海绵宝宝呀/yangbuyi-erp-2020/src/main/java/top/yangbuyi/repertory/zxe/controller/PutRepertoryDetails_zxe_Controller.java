package top.yangbuyi.repertory.zxe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Vo;
import top.yangbuyi.repertory.zxe.service.PutRepertoryDetails_zxe_Service;
import top.yangbuyi.repertory.zxe.vo.PutRepertoryDetailsRe_zxe_Votou;
import top.yangbuyi.system.common.DataGridView;

import java.util.List;

@RestController
@RequestMapping("/api/zhangxinge/PutRepertoryDetails")
public class PutRepertoryDetails_zxe_Controller {
	@Autowired
	private PutRepertoryDetails_zxe_Service PutRepertoryDetails_zxe_Service;

	@RequestMapping("/insertPutRepertoryDetails")
	Object addPutRepertoryDetails(@RequestBody List<PutRepertoryDetailsRe_zxe_Vo> list){
		return PutRepertoryDetails_zxe_Service.insertPutRepertoryDetails(list);
	}

	@RequestMapping("/PutRepertoryDetailsid")
	public Object PutRepertoryDetailsid(Integer id){
		return new DataGridView(PutRepertoryDetails_zxe_Service.PutRepertoryDetailsid(id));
	}

	@RequestMapping("/PutRepertoryDetailsreupdate")
	public Object update(@RequestBody List<PutRepertoryDetailsRe_zxe_Votou> PutRepertoryDetailsRe_zxe_Votou){
		return PutRepertoryDetails_zxe_Service.deletePutRepertoryDetails(PutRepertoryDetailsRe_zxe_Votou);
	}
}
