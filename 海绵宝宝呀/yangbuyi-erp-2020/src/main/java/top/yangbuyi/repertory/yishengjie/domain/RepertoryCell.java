package top.yangbuyi.repertory.yishengjie.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "repertory_cell")
public class RepertoryCell implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "store_id")
    private String storeId;

    @TableField(value = "product_id")
    private String productId;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "first_kind_id")
    private String firstKindId;

    @TableField(value = "first_kind_name")
    private String firstKindName;

    @TableField(value = "second_kind_id")
    private String secondKindId;

    @TableField(value = "second_kind_name")
    private String secondKindName;

    @TableField(value = "third_kind_id")
    private String thirdKindId;

    @TableField(value = "third_kind_name")
    private String thirdKindName;

    @TableField(value = "min_amount")
    private BigDecimal minAmount;

    @TableField(value = "max_amount")
    private BigDecimal maxAmount;

    @TableField(value = "max_capacity_amount")
    private BigDecimal maxCapacityAmount;

    @TableField(value = "amount")
    private BigDecimal amount;

    @TableField(value = "config")
    private String config;

    @TableField(value = "register")
    private String register;

    @TableField(value = "register_time")
    private Date registerTime;

    @TableField(value = "checker")
    private String checker;

    @TableField(value = "check_time")
    private Date checkTime;

    @TableField(value = "check_tag")
    private String checkTag;

    @TableField(value = "store_name")
    private String storeName;

    @TableField(value = "storage_unit_shorted")
    private String storageUnitShorted;

    @TableField(value = "amount_unit")
    private String amountUnit;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_STORE_ID = "store_id";

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_FIRST_KIND_ID = "first_kind_id";

    public static final String COL_FIRST_KIND_NAME = "first_kind_name";

    public static final String COL_SECOND_KIND_ID = "second_kind_id";

    public static final String COL_SECOND_KIND_NAME = "second_kind_name";

    public static final String COL_THIRD_KIND_ID = "third_kind_id";

    public static final String COL_THIRD_KIND_NAME = "third_kind_name";

    public static final String COL_MIN_AMOUNT = "min_amount";

    public static final String COL_MAX_AMOUNT = "max_amount";

    public static final String COL_MAX_CAPACITY_AMOUNT = "max_capacity_amount";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_CONFIG = "config";

    public static final String COL_REGISTER = "register";

    public static final String COL_REGISTER_TIME = "register_time";

    public static final String COL_CHECKER = "checker";

    public static final String COL_CHECK_TIME = "check_time";

    public static final String COL_CHECK_TAG = "check_tag";

    public static final String COL_STORE_NAME = "store_name";

    public static final String COL_STORAGE_UNIT_SHORTED = "storage_unit_shorted";

    public static final String COL_AMOUNT_UNIT = "amount_unit";

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return store_id
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * @param storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return product_name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return first_kind_id
     */
    public String getFirstKindId() {
        return firstKindId;
    }

    /**
     * @param firstKindId
     */
    public void setFirstKindId(String firstKindId) {
        this.firstKindId = firstKindId;
    }

    /**
     * @return first_kind_name
     */
    public String getFirstKindName() {
        return firstKindName;
    }

    /**
     * @param firstKindName
     */
    public void setFirstKindName(String firstKindName) {
        this.firstKindName = firstKindName;
    }

    /**
     * @return second_kind_id
     */
    public String getSecondKindId() {
        return secondKindId;
    }

    /**
     * @param secondKindId
     */
    public void setSecondKindId(String secondKindId) {
        this.secondKindId = secondKindId;
    }

    /**
     * @return second_kind_name
     */
    public String getSecondKindName() {
        return secondKindName;
    }

    /**
     * @param secondKindName
     */
    public void setSecondKindName(String secondKindName) {
        this.secondKindName = secondKindName;
    }

    /**
     * @return third_kind_id
     */
    public String getThirdKindId() {
        return thirdKindId;
    }

    /**
     * @param thirdKindId
     */
    public void setThirdKindId(String thirdKindId) {
        this.thirdKindId = thirdKindId;
    }

    /**
     * @return third_kind_name
     */
    public String getThirdKindName() {
        return thirdKindName;
    }

    /**
     * @param thirdKindName
     */
    public void setThirdKindName(String thirdKindName) {
        this.thirdKindName = thirdKindName;
    }

    /**
     * @return min_amount
     */
    public BigDecimal getMinAmount() {
        return minAmount;
    }

    /**
     * @param minAmount
     */
    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    /**
     * @return max_amount
     */
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    /**
     * @param maxAmount
     */
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    /**
     * @return max_capacity_amount
     */
    public BigDecimal getMaxCapacityAmount() {
        return maxCapacityAmount;
    }

    /**
     * @param maxCapacityAmount
     */
    public void setMaxCapacityAmount(BigDecimal maxCapacityAmount) {
        this.maxCapacityAmount = maxCapacityAmount;
    }

    /**
     * @return amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return config
     */
    public String getConfig() {
        return config;
    }

    /**
     * @param config
     */
    public void setConfig(String config) {
        this.config = config;
    }

    /**
     * @return register
     */
    public String getRegister() {
        return register;
    }

    /**
     * @param register
     */
    public void setRegister(String register) {
        this.register = register;
    }

    /**
     * @return register_time
     */
    public Date getRegisterTime() {
        return registerTime;
    }

    /**
     * @param registerTime
     */
    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * @return checker
     */
    public String getChecker() {
        return checker;
    }

    /**
     * @param checker
     */
    public void setChecker(String checker) {
        this.checker = checker;
    }

    /**
     * @return check_time
     */
    public Date getCheckTime() {
        return checkTime;
    }

    /**
     * @param checkTime
     */
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * @return check_tag
     */
    public String getCheckTag() {
        return checkTag;
    }

    /**
     * @param checkTag
     */
    public void setCheckTag(String checkTag) {
        this.checkTag = checkTag;
    }

    /**
     * @return store_name
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return storage_unit_shorted
     */
    public String getStorageUnitShorted() {
        return storageUnitShorted;
    }

    /**
     * @param storageUnitShorted
     */
    public void setStorageUnitShorted(String storageUnitShorted) {
        this.storageUnitShorted = storageUnitShorted;
    }

    /**
     * @return amount_unit
     */
    public String getAmountUnit() {
        return amountUnit;
    }

    /**
     * @param amountUnit
     */
    public void setAmountUnit(String amountUnit) {
        this.amountUnit = amountUnit;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", storeId=").append(storeId);
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", firstKindId=").append(firstKindId);
        sb.append(", firstKindName=").append(firstKindName);
        sb.append(", secondKindId=").append(secondKindId);
        sb.append(", secondKindName=").append(secondKindName);
        sb.append(", thirdKindId=").append(thirdKindId);
        sb.append(", thirdKindName=").append(thirdKindName);
        sb.append(", minAmount=").append(minAmount);
        sb.append(", maxAmount=").append(maxAmount);
        sb.append(", maxCapacityAmount=").append(maxCapacityAmount);
        sb.append(", amount=").append(amount);
        sb.append(", config=").append(config);
        sb.append(", register=").append(register);
        sb.append(", registerTime=").append(registerTime);
        sb.append(", checker=").append(checker);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", checkTag=").append(checkTag);
        sb.append(", storeName=").append(storeName);
        sb.append(", storageUnitShorted=").append(storageUnitShorted);
        sb.append(", amountUnit=").append(amountUnit);
        sb.append("]");
        return sb.toString();
    }
}