package top.yangbuyi.repertory.liufan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yangbuyi.repertory.liufan.domain.OutRepertory;
import top.yangbuyi.repertory.liufan.domain.OutRepertoryExample;
import top.yangbuyi.repertory.liufan.domain.ProduceMassage;
import top.yangbuyi.repertory.liufan.vo.ProduceMassageVo;
import top.yangbuyi.repertory.liufan.vo.ShengHeVo;
import top.yangbuyi.repertory.liufan.vo.ShengHeVo2;
import top.yangbuyi.repertory.liufan.vo.repertoryCellVo;

import java.util.List;

public interface OutRepertory_liufan_Mapper extends BaseMapper<OutRepertory> {
    long countByExample(OutRepertoryExample example);

    int deleteByExample(OutRepertoryExample example);

    List<OutRepertory> selectByExample(OutRepertoryExample example);

    int updateByExampleSelective(@Param("record") OutRepertory record, @Param("example") OutRepertoryExample example);

    int updateByExample(@Param("record") OutRepertory record, @Param("example") OutRepertoryExample example);

    List<ProduceMassageVo> selectBygatherTag();

    void addOutRepertory(OutRepertory o1);

    OutRepertory selectByPayId(String payId);

    List<ShengHeVo> selectShengHe(ShengHeVo sh);

    List<ShengHeVo2> selectShengHe2(@Param("o1Id") Integer o1Id);

    void ShenHePass(int id);

    void ShenHeNoPass(int id);

    List<ShengHeVo> QueryShengHe(@Param("shvo") ShengHeVo shvo);

    List<ProduceMassage> QueryProducemassage();
    
    List<repertoryCellVo> QueryRepertoryCell();
}
