package top.yangbuyi.repertory.yishengjie.service;

import top.yangbuyi.repertory.yishengjie.domain.RepertoryCell;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface RepertoryCell_ysj_Service extends IService<RepertoryCell>{
      public List<RepertoryCell> pageQuery(RepertoryCell repertoryCell);

}
